/**
 * Returns back in browser history.
 */
function goBackInHistory() {
    window.history.back();
}

/**
 * Information or error message.
 * 
 * @param {String} text text of message.
 * @param {String} type type of message.
 * @class MessageBox
 * @constructor
 */
MessageBox = function(text, type) {
    this.text = text;
    this.type = type;
};

/**
 * Shows entered messages.
 * 
 * @param {Array} messages array of MessageBox which will be shown.
 */
function showMessages(messages) {
    for(var i = 0; i < messages.length; i++) {
        showMessage(messages[i].text, messages[i].type);
    }
}

/**
 * Shows message with entered text.
 * 
 * @param {String} text text of message.
 * @param {String} type type of message.
 */
function showMessage(text, type) {
    if(type === undefined || type === null) {
        type = "";
    }
    
    var message = {
        text: text,
        type: type
    };

    dhtmlx.message(message);
}

/**
 * Blocks UI.
 */
function blockUI() {
    $.blockUI({message: '<h1 class="loading"><img src="../images/loading.gif" /><span>Loading</span></h1>'});
}

/**
 * Unblocks UI.
 */
function unblockUI() {
    $.unblockUI();
}

$(document).ajaxStart(function(){
    blockUI();
}).ajaxComplete(function(){
    unblockUI();
});

/**
 * Shows error message.
 * 
 * @param {String} text text of message.
 */
function error(text) {
    showMessage(text, "error");
}

/**
 * Shows information message.
 * 
 * @param {String} text text of message.
 */
function message(text) {
    showMessage(text);
}

/**
 * Shows error dialog.
 * 
 * @param {String} title title of dialog.
 * @param {String} text text in dialog.
 */
function errorDialog(title, text) {
    dhtmlx.alert({
        type: "alert-error",
        title: title, 
        text: text
    });
}

/**
 * Removes all entered elements from array.
 * 
 * @param {*} value element which will be removed from array.
 * @returns {Array} instance of this array.
 */
Array.prototype.removeElement = function(value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === value) {
            this.splice(i, 1);
            i--;
        }
    }
    
    return this;
};

/**
 * Returns information whether this string starts with entered string.
 * 
 * @param {String} needle string which will be search in this string.
 * @returns {Boolean} information whether this string starts with entered string.
 */
String.prototype.startsWith = function(needle) {
    return(this.indexOf(needle) === 0);
};

$(document).ready(function() {
    var header = "#header";
    var page = "#page";
    var hide = "#hide-logo";
    if(!$(header).length) {
        header = "#admin-header";
    }
    if(!$(page).length) {
        page = "#admin-page";
        if(!$(page).length) {
            page = "#login-page";
        }
    }
    
    $("#noscript").hide();
    $(page).show();

    
    $(header).mouseenter(function() {
        $(hide).show();
    }).mouseleave(function() {
        if($(this).find(hide).length) {
            $(hide).hide();
        }
    });
    
    $(hide).click(function() {
        if($(header).is(':visible')) {
            var element = $(this).detach();
            $(page).append(element);
            $(header).hide("slow");
        }
        else {
            var element = $(this).detach();
            $(header).append(element);
            $(header).show("slow");
            $(this).hide();
        }
    });
});
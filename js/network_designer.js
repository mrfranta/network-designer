/**
 * Namespace for network designer.
 * 
 * @namespace Network
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 */
var Network = Network || {};

/**
 * Tools which serves for designer.
 * @class DesignerTools
 * @static
 */
Network.DesignerTools = {
    /**
     * Returns entered color in rgb format: rgb(R, G, B).
     * 
     * @param {String} color in format rgb(R, G, B), #RGB or in english name.
     * @returns {String} Color in rgb format.
     * @function getRGB
     * @static
     */
    getRGB: function(color) {
        var result;
        var rgb = [0, 0, 0];

        // Look for rgb(num,num,num)
        if (result = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color)) {
            rgb = [parseInt(result[1]), parseInt(result[2]), parseInt(result[3])];
        }
        else if (result = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color)) { // Look for rgb(num%,num%,num%)
            rgb = [parseFloat(result[1]) * 2.55, parseFloat(result[2]) * 2.55, parseFloat(result[3]) * 2.55];
        }
        else {
            var hexColor = "#" + color;
            if (result = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(hexColor)) { // Look for #a0b1c2
                rgb = [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)];
            }
            else if (result = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color)) { // Look for #fff
                rgb = [parseInt(result[1] + result[1], 16), parseInt(result[2] + result[2], 16), parseInt(result[3] + result[3], 16)];
            }
            else { // english word for color
                // create a temporary div. 
                var div = $('<div></div>').appendTo("body").css('background-color', color);
                var computedStyle = window.getComputedStyle(div[0]);

                // get computed color.
                var rgbValue = computedStyle.backgroundColor;

                // cleanup temporary div.
                div.remove();

                if (rgbValue !== "transparent") {
                    return rgbValue;
                }
            }
        }

        var ret = "rgb(";
        for (var i = 0; i < rgb.length; i++) {
            if (i !== 0) {
                ret += ", ";
            }

            ret += rgb[i];
        }
        ret += ")";

        return ret;
    },
    /**
     * Create hexadecimal value of color from rgb format.
     * 
     * @param {String} colorval in rgb (R, G, B) format.
     * @returns {String} hexadecimal value of color.
     * @function hexc
     * @static
     */
    hexc: function(colorval) {
        var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        delete(parts[0]);
        for (var i = 1; i <= 3; ++i) {
            parts[i] = parseInt(parts[i]).toString(16);
            if (parts[i].length === 1)
                parts[i] = '0' + parts[i];
        }

        return parts.join('');
    },
    /**
     * Validates entered IP address.
     * 
     * @param {String} ipaddress IP address for validation.
     * @returns {Boolean} information whether is entered IP address valid.
     * @function validateIPaddress
     * @static
     */
    validateIPaddress: function(ipaddress) {
        if (this.IPaddressRegexp.test(ipaddress)) {
            return true;
        }

        return false;
    }
};

/**
 * @property {String} IPaddressRegexp regular expression for validation IP addresses.
 * @type String
 * @static
 */
Network.DesignerTools.IPaddressRegexp = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i;


/**
 * Represents basic mathematical 2D point.
 * 
 * @param {integer} x x coordinate of this point.
 * @param {integer} y y coordinate of this point.
 * @class Point
 * @constructor
 */
Network.Point = function(x, y) {
    /**
     * @property {integer} x x coordinate of this point.
     * @type integer
     */
    this.x = x;
    /**
     * @property {integer} y y coordinate of this point.
     * @type integer
     */
    this.y = y;
};

/**
 * Represents basic mathematical 2D vector.
 * 
 * @param {Network.Point} a first point of vector.
 * @param {Network.Point} b second point of vector.
 * @class Vector
 * @constructor
 */
Network.Vector = function(a, b) {
    /**
     * @property {integer} x first coordinate of this vector.
     * @type integer
     */
    this.x = (b.x - a.x);
    /**
     * @property {integer} y second coordinate of this vector.
     * @type integer
     */
    this.y = (b.y - a.y);

    /**
     * Counts angle of this vector with axel X.
     * 
     * @return {Number} Angle of this vector with axel X.
     * @function angle
     */
    this.angle = function() {
        var angle = Math.atan2(this.x, this.y);
        angle = angle * (180 / Math.PI) - 90;
        if (angle < 0) {
            angle += 360;
        }

        return angle;
    };
};

/**
 * Represents basic rectangle.
 * 
 * @param {integer} width rectangle width in px.
 * @param {integer} height rectangle height in px.
 * @class Rectangle
 * @constructor
 */
Network.Rectangle = function(width, height) {
    /**
     * @property {integer} width width of rectangle.
     * @type integer
     */
    this.width = width;
    /**
     * @property {integer} height height of rectangle.
     * @type integer
     */
    this.height = height;
};

/**
 * This class creates and shows modal dialog.
 * 
 * @param {String} title title of dialog
 * @param {String} text content of dialog.
 * @param {Array} buttons array of buttons.
 * @param {function} callback callback function for modal dialog.
 * @param {integer} width width of dialog (default is 300).
 * @param {String} element element of dialog which will be foccused after 
 * opening dialog. Its important for IE. Default is "input".
 * @returns {HTMLDivElement}
 * @class ModalDialog
 * @constructor
 */
Network.ModalDialog = function(title, text, buttons, callback, width, element) {
    if(callback === undefined || callback === null) {
        callback = function(box, button) {};
    }
    
    /** @type HTMLDivElement */
    var box = dhtmlx.modalbox({
        title: title,
        text: text,
        width: (width === undefined ? 300 : width) + "px",
        buttons: buttons,
        callback: function(button) {
            callback(box, button);
        }
    });

    //timeout is necessary only in IE
    setTimeout(function() {
        var name = element === undefined ? "input" : element;
        box.getElementsByTagName(name)[0].focus();
    }, 1);

    return box;
};

/**
 * Returns form box for entered form.
 * 
 * @param {String} form content of form.
 * @returns {String} form box which contains entered form.
 * @function formBox
 * @static
 */
Network.ModalDialog.formBox = function(form) {
    return "<div class='form_in_box'>\n" + form + "</div>";
};
/**
 * Returns line of form which contains input.
 * 
 * @param {String} label label for input.
 * @param {String} id identifier of input.
 * @param {String} value value of input. This is optional parameter.
 * @param {String} addition additional informations for input. It's optional parameter.
 * @param {String} type type of input. This is optional parameter.
 * @returns {String} form line which contains input.
 * @function formLine
 * @static
 */
Network.ModalDialog.formLine = function(label, id, value, addition, type) {
    return "    <div><label>" + label + ": <input id='" + id + "' class='inform' " + 
            "type='" + (type === undefined ? "text" : type) + "' " + 
            (value === undefined ? "" : " value = '" + value + "'") +
            (addition === undefined ? "" : addition) + " /></label></div>\n";
};

/**
 * Constructs and show configuration dialog for entered network designer.
 * 
 * @param {Network.Designer} designer designer for configuration.
 * @class DesignerConfigurationDialog
 * @constructor
 */
Network.DesignerConfigurationDialog = function(designer) {
    /** @type String */
    var text = Network.ModalDialog.formLine("Network name", "network-name", designer.getName(), "maxlength='100'") +
            Network.ModalDialog.formLine("Width", "designer-width", designer.getWidth()) +
            Network.ModalDialog.formLine("Height", "designer-height", designer.getHeight()) +
            Network.ModalDialog.formLine("Background", "designer-background", Network.DesignerTools.hexc(designer.getBackground()), "maxlength='20'");
    text = Network.ModalDialog.formBox(text);
    
    /**
     * Callback function for modal dialog which serves to configuration of entered designer.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @function callback
     */
    var callback = function(box) {
        var inputs = box.getElementsByTagName("input");
        var value = inputs[0].value;
        if (value === "") {
            error("Entered empty network. It will be set to the default value.");
        }
        else {
            designer.setName(value);
        }

        value = inputs[1].value;
        if (value <= 0) {
            error("Entered incorrectly width of designer. It will be set to the default value.");
        }
        else {
            designer.setWidth(value);
        }

        value = inputs[2].value;
        if (value <= 0) {
            error("Entered incorrectly height of designer. It will be set to the default value.");
        }
        else {
            designer.setHeight(value);
        }

        value = inputs[3].value;
        if (value === "") {
            error("Entered incorrectly background color of designer. It will be set to the default value.");
        }
        else {
            designer.setBackground(Network.DesignerTools.getRGB(value));
        }

        designer.init();

        message("Designer was configured.");
    };

    new Network.ModalDialog("Designer configuration", text, ["Configure"], callback);
};

/**
 * Constructs and show dialog for changing name of entered network designer.
 * 
 * @param {Network.Designer} designer designer for change name.
 * @class DesignerNameDialog
 * @constructor
 */
Network.DesignerNameDialog = function(designer) {
    /** @type String */
    var additions = "maxlength='100'";
    if(Network.Designer.viewer) {
        additions += " readonly='readonly'";
    }
    
    /** @type String */
    var text = Network.ModalDialog.formBox(
            Network.ModalDialog.formLine("Network name", "network-name", designer.getName(), additions)
    );
    
    /** @type Array.*/
    var buttons = Network.Designer.viewer ? ["Ok"] : ["Edit", "Cancel"];
    /** @type String.*/
    var title = Network.Designer.viewer ? "Network name" : "Edit network name";

    /**
     * Callback function for modal dialog which serves to editing name of network.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @param {String} button number of button which was pressed in dialog.
     * @function callback
     */
    var callback = function(box, button) {
        if (parseInt(button) === 1 || Network.Designer.viewer) {
            return;
        }

        var inputs = box.getElementsByTagName("input");
        var input = inputs[0];
        if (input.value === "") {
            error("Network name cannot be empty.");

            return;
        }
        else {
            designer.setName(input.value);
        }
    };

    new Network.ModalDialog(title, text, buttons, callback);
};

/**
 * Core class for network designer. This class contains all important data such as 
 * KineticJS stage, layers, shapes etc. Also contains all created devices and 
 * cables between them.
 * 
 * @class Designer
 * @constructor
 */
Network.Designer = function() {
    /** @type String */
    var designer = "#designer";
    $(designer).bind("contextmenu", new Function("return false"));

    /** @type Network.Designer */
    var instance = this;
    
    /**
     * @property {integer} id identifier of designer.
     * @type integer
     */
    this.id = -1;
    /**
     * @property {String} configurationName name of configuration which contains this network.
     * @type String
     */
    this.configurationName = null;
    
    /**
     * @property {boolean} initialized information whether is this designer initialized.
     * @type boolean
     */
    this.initialized = false;

    /** @type Kinetic.Text */
    var name = new Kinetic.Text({
        x: 0,
        y: 0,
        fontSize: 20,
        text: 'Network',
        fontFamily: 'Calibri',
        fill: 'black'
    });

    name.on('click tap', function(e) {
        if (e.which === 1) {
            if (Network.Cable.actual !== null || deleting) {
                return;
            }

            new Network.DesignerNameDialog(instance);
        }
    });

    // add cursor styling
    name.on('mouseover', function() {
        instance.setDeviceCursor();
    });

    name.on('mouseout', function() {
        instance.setDefaultCursor();
    });

    /** @type Array */
    var defaultConfiguration = ["Network", $(designer).width(), 
        $(designer).height(), $(designer).css("background-color")];

    /** @type Kinetic.Stage */
    var stage = null;
    /** @type Kinetic.Rect */
    var background = null;
    /** @type Kinetic.Layer */
    var layer = null;
    /** @type Kinetic.Layer */
    var backgroundLayer = null;

    /**
     * @property {Array} devices array of devices.
     * @type Array
     */
    this.devices = new Array();
    /**
     * @property {Array} cables array of cables.
     * @type Array
     */
    this.cables = new Array();

    /**
     * @property {Network.Managgement} management serves for saving and loading schemas from databse.
     * @type Network.Managgement
     */
    this.management = new Network.Management(this);

    /** @type Network.Device */
    var first = null;
    /** @type Kinetic.Line */
    var connectionLine = null;
    /** @type Boolean */
    var deleting = false;

    /**
     * Initializes designer.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function init
     */
    this.init = function() {
        if (this.isInitialized()) {
            return; // initialized
        }
        
        this.initialized = true;

        stage = new Kinetic.Stage({
            container: "designer",
            width: this.getWidth(),
            height: this.getHeight()
        });

        background = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: this.getWidth(),
            height: this.getHeight(),
            fill: $(designer).css("background-color")
        });

        layer = new Kinetic.Layer(); 
        backgroundLayer = new Kinetic.Layer();
        
        stage.add(backgroundLayer);
        backgroundLayer.add(background);
        
        stage.add(layer);
        
        this.addToTop(name);
        namePosition();

        this.setDefaultCursor();

        return this;
    };

    /**
     * Returns information whether is designer initialized.
     * 
     * @return {Boolean} Informatin whether is designer initialized.
     * @function isInitialized
     */
    this.isInitialized = function() {
        return this.initialized === true;
    };

    /**
     * Returns jQuery type which points to div containing network.
     * 
     * @function getDesigner
     * @returns {jQuery type} div which contains network.
     */
    this.getDesigner = function() {
        return $(designer);
    };

    /**
     * Sets cursor for devices.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function setDeviceCursor
     */
    this.setDeviceCursor = function() {
        var cursor = "url('../images/cursors/pointer.png'), default";
        if (Network.Cable.actual !== null || deleting) {
            return this;
        }

        this.getDesigner().css("cursor", cursor);
        return this;
    };

    /**
     * Sets default cursor for designer.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function setDefaultCursor
     */
    this.setDefaultCursor = function() {
        if (Network.Cable.actual !== null) {
            this.getDesigner().css("cursor", "url('../images/cursors/cable.png'), default");
            return this;
        }

        if (deleting) {
            this.getDesigner().css("cursor", "url('../images/cursors/delete.png'), default");
            return this;
        }

        this.getDesigner().css("cursor", "url('../images/cursors/cross.png'), default");
        return this;
    };
    
    /**
     * Returns pointer position in designer.
     * @function getPointerPosition
     */
    this.getPointerPosition = function() {
        return stage.getPointerPosition();
    };

    /**
     * Returns network name. Default value is "Newtork".
     * 
     * @function getName
     * @returns {String} network name.
     */
    this.getName = function() {
        return name.getText();
    };

    /**
     * Sets position of network name in designer.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function namePosition
     */
    var namePosition = function() {
        if (stage !== null) {
            var x = (stage.getWidth() - name.getWidth()) / 2;
            var y = 15;

            name.setPosition(x, y);
            stage.draw();
        }

        return this;
    };

    /**
     * Sets network name.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @param {String} newName description
     * @function setName
     */
    this.setName = function(newName) {
        name.setText(newName);
        namePosition();
        message("Name of network was changed to " + newName + ".");

        return this;
    };

    /**
     * Returns background of designer.
     * 
     * @return {String} background of designer in format: rgb(R, G, B)
     * @function getBackground
     */
    this.getBackground = function() {
        return $(designer).css("background-color");
    };

    /**
     * Sets background color of designer.
     * 
     * @param {String} color color in format: rgb(R, G, B)
     * @return {Network.Designer} Instance of this designer.
     * @function setBackground
     */
    this.setBackground = function(color) {
        $(designer).css("background-color", color);

        if (this.isInitialized()) {
            background.setFill(color);
            stage.draw();
        }

        message("Background of designer was changed to " + color + ".");

        return this;
    };

    /**
     * Returns width of designer.
     * 
     * @function getWidth
     * @return {integer} width of designer.
     */
    this.getWidth = function() {
        return $(designer).width();
    };

    /**
     * Sets width of designer.
     * 
     * @param {integer} width width of designer to set.
     * @return {Network.Designer} Instance of this designer.
     * @function setWidth
     */
    this.setWidth = function(width) {
        if (this.isInitialized()) {
            var error = false;
            layer.getChildren().each(function(shape, n) {
                if(shape === name) {
                    return;
                }
                
                if ((shape.getX() + shape.getWidth()) > width) {
                    error = true;
                }
            });
            
            if (error) {
                error("There is a device in the designer which will occur out of designer after width change.");

                return this;
            }
            
            background.setWidth(width);
            stage.setWidth(width);
            namePosition();
            this.draw();
        }

        $(designer).width(width);
        message("Width of designer was changed to " + width + "px.");

        return this;
    };

    /**
     * Returns height of designer.
     * 
     * @return {integer} height of designer.
     * @function getHeight
     */
    this.getHeight = function() {
        return $(designer).height();
    };

    /**
     * Sets height of designer.
     * 
     * @param {integer} height height of designer to set.
     * @return {Network.Designer} Instance of this designer.
     * @function setHeight
     */
    this.setHeight = function(height) {
        if (this.isInitialized()) {
            var error = false;
            layer.getChildren().each(function(shape, n) {
                if(shape === name) {
                    return;
                }
                
                if ((shape.getY() + shape.getHeight()) >= height) {
                    error = true;
                }
            });
            
            if (error) {
                error("There is a device in the designer which will occur out of designer after height change.");

                return this;
            }

            background.setHeight(height);
            stage.setHeight(height);
            this.draw();
        }

        $(designer).height(height);
        message("Height of designer was changed to " + height + "px.");

        return this;
    };

    /**
     * Adds entered shape into bottom of layer.
     * 
     * @param {Kinetic.Shape} shape shape to add into bottom layer.
     * @return {Kinetic.Layer} return layer.
     * @function addToBottom
     */
    this.addToBottom = function(shape) {
        layer.add(shape);
        shape.moveToBottom();

        return layer;
    };

    /**
     * Adds entered shape into top of layer.
     * 
     * @param {Kinetic.Shape} shape shape to add into top layer.
     * @return {Kinetic.Layer} returns layer.
     * @function addToTop
     */
    this.addToTop = function(shape) {
        layer.add(shape);
        shape.moveToTop();
        shape.moveDown();

        return layer;
    };

    /**
     * Redraws all layers in designer.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function draw
     */
    this.draw = function() {
        stage.draw();

        return this;
    };
    
    /**
     * Fast redraw.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function batchDraw
     */
    this.batchDraw = function() {
        stage.batchDraw();
        
        return this;
    };

    /**
     * Click action for entered device.
     * 
     * @param {Network.Device} device device on which was clicked.
     * @function deviceClickAction
     */
    this.deviceClickAction = function(device) {        
        if (deleting) {
            device.remove();

            return;
        }

        if (Network.Cable.actual === null) {
            new Network.DeviceInformationDialog(device);

            return;
        }

        if (first === null) {
            if (device.structure.avaible <= 0) {
                error("This device has all ports occupied.");

                return;
            }

            first = device;
            device.setSelected(true);

            var x = device.getX() + (device.getWidth() / 2);
            var y = device.getY() + 5;
            var cable = Network.Cable.actual;
            connectionLine = new Kinetic.Line({
                points: [x, y, x, y],
                stroke: cable.color,
                strokeWidth: 5,
                lineCap: 'round'
            });

            if (cable.line === "dashed") {
                connectionLine.setDashArray([20, 10]);
            }

            if (cable.line === "dotted") {
                connectionLine.setDashArray([0.001, 20]);
            }

            stage.on('mousemove', function() {
                var mousePos = stage.getPointerPosition();
                var lx = x;
                var ly = y;
                var mx = mousePos.x;
                var my = mousePos.y;

                connectionLine.setPoints([lx, ly, mx, my]);
                instance.draw();
            });

            this.addToBottom(connectionLine);
            this.draw();

            return;
        }

        if (first === device) {
            this.resetConnecting();

            return;
        }

        if (device.structure.avaible <= 0) {
            error("This device has all ports occupied.");
            this.resetConnecting();
            return;
        }

        device.setSelected(true);
        stage.off('mousemove');
        this.draw();

        new Network.CreateCableDialog(this, Network.Cable.actual, connectionLine, first, device);

        connectionLine = null;
        first = null;
    };

    /**
     * Click action for entered cable.
     * 
     * @param {Network.Cable} cable cable on which was clicked.
     * @function cableClickAction
     */
    this.cableClickAction = function(cable) {
        if(Network.Designer.viewer) {
            return;
        }
        
        if (deleting) {
            cable.remove();
            return;
        }

        if (Network.Cable.actual === null) {
            new Network.EditCableDialog(cable);
        }
    };

    /**
     * Resets action for connecting two devices.
     * @function resetConnecting
     */
    this.resetConnecting = function() {
        if (first !== null) {
            stage.off('mousemove');
            first.setSelected(false);
            first = null;

            connectionLine.remove();
            connectionLine = null;
            this.draw();
        }
    };

    /**
     * Erase designer and then show configuration dialog.
     * @function clear
     */
    this.clear = function() {        
        dhtmlx.confirm({
            title: "Erase designer",
            type: "confirm-warning",
            text: "Are you sure you want to erase designer?",
            callback: function(button) {
                if (!button) {
                    return;
                }
                
                var def = defaultConfiguration;
                instance.erase(def[0], def[1], def[2], def[3]);
                message("Designer was erased."); 
                
                new Network.DesignerConfigurationDialog(instance);
            }
        });
    };
    
    this.erase = function(newName, newWidth, newHeight, newBackground, init) {        
        if (Network.DesignerButton.actual !== null) {
            if(deleting) {
                this.setDeletionAction(Network.DesignerButton.actual.button);
            }
            else {
                this.setCable(Network.DesignerButton.actual.button, Network.Cable.actual);
            }
        }
        stage.destroyChildren();
        stage.destroy();
        
        name.setText(newName);

        var dsgner = instance.getDesigner();
        dsgner.width(newWidth);
        dsgner.height(newHeight);
        dsgner.css("background-color", newBackground);

        this.devices = new Array();
        this.cables = new Array();
        this.initialized = false;
        
        if(init !== undefined) {
            this.init();
        }
    };

    /**
     * Shows dialog which contains image of network in designer.
     * 
     * @function showImage
     */
    this.showImage = function() {
        stage.toDataURL({
            callback: function(dataUrl) {
                dataUrl = dataUrl.replace("'", "\\'");

                var text = "<div id='img_in_box'><img src='" + dataUrl + "' style='width: 500px; margin-left: 5px;' alt='Designer image'></div>";

                dhtmlx.modalbox({
                    title: "Network image",
                    text: text,
                    width: "540px",
                    buttons: ["Ok"]
                });
            }
        });
    };

    /**
     * Activate deletion mode in designer.
     * 
     * @param {HTMLButtonElement} button button which called this function.
     * @function setDeleteAction
     */
    this.setDeletionAction = function(button) {
        if(Network.Designer.viewer) {
            return;
        }
        
        if (deleting) {
            deleting = false;
            $('.tab button').removeAttr("disabled");
            Network.DesignerButton.actual.changeImage();
            Network.DesignerButton.actual = null;
        }
        else {
            this.resetConnecting();

            if (Network.DesignerButton.actual !== null) {
                this.setCable(Network.DesignerButton.actual.button, Network.Cable.actual);
            }

            deleting = true;
            $('.tab button').attr("disabled", "disabled");

            var children = $(button).find("img");
            var src = children.attr("src");
            Network.DesignerButton.actual = new Network.DesignerButton(button, src, "../images/no.png");
            Network.DesignerButton.actual.changeImage();
        }

        this.setDefaultCursor();
    };

    /**
     * Save configuration from this designer into database.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function save
     */
    this.save = function() {
        if(Network.Designer.viewer) {
            return this;
        }
        
        this.management.save();

        return this;
    };
    
    /**
     * Save configuration from this designer as new configuration into database.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function saveAs
     */
    this.saveAs = function() {
        if(Network.Designer.viewer) {
            return this;
        }
        
        this.management.saveAs();
        
        return this;
    };

    /**
     * Loads configuration from database which is inserted into designer.
     * 
     * @param {boolean} prompt information whether will be shown prompt dialog.
     * @return {Network.Designer} Instance of this designer.
     * @function load
     */
    this.load = function(prompt) {       
        var id = $("#networks").val();   
        if(id <= 0) {
            return this;
        }
        
        this.management.load(id, prompt);

        return this;
    };
    
    /**
     * Removes selected configuration from database.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function remove
     */
    this.remove = function() {
        if(Network.Designer.viewer) {
            return this;
        }
        
        var id = $("#networks").val();   
        if(id <= 0) {
            return this;
        }
        
        this.management.remove(id);

        return this;
    };
    
    /**
     * Exports selected configuration from database.
     * 
     * @return {Network.Designer} Instance of this designer.
     * @function export
     */
    this.exportNetwork = function() {
        var id = $("#networks").val();   
        if(id <= 0) {
            return this;
        }
        
        this.management.export(id);

        return this;
    };
    
    this.importNetwork = function() {        
        this.management.import();

        return this;
    };
};

/**
 * @property {boolean} viewer information whether is this designer in viewer mode.
 * @type boolean
 * @static
 */
Network.Designer.viewer = false; 

/**
 * Shows dialog for changing configuration of designer.
 */
Network.Designer.prototype.changeConfiguration = function() {
    new Network.DesignerConfigurationDialog(this);
};

/**
 * Adds device into designer.
 * 
 * @param {Network.DeviceStructure} structure structure of device to add.
 * @function addDevice
 */
Network.Designer.prototype.addDevice = function(structure) {
    var device = new Network.Device(this, structure);
    this.addToTop(device.getShape()).draw();
    
    if(this.devices.length === 0) {
        $(window).bind('beforeunload', Network.Designer.beforeunload);
    }
    
    this.devices.push(device);
    
    device.structure.idInDesigner = this.devices.indexOf(device);

    message("Device " + structure.name + " was added into designer.");
    
    return device;
};

/**
 * Action for button which shows dialog for creation new device.
 * 
 * @param {HTMLButtonElement} button button which called this function.
 * @param {integer} id indetifier of device.
 * @param {integer} ports number of ports in device. 0 means user specific.
 * @function createDevice
 */
Network.Designer.prototype.createDevice = function(button, id, ports) {
    var children = $(button).find("img");
    var src = children.attr('src');
    if (src === "") {
        return;
    }

    this.resetConnecting();
    if (Network.DesignerButton.actual !== null) {
        this.setCable(Network.DesignerButton.actual.button, Network.Cable.actual);
    }

    var imageObj = new Image();
    var designer = this;
    imageObj.onload = function() {
        var name = button.name;
        var device = new Network.DeviceStructure(id, name, this, ports);

        new Network.CreateDeviceDialog(designer, device);
    };

    imageObj.src = src;
};

/**
 * Action for button which select some cable as default.
 * 
 * @param {HTMLButtonElement} button button which called this function.
 * @param {Network.Cables} cable cable which will be selected as actual for connections.
 */
Network.Designer.prototype.setCable = function(button, cable) {
    var children = $(button).find("img");
    var src = children.attr("src");
    if (src === "") {
        return;
    }

    this.resetConnecting();

    if (Network.DesignerButton.actual !== null) {
        Network.DesignerButton.actual.changeImage();

        if (Network.DesignerButton.actual.button === button) {
            Network.DesignerButton.actual = null;
            Network.Cable.actual = null;
            this.setDefaultCursor();
            return;
        }
    }

    Network.DesignerButton.actual = new Network.DesignerButton(button, src, "../images/choosen.png");
    Network.DesignerButton.actual.changeImage();
    Network.Cable.actual = cable.structure;

    message("Cable " + cable.structure.name + " was set.");

    this.setDefaultCursor();
};

/**
 * Beforeunload action for document which contains this designer.
 * 
 * @returns {String} text for prompt before leaving document.
 * @function beforeunload
 * @static
 */
Network.Designer.beforeunload = function() {
    if (window.designer.isInitialized()) {
        return 'Changes have been made. Are you sure you want to leave this page?';
    }
    else {
        return null;
    }
};

/**
 * Structure for tool tips in designer.
 * @static
 */
Network.TooltipStructure = {
    /**
     * Constant for tooltip offset.
     * @static
     */
    OFFSET: {x: 20, y: 20},
    /**
     * Constant for identifiers of tooltip elements.
     * @static
     */
    ELEMENTS: {id: "#tooltip", titleId: "#tooltip-header", bodyId: "#tooltip-body"}
};

/**
 * Tooltip for designer.
 * 
 * @param {String} title title for tooltip.
 * @param {String} text text (body) for tooltip.
 * @class Tooltip
 * @constructor
 */
Network.Tooltip = function(title, text) {
    var tooltip = "<span id='tooltip'>\n\
    <span id='tooltip-header'>" + title + "</span>\n\
    <span id='tooltip-body'>" + text + "</span>\n\
</span>";

    $("body").append(tooltip);
    
    /**
     * Moves tooltip to specified position.
     * @param {PlainObject} e actual position of mouse cursor.
     * @function move
     */
    this.move = function(e) {
        var offset = Network.TooltipStructure.OFFSET;
        $(Network.TooltipStructure.ELEMENTS.id).css("top", (e.pageY + offset.y) + "px")
                .css("left", (e.pageX + offset.x) + "px").fadeIn("fast");
    };
    
    /**
     * Removes tooltip from page.
     * @function remove
     */
    this.remove = function() {
        $(Network.TooltipStructure.ELEMENTS.id).remove();
    };
    
    /**
     * Hides tooltip.
     * @function hide
     */
    this.hide = function() {
        $(Network.TooltipStructure.ELEMENTS.id).hide();
    };
    
    /**
     * Shows tooltip.
     * @function show
     */
    this.show = function() {
        $(Network.TooltipStructure.ELEMENTS.id).show();
    };
};

/**
 * Creates tooltip at specified position and sets it as actual.
 * @param {String} title title for tooltip.
 * @param {String} text text (body) for tooltip.
 * @param {PlainObject} e actual position of mouse cursor.
 * @returns {Network.Tooltip} Created tooltip.
 * @satic
 */
Network.Tooltip.create = function(title, text, e) {
    Network.Tooltip.actual = new Network.Tooltip(title, text);
    Network.Tooltip.actual.move(e);
    
    return Network.Tooltip.actual;
};

/**
 * @property {Network.Tooltip} Actual tooltip for designer.
 * @type Network.Tooltip
 * @static
 */
Network.Tooltip.actual = null;

/**
 * Class for button which was pressed in designer for connecting or removing devices.
 * 
 * @param {HTMLButtonElement} button button which was pressed.
 * @param {String} oldSrc old source path of image in button.
 * @param {type} newSrc new source path of image in button.
 * @constructor
 */
Network.DesignerButton = function(button, oldSrc, newSrc) {
    /**
     * @property {HTMLButtonElement} button HTML button.
     * @type HTMLButtonElement
     */
    this.button = button;
    /**
     * @property {String} oldSrc old source of button image.
     * @type String
     */
    this.oldSrc = oldSrc;
    /**
     * @property {String} newSrc new source of button image.
     * @type String
     */
    this.newSrc = newSrc;

    /**
     * Changes image in button.
     * @function changeImage
     */
    this.changeImage = function() {
        var img = $(button).find("img");
        var src = img.attr("src");
        if (src === oldSrc) {
            img.attr("src", newSrc);
        }
        else {
            img.attr("src", oldSrc);
        }
        ;
    };
};

/**
 * Constructs and show dialog for device creation.
 * 
 * @param {Network.Designer} designer designer in which will be created device.
 * @param {Network.DeviceStructure} device structure of device to create.
 * @class CreateDeviceDialog
 * 
 * @constructor
 */
Network.CreateDeviceDialog = function(designer, device) {
    /** @type String */
    var text = Network.ModalDialog.formLine("Device name", "device-name", device.label, "maxlength='100'");
    if (device.getPorts() === 0) {
        text += Network.ModalDialog.formLine("Device ports", "device-ports", "0", "maxlength='3'");
    }

    /**
     * Callback function for modal dialog which serves to creating device.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @param {String} button number of button which was pressed in dialog.
     */
    var callback = function(box, button) {
        if (parseInt(button) === 1) {
            return;
        }

        var inputs = box.getElementsByTagName("input");
        var input = inputs[0];
        if (input.value === "") {
            error("Device name cannot be empty.");
            return;
        }
        else {
            device.label = input.value;
        }

        if (device.getPorts() === 0) {
            input = inputs[1];
            if (input.value <= 0) {
                error("Number of device ports must be greater than 0.");
                return;
            }
            else {
                device.setPorts(input.value);
            }
        }

        designer.addDevice(device);
    };

    new Network.ModalDialog("Create new device", Network.ModalDialog.formBox(text), ["Create", "Cancel"], callback);
};

/**
 * Class which represents structure of device.
 * 
 * @param {integer} id identifier of device.
 * @param {String} name device name.
 * @param {Image} image device image.
 * @param {integer} ports number of ports in device.
 * 
 * @class DeviceStructure
 * @constructor
 */
Network.DeviceStructure = function(id, name, image, ports) {
    /**
     * @property {integer} id identifier of device.
     * @type integer
     */
    this.id = id;
    /**
     * @property {integer} idInDesigner identifier of device in designer.
     * @type integer
     */
    this.idInDesigner = -1;
    /**
     * @property {String} name device name.
     * @type String
     */
    this.name = name;
    /**
     * @property {String} label device label.
     * @type String
     */
    this.label = name;
    /**
     * @property {Image} image of device.
     * @type Image
     */
    this.image = image;
    /**
     * @property {integer} ports number of ports in device.
     * @type integer
     */
    var devicePorts = parseInt(ports);
    /**
     * @property {integer} avaible number of avaible ports.
     * @type integer
     */
    this.avaible = parseInt(ports);
    /**
     * Returns number of ports in device.
     * 
     * @return {int} number of ports in device.
     * @function getPorts
     */
    this.getPorts = function() {
        return devicePorts;
    };

    /**
     * Sets number of ports in device.
     * 
     * @param {int} ports number of ports in device.
     * @function setPorts
     */
    this.setPorts = function(ports) {
        devicePorts = parseInt(ports);
        this.avaible = parseInt(ports);
    };
};

/**
 * Class which represents device in network.
 * 
 * @param {Network.Designer} designer designer for this device.
 * @param {Network.DeviceStructure} structure structure of this device.
 * @class Device
 * @constructor
 */
Network.Device = function(designer, structure) {
    /** @type Network.Device */
    var instance = this;

    /**
     * @property {Array} ports array of device ports.
     * @type Array
     */
    this.ports = new Array();
    /**
     * @property {Array} interfaces array of device interfaces.
     * @type Array
     */
    this.interfaces = new Array();
    for (var i = 0; i < structure.getPorts(); i++) {
        this.ports[i] = null;
        this.interfaces[i] = new Network.Interface();
    }

    /**
     * @property {Array} cables array of connected cables.
     * @type Array
     */
    this.cables = new Array();

    /** @type Kinetic.Image */
    var img = new Kinetic.Image({
        x: 0,
        y: 0,
        image: structure.image,
        width: structure.image.width,
        height: structure.image.height
    });

    /** @type Kinetic.Text */
    var label = new Kinetic.Text({
        x: 0,
        y: 0,
        text: structure.label,
        fontSize: 12,
        fontFamily: 'Calibri',
        fill: 'green'
    });

    /** @type Kinetic.Rect */
    var frame = new Kinetic.Rect({
        x: 0,
        y: 0,
        width: Math.max(img.getWidth(), label.getWidth()),
        height: img.getHeight() + label.getHeight() + 15,
        stroke: '',
        strokeWidth: 0
    });

    /** @type Kinetic.Group */
    var group = new Kinetic.Group({
        x: 0,
        y: 0,
        width: frame.getWidth(),
        height: frame.getHeight(),
        draggable: true
    });
    
    if(Network.Designer.viewer) {
        group.setDraggable(false);
    }

    /**
     * @property {Netork.DeviceStructure} structure structure of device.
     * @type Network.DeviceStructure
     */
    this.structure = structure;

    // add cursor styling
    group.on('mouseover', function(e) {
        designer.setDeviceCursor();
        Network.Tooltip.create(structure.name, instance.getTooltip(), e);
    });
    
    group.on('mousemove', function(e) {
        Network.Tooltip.actual.move(e);
    });

    group.on('mouseout', function() {
        designer.setDefaultCursor();
        Network.Tooltip.actual.remove();
    });

    group.on('dragstart touchstart', function() {
        Network.Tooltip.actual.hide();
    });
    
    group.on('dragend touchend', function(e) {
        var mousePos = designer.getPointerPosition();
        if(mousePos !== undefined) {
            var x = mousePos.x;
            var y = mousePos.y;
            var pos = this.getAbsolutePosition();

            if(x > pos.x && x < pos.x + this.getWidth()) {
                if(y > pos.y && y < pos.y + this.getHeight()) {
                    Network.Tooltip.actual.move(e);
                    Network.Tooltip.actual.show();
                    return;
                }
            }
        }
        
        Network.Tooltip.actual.remove();
    });

    group.on('dragstart dragmove touchstart touchmove', function() {
        for (var i = 0; i < instance.cables.length; i++) {
            instance.cables[i].update(instance);
        }
        
        designer.batchDraw();
    });

    label.on('click tap', function(e) {
        if (e.which === 1) {
            if (Network.Cable.actual !== null) {
                return;
            }

            new Network.DeviceLabelDialog(instance);
        }
    });

    img.on('click tap', function(e) {
        if (e.which === 1) {
            designer.deviceClickAction(instance);
        }
    });

    var x = (group.getWidth() - img.getWidth()) / 2;
    var y = 0;
    img.setPosition(x, y);

    x = (group.getWidth() - label.getWidth()) / 2;
    y = img.getHeight() + 15;
    label.setPosition(x, y);

    group.add(frame);
    group.add(img);
    group.add(label);

    /**
     * Sets X coordinate of device.
     * 
     * @param {integer} x x coordinate of device.
     * @function setX
     */
    this.setX = function(x) {
        group.setAbsolutePosition(x, group.getPosition().y);
        group.getLayer().draw();
    };

    /**
     * Returns X coordinate of device.
     * 
     * @return {integer} X coordinate of device.
     * @function getX
     */
    this.getX = function() {
        return group.getPosition().x;
    };

    /**
     * Sets Y coordinate of device.
     * 
     * @param {integer} y y coordinate of device.
     * @function setY
     */
    this.setY = function(y) {
        group.setAbsolutePosition(group.getPosition().x, y);
        group.getLayer().draw();
    };

    /**
     * Returns Y coordinate of device.
     * 
     * @return {integer} Y coordinate of device.
     * @function getY
     */
    this.getY = function() {
        return group.getPosition().y;
    };

    /**
     * Returns device shape (group)
     * 
     * @return {Kinetic.Shape} device shape.
     * @function getX
     */
    this.getShape = function() {
        return group;
    };
    
    /**
     * Returns width of device.
     * 
     * @return {integer} width coordinate of device.
     * @function getWidth
     */
    this.getWidth = function() {
        return group.getWidth();
    };

    /**
     * Returns height of device.
     * 
     * @return {integer} height of device.
     * @function getHeight
     */
    this.getHeight = function() {
        return group.getHeight();
    };
    
    /**
     * Returns label for device.
     * 
     * @return {String} Label for device.
     * @function getLabel
     */
    this.getLabel = function() {
        return this.structure.label;
    };

    /**
     * Sets label of device.
     * 
     * @param {String} lbl label of device.
     * @function setLabel
     */
    this.setLabel = function(lbl) {
        var layer = group.getLayer();
        label.setText(lbl);

        structure.label = lbl;

        var width = Math.max(img.getWidth(), label.getWidth());
        group.setWidth(width);

        var x = (width - img.getWidth()) / 2;
        var y = 0;
        img.setPosition(x, y);

        x = (width - label.getWidth()) / 2;
        y = img.getHeight() + 15;
        label.setPosition(x, y);

        message("Device name was changed.");
        layer.draw();
    };
    
    /**
     * Returns tooltip for device.
     * 
     * @return {String} text of tooltip for this device.
     */
    this.getTooltip = function() {
        var tooltip = "<strong>Name: </strong>" + this.structure.label + "<br />\n";
        tooltip += "<strong>Interfaces:</strong><br />";
        tooltip += "<table class='interfaces'>"
        for(var i = 0; i < this.ports.length; i++) {
            tooltip += "<tr>"
            tooltip += "<td>eth" + i + "</td>";
            tooltip += "<td>" + (this.interfaces[i].ip == "" ? "---" : this.interfaces[i].ip) + "</td>";
            tooltip += "<td>" + (this.interfaces[i].mask == "" ? "---" : this.interfaces[i].mask) + "</td>";
            tooltip += "<td>" + (this.interfaces[i].vlans == "" ? "---" : this.interfaces[i].vlans) + "</td>";
            tooltip += "<td " + (this.ports[i] === null ? "class='disconnected'>disconnected" : "class='connected'>connected");
            tooltip += "</td>\n</tr>";
        }
        
        return tooltip;
    };

    /**
     * Removes device from designer.
     */
    this.remove = function() {
        while (this.cables.length > 0) {
            this.cables[0].remove();
        }

        designer.devices.removeElement(this);
        
        if(designer.devices.length === 0) {
            $(window).unbind('beforeunload');
        }
         
        var layer = group.getLayer();
        group.remove();
        layer.draw();
    };

    /**
     * Shows or hide border which represents selection of device.
     * 
     * @param {Boolean} bool information whether is device selected.
     * @function setSelected
     */
    this.setSelected = function(bool) {
        group.setDraggable(!bool);

        if (bool === true) {
            frame.setStroke('blue');
            frame.setStrokeWidth(1);
            group.getLayer().draw();

            return;
        }

        frame.setStroke('');
        frame.setStrokeWidth(0);
        group.getLayer().draw();
    };
};

/**
 * Structure for interface in device.
 * 
 * @class Interface
 * @constructor
 */
Network.Interface = function() {
    /**
     * @property {String} ip ip address of interface.
     * @type String
     */
    this.ip = "";
    /**
     * @property {String} mask subnet mask of interface.
     * @type String
     */
    this.mask = "";
    /**
     * @property {String} vlans accessible vlans.
     * @type String
     */
    this.vlans = "";
    /**
     * @property {String} info other informations about interface.
     * @type String
     */
    this.info = "";
};

/**
 * Constructs and show dialog for editing device name (label).
 * 
 * @param {Network.Device} device device for name edition.
 * 
 * @class DeviceLabelDialog
 * @constructor
 */
Network.DeviceLabelDialog = function(device) {
    /** @type String */
    var additions = "maxlength='100'";
    if(Network.Designer.viewer) {
        additions += " readonly='readonly'";
    }
    
    /** @type String */
    var text = Network.ModalDialog.formBox(
            Network.ModalDialog.formLine("Device name", "device-name", device.getLabel(), additions)
    );
    
    /** @type Array. */
    var buttons = Network.Designer.viewer ? ["Ok"] : ["Edit", "Cancel"];
    /** @type String. */
    var title = Network.Designer.viewer ? "Device name" : "Edit device name";

    /**
     * Callback function for modal dialog which serves to changes of device name.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @param {String} button number of button which was pressed in dialog.
     * @function callback
     */
    var callback = function(box, button) {
        if (parseInt(button) === 1 || Network.Designer.viewer) {
            return;
        }

        var inputs = box.getElementsByTagName("input");
        var input = inputs[0];
        if (input.value === "") {
            error("Device name cannot be empty.");
            return;
        }
        else {
            device.setLabel(input.value);
        }
    };

    new Network.ModalDialog(title, text, buttons, callback);
};

/**
 * Construct and shows dialog for changing interfaces of entered device.
 * 
 * @param {Network.Device} device device for which will be changed interface informations.
 * @class DeviceInformationDialog
 * @constructor
 */
Network.DeviceInformationDialog = function(device) {
    var text = "<select id='interfaces'>";
    for(var i = 0; i < device.interfaces.length; i++) {
        text += "   <option value='eth" + i + "'>eth" + i + "</option>";
    }
    text += "</select>";
    
    /** @type String */
    var additions = "maxlength='15'";
    var additionsTA = "";
    if(Network.Designer.viewer) {
        additions += " readonly='readonly'";
        additionsTA = " readonly='readonly'";
    }
    
    for(var i = 0; i < device.interfaces.length; i++) {
        var interface = device.interfaces[i];
        /** @type String */
        var form = Network.ModalDialog.formLine("IP address", "device-ip-" + i, interface.ip, additions) + 
                Network.ModalDialog.formLine("Subnet mask", "device-mask-" + i, interface.mask, additions) + 
                Network.ModalDialog.formLine("Vlans", "device-vlans-" + i, interface.vlans, additions);
        form += "   <div><label>Other informations:<textarea class='inform'" + additionsTA + ">" + interface.info + "</textarea></label></div>\n";
        
        if(i === 0) {
            text += Network.ModalDialog.formBox(form);
        }
        else {
            text += "<div class='form_in_box' style='display: none;'>\n" + form + "</div>";
        }
    }
    
    /** @type Array */
    var buttons = Network.Designer.viewer ? ["Ok"] : ["Edit", "Cancel"];

    /**
     * Callback function for modal dialog which serves to changes of device informations.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @param {String} button number of button which was pressed in dialog.
     * @function callback
     */
    var callback = function(box, button) {
        if (parseInt(button) === 1 || Network.Designer.viewer) {
            return;
        }

        var inputs = box.getElementsByTagName("input");
        var textareas = box.getElementsByTagName("textarea");
        for(var i = 0; i < inputs.length; i+=3) {
            var j = i/3;
            var interface = device.interfaces[j];
            var value = inputs[i].value;
            if ((value !== "") && !Network.DesignerTools.validateIPaddress(value)) {
                error("IP address of eth" + j + " is invalid.");
            }
            else {
                interface.ip = value;
            }

            value = inputs[i+1].value;
            if ((value !== "") && !Network.DesignerTools.validateIPaddress(value)) {
                error("Subnet mask of eth" + j + " is invalid.");
            }
            else {
                interface.mask = value;
            }
            
            interface.vlans = inputs[i+2].value;
            interface.info = textareas[j].value;
        }
        
        message("Interfaces of " + device.getLabel() + " was edited.");
    };

    var box = new Network.ModalDialog(device.getLabel(), text, buttons, callback);

    $('#interfaces').change(function() {
        var selected = $(this).prop("selectedIndex");
        $(box).find('.form_in_box').each(function(i) {
            if(i == selected) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });
};

/**
 * Class for cable structure.
 * 
 * @param {integer} id identifier of cable.
 * @param {String} name cable name.
 * @param {String} color color of cable.
 * @param {String} line type of line for cable.
 * @class CableStructure
 * @constructor
 */
Network.CableStructure = function(id, name, color, line) {
    /**
     * @property {integer} id identifier of cable.
     * @type integer
     */
    this.id = parseInt(id);
    /**
     * @property {String} name cable name.
     * @type String
     */
    this.name = name;
    /**
     * @property {String} color color of cable in format rgb(R,G,B)
     * @type String
     */
    this.color = Network.DesignerTools.getRGB("#" + color);
    /**
     * @property {String} line type of line for cable.
     * @type String
     */
    this.line = line;
};

/**
 * @property {Network.DesignerButton} actual button which was pressed for connecting cables.
 * @type {Network.DesignerButton}
 * @static
 */
Network.DesignerButton.actual = null;

/**
 * This class represents Cable which connects two devices.
 * 
 * @param {Network.Designer} designer network designer which contains this cable.
 * @param {Network.CableStructure} structure structure of this cable.
 * @param {Kinetic.Line} line cable line.
 * @param {Network.Device} from device from which cable leads.
 * @param {Network.Device} to device into which cable leads.
 * @param {integer} fromPort port number of device from which cable leads.
 * @param {integer} toPort port number of device into which cable leads.
 * @class Cable
 * @constructor
 */
Network.Cable = function(designer, structure, line, from, to, fromPort, toPort) {
    /**
     * @property {Network.Device} from device from which cable leads.
     * @type Network.Device
     */
    this.from = from;
    /**
     * @property {Network.Device} to device into which cable leads.
     * @type Network.Device
     */
    this.to = to;

    /**
     * @property {Network.CableStructure} structure structure of this able.
     * @type String
     */
    this.structure = structure;
    /**
     * @property {integer} offset for cable.
     * @type integer
     */
    this.offset = 5;
    
    var cables = designer.cables;
    for(var i = 0; i < cables.length; i++) {
        if(cables[i].equals(this) && cables[i] !== this) {
            this.offset += 7;
        }
    }

    /** @type Kinetic.Text */
    var fromLabel = new Kinetic.Text({
        x: 0,
        y: 0,
        text: 'eth' + fromPort,
        fontSize: 14,
        fontFamily: 'Calibri',
        fontStyle: 'bold',
        fill: 'green'
    });

    /** @type Kinetic.Text */
    var toLabel = new Kinetic.Text({
        x: 0,
        y: 0,
        text: 'eth' + toPort,
        fontSize: 14,
        fontFamily: 'Calibri',
        fontStyle: 'bold',
        fill: 'green'
    });

    /** @type Kinetic.Group */
    designer.addToBottom(fromLabel);
    designer.addToBottom(toLabel);
    
    /** @type Number */
    var angle = 0;

    /** @type Network.Cable */
    var instance = this;
    var init = function(shape) {
        shape.on('click tap', function(e) {
            if (e.which === 1) {
                designer.cableClickAction(instance);
            }
        });

        // add cursor styling
        shape.on('mouseover', function() {
            designer.setDeviceCursor();
        });

        shape.on('mouseout', function() {
            designer.setDefaultCursor();
        });
    };
    
    init(fromLabel);
    init(toLabel);
    init(line);

    /**
     * Updates at first device.
     * 
     * @param {Kinetic.Shape} shape shape of first device.
     * @function updateAtSource
     */
    var updateAtSource = function(shape) {
        var points = line.getPoints();
        var position = shape.getPosition();
        line.setPoints([position.x + shape.getWidth() / 2 - 5 + instance.offset,
            position.y + instance.offset,
            points[1].x,
            points[1].y]);
    };

    /**
     * Updates at second device.
     * 
     * @param {Kinetic.Shape} shape shape of second device.
     * @function updateAtTarget
     */
    var updateAtTarget = function(shape) {
        var points = line.getPoints();
        var position = shape.getPosition();
        line.setPoints([points[0].x,
            points[0].y,
            position.x + shape.getWidth() / 2 - 5 + instance.offset,
            position.y + instance.offset]);
    };

    /**
     * Changes label position as update reaction.
     * 
     * @param {Network.Point} pos label position.
     * @param {Network.Rectangle} rect label rectangle.
     * @param {Number} angle angle of cable line.
     */
    var changeLabelPosition = function(pos, rect, angle) {
        if (angle >= 0 && angle <= 90) {
            pos.x -= rect.width;
            pos.y -= rect.height;
        }
        else if (angle > 90 && angle <= 180) {
            pos.x -= rect.width;
        }
        else if (angle >= 270) {
            pos.y -= rect.height;
        }

        return pos;
    };

    /**
     * Updates labels for cable line.
     * 
     * @function updateLabels
     */
    var updateLabels = function() {
        var points = line.getPoints();
        var start = new Network.Point(points[0].x, points[0].y);
        var end = new Network.Point(points[1].x, points[1].y);

        var v = new Network.Vector(start, end);
        var angle = v.angle();

        var tmp = new Network.Rectangle(end.x - start.x, end.y - start.y);
        var fromRect = new Network.Rectangle(fromLabel.getWidth(), fromLabel.getHeight());
        var toRect = new Network.Rectangle(toLabel.getWidth(), toLabel.getHeight());

        var posFrom = new Network.Point(start.x + tmp.width / 3, start.y + tmp.height / 3); // 1/4 of line
        var posTo = new Network.Point(start.x + (2 * tmp.width) / 3, start.y + (2 * tmp.height) / 3); // 3/4 of line

        posFrom = changeLabelPosition(posFrom, fromRect, angle);
        posTo = changeLabelPosition(posTo, toRect, angle);

        fromLabel.setPosition(posFrom.x, posFrom.y);
        toLabel.setPosition(posTo.x, posTo.y);
    };

    /**
     * Updates cable as reaction for moving or changing entered device.
     * 
     * @param {Network.Device} device device wich was changed.
     * @param {boolean} draw information whether will be designer redrawed (optional - default true).
     * @function update
     */
    this.update = function(device, draw) {
        if (device === this.from) {
            updateAtSource(device.getShape());
        }
        else if (device === this.to) {
            updateAtTarget(device.getShape());
        }
        else {
            updateAtSource(this.from.getShape());
            updateAtTarget(this.to.getShape());
        }

        updateLabels();
        
        if(draw === undefined || draw === true) {
            designer.batchDraw();  //redraw current layer
        }
    };

    this.update(); // updates labels and line.

    /**
     * Returns port of device from which cable leads.
     * 
     * @return {integer} port number of device from which cable leads.
     * @return getFromPort.
     */
    this.getFromPort = function() {
        return fromPort;
    };

    /**
     * Sets port of device from which cable leads.
     * 
     * @param {integer} port port number of device from which cable leads.
     * @function setFromPort
     */
    this.setFromPort = function(port) {
        fromPort = port;
        fromLabel.setText('eth' + port);
        updateLabels();
    };

    /**
     * Returns port of device into which cable leads.
     * 
     * @return {integer} port number of device into which cable leads.
     * @return getToPort.
     */
    this.getToPort = function() {
        return toPort;
    };

    /**
     * Sets port of device into which cable leads.
     * 
     * @param {integer} port port number of device into which cable leads.
     * @function setToPort
     */
    this.setToPort = function(port) {
        toPort = port;
        toLabel.setText('eth' + port);
        updateLabels();
    };

    /**
     * Returns device from which cable leads.
     * 
     * @return {Network.Device} device fro which cable leads.
     * @function getToDevice
     */
    this.getToDevice = function() {
        return to;
    };

    /**
     * Returns device into which cable leads.
     * 
     * @return {Network.Device} device into which cable leads.
     * @function getFromDevice
     */
    this.getFromDevice = function() {
        return from;
    };
    
    /**
     * Returns information whether this cable equals to entered cable
     * 
     * @param {Network.Cable} cable cable to compare.
     * @return {boolean} information whether this cable equals to entered one.
     * @function equals
     */
    this.equals = function(cable) {
        if(cable.from === this.from && cable.to === this.to) {
            return true;
        }
        
        return (cable.from === this.to && cable.to === this.from);
    };

    /**
     * Removes cable from designer (configuration).
     * @function remove
     */
    this.remove = function() {
        designer.cables.removeElement(this);

        this.from.ports[fromPort] = null;
        this.from.cables.removeElement(this);
        this.from.structure.avaible++;

        this.to.ports[toPort] = null;
        this.to.cables.removeElement(this);
        this.to.structure.avaible++;

        var layer = line.getLayer();
        fromLabel.remove();
        toLabel.remove();
        line.remove();

        layer.draw();
    };
};

/**
 * @property {Network.CableStructure} actual structure of choosen cable.
 * @type Network.CableStructure
 * @static
 */
Network.Cable.actual = null;

/**
 * Creates cable between two ports of entered devices.
 * 
 * @param {Network.Designer} designer network designer which will contains cable.
 * @param {Network.CableStructure} structure cable structure.
 * @param {Kinetic.Line} line line which was created by designer.
 * @param {Network.Device} from device from which cable leads.
 * @param {Network.Device} to device into which cable leads.
 * @constructor
 */
Network.CreateCableDialog = function(designer, structure, line, from, to) {
    /** @type String */
    var selectFrom = Network.CreateCableDialog.createSelect(from, -1);
    /** @type String */
    var selectTo = Network.CreateCableDialog.createSelect(to, -1);

    /**
     * Callback function for modal dialog which serves to creating (connecting) cable.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @param {String} button number of button which was pressed in dialog.
     */
    var callback = function(box, button) {
        from.setSelected(false);
        to.setSelected(false);

        if (parseInt(button) === 1) {
            var layer = line.getLayer();
            line.remove();
            layer.draw();

            return;
        }

        var selects = box.getElementsByTagName("select");
        var fromPort = selects[0].value;
        var toPort = selects[1].value;

        var cable = new Network.Cable(designer, structure, line, from, to, fromPort, toPort);
        designer.cables.push(cable);

        from.ports[fromPort] = cable;
        from.cables.push(cable);
        from.structure.avaible--;

        to.ports[toPort] = cable;
        to.cables.push(cable);
        to.structure.avaible--;

        message("Cable " + structure.name + " was connected.");
    };

    new Network.ModalDialog("Connect devices via " + structure.name,
            Network.ModalDialog.formBox(selectFrom + selectTo), ["Connect", "Cancel"],
            callback, 500, "select");
};

/**
 * Create select for modal dialogs which serves to managing cables.
 * 
 * @param {Network.Device} device device for which will be created select which serves to port selection.
 * @param {integer} selected index of selected element.
 * @returns {String} Created select element for formular.
 * @function createSelect
 * @static
 */
Network.CreateCableDialog.createSelect = function(device, selected) {
    /** @type String */
    var select = "  <div><label>Port of " + device.structure.name + ": <select id='from-port' class='inform port-select'>\n";
    for (var i = 0; i < device.ports.length; i++) {
        if (device.ports[i] === null) {
            select += "      <option value='" + i + "'" + (i == selected ? " selected='selected'" : "") + ">eth" + i + "</option>\n";
        }
    }
    select += " </select></label></div>\n";

    return select;
};

/**
 * Constructs and shows dialog for editing cable.
 * 
 * @param {Network.Cable} cable cable to edit.
 * @class EditCableDialog
 * @constructor
 */
Network.EditCableDialog = function(cable) {
    /** @type integer */
    var fromPort = cable.getFromPort();
    /** @type integer */
    var toPort = cable.getToPort();

    /** @type Network.Device */
    var from = cable.getFromDevice();
    /** @type Network.Device */
    var to = cable.getToDevice();

    from.ports[fromPort] = null;
    to.ports[toPort] = null;

    /** @type String */
    var selectFrom = Network.CreateCableDialog.createSelect(from, fromPort);
    /** @type String */
    var selectTo = Network.CreateCableDialog.createSelect(to, toPort);

    /**
     * Callback function for modal dialog which serves to editing cable.
     * 
     * @param {HTMLDivElement} box modal box of dialog.
     * @param {String} button number of button which was pressed in dialog.
     * @function callback
     */
    var callback = function(box, button) {        
        if (parseInt(button) === 1) {
            from.ports[fromPort] = cable;
            to.ports[toPort] = cable;

            return;
        }

        var selects = box.getElementsByTagName("select");
        fromPort = selects[0].value;
        toPort = selects[1].value;
        
        from.ports[fromPort] = cable;
        to.ports[toPort] = cable;
        
        cable.setFromPort(fromPort);
        cable.setToPort(toPort);
        cable.update();

        message("Cable " + cable.structure.name + " was edited.");
    };

    new Network.ModalDialog("Edit cable " + cable.structure.name,
            Network.ModalDialog.formBox(selectFrom + selectTo), ["Edit", "Cancel"],
            callback, 500, "select");
};

/**
 * This class serves for managing networks in database.
 * 
 * @param {Network.Designer} designer designer which will be managed.
 * @class Management
 * @constructor
 */
Network.Management = function(designer) {
    /** @type Network.Management */
    var instance = this;
    /** @type Array */
    var devicesData = null;
    /** @type Array */
    var cablesData = null;
    /** @type Array. */
    var interfacesData = null;
    /** @type HTMLDivElement */
    this.box = null;
    
    /**
     * Escapes characters '\' ("\\") and ';' ("\;") in entered variable.
     * 
     * @param {*} variable any type of variable which will changed into string 
     * which will be escaped.
     * @return {String} Escaped value of entered variable.
     * @function escape
     */
    this.escape = function(variable) {
        var str = "" + variable;
        var ret = "";
        for(var i = 0; i < str.length; i++) {
            var c = str.charAt(i);
            
            if(c === "\\" || c === ";") {
                ret += "\\";
            }
            
            ret += c;
        }
        
        return ret;
    };
    
    /**
     * Parses data obtained from database into array.
     * 
     * @param {String} data data obtained from database.
     * @return {Array} Array which contains parsed data.
     * @function parse
     */
    this.parse = function(data) {
        var atom = "";
        var parsed = new Array();
        if(data.length === 0) {
            return parsed;
        }
        
        var esc = false;
        for(var i = 0; i < data.length; i++) {
            var c = data.charAt(i);
            if(c === "\\") {
                if(esc) {
                    atom += "\\";
                    esc = false;
                }
                else {
                    esc = true;
                }
                
                continue;
            }
            
            if(!esc && c === ";") {
                parsed.push(atom);
                atom = "";
                
                continue;
            }
            
            if(esc && c !== ";") {
                return false;
            }
            else {
                esc = false;
            }
            
            atom += c;
        }
        
        parsed.push(atom);
        
        return parsed;
    };

    /**
     * This function has variable number of arguments from which creates string 
     * which is used for sending data into database.
     * @function toSaveString
     */
    this.toSaveString = function() {
        var ret = "";
        for(var i = 0; i < arguments.length; i++) {
            if(i > 0) {
                ret += ";";
            }
            
            ret += this.escape(arguments[i]);
        }
        
        return ret;
    };

    /**
     * Saves network with entered id into database.
     * 
     * @param {integer} id identifier of network.
     * @function saveAction
     */
    var saveAction = function(id) {
        var network = "network=" + instance.toSaveString(designer.configurationName, 
                designer.getName(), designer.getWidth(), designer.getHeight(), 
                designer.getBackground());

        var devices = "devices=";
        var interfaces = "interfaces=";
        var len = designer.devices.length;
        for (var i = 0; i < len; i++) {
            /** @type Network.Device */
            var device = designer.devices[i];
            /** @type Network.DeviceStructure */
            var structure = device.structure;
            devices += instance.toSaveString(i, structure.id, structure.label, 
                    device.getX(), device.getY(), structure.getPorts());
            
            var ports = structure.getPorts();
            for(var j = 0; j < ports; j++) {
                var interface = device.interfaces[j];
                interfaces += instance.toSaveString(i, interface.ip, interface.mask, 
                        interface.vlans, interface.info);
                if(j < ports - 1) {
                    interfaces += ";";
                }
                if(j == ports - 1 && i < len - 1) {
                    interfaces += ";";
                }
            }
                    
            if(i < len - 1) {
                devices += ";";
            }
            
            structure.idInDesigner = i;
        }
        
        var cables = "cables=";
        var len = designer.cables.length;
        for (var i = 0; i < len; i++) {
            /** @type Network.Cable */
            var cable = designer.cables[i];
            /** @type Network.CableStructure */
            var structure = cable.structure;
            /** @type Network.Device */
            var from = cable.getFromDevice();
            /** @type Network.Device */
            var to = cable.getToDevice();
            
            cables += instance.toSaveString(structure.id, from.structure.idInDesigner, 
                to.structure.idInDesigner, cable.getFromPort(), cable.getToPort());
            
            if(i < len - 1) {
                cables += ";";
            }
        }

        $.ajax({
            type: "POST",
            url: "manager.php?save=" + id,
            data: network + "&" + devices + "&" + interfaces + "&" + cables,
            success: function(resp) {                
                var splitted = resp.split(";");
                if(splitted[0] === "-1") {
                    error(splitted[1]);
                }
                else {
                    designer.id = parseInt(splitted[0]);
                    if(designer.id != id) {
                        $("#networks").append("<option value='" + designer.id + "' selected='selected'>" + designer.configurationName + "</option>");
                    }
                    message(splitted[1]);
                }
            },
                    
            error: function(e) {
                error("An error occurred during saving of network: " + e);
            }
        });
    };

    /**
     * Saves network in designer into database.
     * @function save
     */
    this.save = function() {
        if(designer.id > 0) {
            dhtmlx.confirm({
                title: "Save network",
                type: "confirm-warning",
                text: "Are you sure you want to save changes in this network?",
                callback: function(button) {
                    if (!button) {
                        return;
                    }

                    saveAction(designer.id);
                }
            });
        }
        else {
            this.saveAs();
        }
    };
    
    /**
     * Prompt user for network name and then calls saveAction.
     * @function saveAs
     */
    this.saveAs = function() {
        var name = designer.getName();
        if(designer.configurationName !== null) {
            name = designer.configurationName;
        }
        
        var text = Network.ModalDialog.formBox(Network.ModalDialog.formLine("Network configuration name", 
                    "network-name", name, "maxlength='100'"));

        var callback = function(box, button) {
            if (parseInt(button) === 1) {
                return;
            }

            var inputs = box.getElementsByTagName("input");
            var input = inputs[0];
            if (input.value === "") {
                error("Network configuration name cannot be empty.");
                return;
            }

            designer.configurationName = input.value;
            saveAction(-1);
        };

        new Network.ModalDialog("Save network", text, ["Save", "Cancel"], callback);
    };
    
    /**
     * Creates cable from entered arguments.
     * 
     * @param {Array} args arguments for device.
     * @function createCable
     */
    var createCable = function(args) {
        var structure = Network.Cables.cables[args[0]].structure;
        
        var from = designer.devices[args[1]];
        var to = designer.devices[args[2]];
        
        var x = from.getX() + (from.getWidth() / 2);
        var y = from.getY() + 5;
        var line = new Kinetic.Line({
            points: [x, y, x, y],
            stroke: structure.color,
            strokeWidth: 5,
            lineCap: 'round'
        });

        if (structure.line === "dashed") {
            line.setDashArray([20, 10]);
        }

        if (structure.line === "dotted") {
            line.setDashArray([0.001, 20]);
        }
        
        designer.addToBottom(line);
        
        var fromPort = args[3];
        var toPort = args[4];
        
        var cable = new Network.Cable(designer, structure, line, from, to, fromPort, toPort);
        designer.cables.push(cable);

        from.ports[fromPort] = cable;
        from.cables.push(cable);
        from.structure.avaible--;

        to.ports[toPort] = cable;
        to.cables.push(cable);
        to.structure.avaible--;

        message("Cable " + structure.name + " was connected.");
    };
    
    /**
     * Crates cables from entered cables string.
     * 
     * @function createCables.
     */
    var createCables = function() {
        if(cablesData.length > 0) {
            for(var i = 0; i < cablesData.length; i++) {
                createCable(cablesData[i]);
            }
        } 
        
        cablesData = null;
    };
    
    var createInterface = function(args) {
        var device = designer.devices[args[0]];
        var i = device.interfaces.length;
        var interface = new Network.Interface();
        interface.ip = args[1];
        interface.mask = args[2];
        interface.vlans = args[3];
        interface.info = args[4];
        
        device.interfaces.push(interface);
        message("Interface eth" + i + " in device " + device.getLabel() + " was configured.");
    };
    
    /**
     * Crates cables from entered cables string.
     * 
     * @function createCables.
     */
    var createInterfaces = function() {
        if(interfacesData.length > 0) {
            for(var i = 0; i < interfacesData.length; i++) {
                createInterface(interfacesData[i]);
            }
        } 
        
        interfacesData = null;
    };
    
    /**
     * Create device which has arguments with entered index.
     * 
     * @param {integer} index index for arguments of this device.
     * @function createDevice
     */
    var createDevice = function(index) {
        var args = devicesData[index];
        var x = args[3];
        var y = args[4];
        var imageObj = new Image();
        imageObj.onload = function() {
            var structure = new Network.DeviceStructure(args[0], args[2], this, args[5]);
            var device = designer.addDevice(structure);
            device.interfaces = new Array();
            
            device.setX(parseInt(x));
            device.setY(parseInt(y));
            
            if(index + 1 < devicesData.length) {
                createDevice(index + 1);
            }
            else {
                devicesData = null;
                createInterfaces();
                createCables();
            }
        };
        imageObj.src = args[1];
    };
    
    /**
     * Crates devices from entered devices string.
     * 
     * @function createDevices.
     */
    var createDevices = function() {
        if(devicesData.length > 0) {
            createDevice(0);
        }
    };
    
    /**
     * Action for loading network.
     * 
     * @param {integer} id identifier of network which will be loaded from database.
     * @function loadAction
     */
    var loadAction = function(id) {
        var data = null;
        
        $.ajax({
            type: "GET",
            url: "manager.php?load=" + id,
            success: function(resp) {
                if(resp.startsWith("-1;")) {
                    var splitted = resp.split(";");
                    error(splitted[1]);
                }
                else {
                    data = resp;
                    
                    var splitted = data.substring(6, data.length).split("\n");
                    var network = instance.parse(splitted[0]);
                    designer.id = id;
                    designer.configurationName = network[0];
                    designer.erase(network[1], network[2], network[3], network[4], true);
                    
                    var devices = instance.parse(splitted[1]);
                    if(devices.length > 0) {
                        devicesData = new Array();
                        for(var i = 0; i < devices.length / 6; i++) {
                            var args = new Array();
                            var k = i * 6;
                            for(var j = 0; j < 6; j++) {
                                args[j] = devices[k+j];
                            }

                            devicesData.push(args);
                        }
                        
                        var interfaces = instance.parse(splitted[2]);
                        interfacesData = new Array();
                        if(interfaces.length > 0) {
                            for(var i = 0; i < interfaces.length / 5; i++) {
                                var args = new Array();
                                var k = i * 5;
                                for(var j = 0; j < 5; j++) {
                                    args[j] = interfaces[k+j];
                                }
                                interfacesData.push(args);
                            }
                        }
                        
                        var cables = instance.parse(splitted[3]);
                        cablesData = new Array();
                        if(cables.length > 0) {
                            for(var i = 0; i < cables.length / 5; i++) {
                                var args = new Array();
                                var k = i * 5;
                                for(var j = 0; j < 5; j++) {
                                    args[j] = cables[k+j];
                                }
                                cablesData.push(args);
                            }
                        }
                        
                        createDevices();
                    }
                }
            },
                    
            error: function(e) {
                error("An error occurred during loading of network: " + e);
            }
        });
    };
    
    /**
     * Loads network with entered id from database.
     * 
     * @param {integer} id identifier of network which will be loaded from database.
     * @param {boolean} prompt information whether will be shown prompt dialog.
     * @function load
     */
    this.load = function(id, prompt) {
        if(prompt === undefined || prompt === false) {
            loadAction(id);
        }
        else {
            dhtmlx.confirm({
                title: "Load network",
                type: "confirm-warning",
                text: "Are you sure you want to load selected network?",
                callback: function(button) {             
                    if (!button) {
                        return;
                    }

                    loadAction(id);
                }
            });
        }
    };
    
    /**
     * Removes network with entered id from database.
     * 
     * @param {integer} id identifier of network which will be removed from database.
     * @function remove
     */
    this.remove = function(id) {
        dhtmlx.confirm({
            title: "Remove network",
            type: "confirm-warning",
            text: "Are you sure you want to delete selected network?",
            callback: function(button) {             
                if (!button) {
                    return;
                }
                
                $.ajax({
                    type: "GET",
                    url: "manager.php?remove=" + id,
                    success: function(resp) {
                        var splitted = resp.split(";");
                        if(splitted[0] === "-1") {
                            error(splitted[1]);
                        }
                        else {
                            message(splitted[1]);
                            $("select#networks option[value='" + id + "']").remove();
                        }
                    },

                    error: function(e) {
                        error("An error occurred during removing of network: " + e);
                    }
                });
            }
        });
    };
    
    /**
     * Exports network with entered id from database.
     * 
     * @param {integer} id identifier of network which will be exported from database.
     * @function export
     */
    this.export = function(id) {
        var dlif = $('<iframe/>' , {'src':"manager.php?export=" + id}).hide();
        $("body").append(dlif);
    };
    
    /**
     * Shows dialog for importing network from XML file.
     * @function import
     */
    this.import = function() {
        /** @type String */
        var text = "<form id='import-form' name='form' action='' method='post' enctype='multipart/form-data'>" + 
            Network.ModalDialog.formBox(
                Network.ModalDialog.formLine("Network file", "fileToUpload", undefined, 
                    "onchange='window.designer.management.validateInput(this);' name='fileToUpload'", "file")
            ) + 
            "   <div class='dhtmlx_button'>\n\
        <input type='button' name='file' value='Import' onclick='return window.designer.management.importAction();' />\n\
    </div>\n\
    <div class='dhtmlx_button'>\n\
        <input type='button' name='cancel' value='Cancel' onclick='dhtmlx.modalbox.hide(window.designer.management.box); return true;' />\n\
    </div>\n\
</form>\n\
<img id='loading' src='../images/loading.gif' />";
        
        this.box = new Network.ModalDialog("Import network", text, null);
    };
    
    /**
     * Validates entered file in file input.
     * 
     * @param {HTMLInputElement} input file input which contains file for import.
     * @return {boolean} information whether is entered file valid - XML file.
     * @function validateInput
     */
    this.validateInput = function(input) {
        var file = input.files[0];
        if(file.type !== 'text/xml') {
            errorDialog("Invalid network file", "Entered file: " + file.name + " is invalid. Expected XML file (*.xml).");
            input.value = "";
            return false;
        }
        
        return this;
    };
    
    /**
     * Import action.
     * @function importAction
     */
    this.importAction = function() {
        if($("#fileToUpload").val() == "") {
            dhtmlx.modalbox.hide(instance.box);
            error("No file was selected.");
            return false;
        }
        
        $(instance.box).hide();
        $.ajaxFileUpload({
            url:'manager.php?import',
            secureuri:false,
            fileElementId: 'fileToUpload',
            dataType: 'json',
            data:{name:'logan', id:'id'},
            success: function (data, status) {
                dhtmlx.modalbox.hide(instance.box);
                if(typeof(data.error) != 'undefined') {
                    if(data.error != '') {
                        error(data.error);
                    } 
                    else {
                        var splitted = data.msg.split(";");
                        if(splitted[0] === "-1") {
                            error(splitted[1]);
                        }
                        else {
                            for(var i = 0; i < splitted.length; i+=3) {
                                var id = splitted[i];
                                if(id === "-1") {
                                    error(splitted[i+1]);
                                }
                                else {
                                    $("#networks").append("<option value='" + id + "' selected='selected'>" + splitted[i+1] + "</option>");
                                    message(splitted[i+2]);
                                }
                            }
                        }
                    }
                }
            },  
            error: function(data, status, e) {
                dhtmlx.modalbox.hide(instance.box);
                error("An error occurred during importing of network: " + e);
            }
        });
        
        return false;
    };
};
<?php
session_start();
include_once("./php/config.php");
include_once(PHP_DIR . "libs/Smarty/Smarty.class.php");

$page = new DesignerPage();
$page->init()->printOut();

if(MySQL::is_opened()) {
    MySQL::get_instance()->close();
}
?>

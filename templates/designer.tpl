        <div id="page">
{include file="./logo.tpl" admin="false"}

            <div id="body">      
                <div id="designer-actions">                    
                    <label for="networks">Created networks:</label>
                    <select id="networks" name="networks">
{if isset($networks) && $networks neq false}
{foreach from=$networks item=network}
                        <option value="{$network[0]}">{$network[1]}</option>
{/foreach}
{/if}
                    </select>
                    
                    <button onclick="designer.load();" {include file="./title.tpl" titleHeader="Load network"}>
                        <img src="{$smarty.const.ROOT}images/load.png" alt="Load network" />
                    </button>
                    
                    <button onclick="designer.importNetwork();" {include file="./title.tpl" titleHeader="Import network..."}>
                        <img src="{$smarty.const.ROOT}images/import.png" alt="Import network..." />
                    </button> 
                        
                    <button onclick="designer.exportNetwork();" {include file="./title.tpl" titleHeader="Export network..."}>
                        <img src="{$smarty.const.ROOT}images/export.png" alt="Export network..." />
                    </button>  
                        
                    <button onclick="designer.showImage();" {include file="./title.tpl" titleHeader="Show network as image"}>
                        <img src="{$smarty.const.ROOT}images/present.png" alt="Show etwork as image" />
                    </button>

                    <span class="separator"></span>
                    
                    <button onclick="location.href = './admin/'" {include file="./title.tpl" titleHeader="Administration"}>
                        <img src="{$smarty.const.ROOT}images/admin.png" alt="Administration" />
                    </button>
                        
                    <button onclick="location.href = '{basename($smarty.server.PHP_SELF)}?logout'" {include file="./title.tpl" titleHeader="Logout"}>
                        <img src="{$smarty.const.ROOT}images/logout.png" alt="Logout" />
                    </button>
                </div>
                        
                <div id="designer-container">
                    <div id="designer"></div>
                </div>
            </div> 
        </div>
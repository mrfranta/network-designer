        <div id="login-page">
            <img id="logo" src="{$smarty.const.ROOT}images/logo-plain.png" alt="Network designer" />
{if $error eq true}
            <span class="error">Wrong username or password!</span>
{/if}
            
            <form method="post" action="{$smarty.server.REQUEST_URI}">
                <div class="form-line">
                    <label for="user">User name:</label>
                    {literal}<input id="user" name="user" type="text" 
                           onfocus="if(this.value === 'Your login name') {this.value = '';}; return true;" 
                           onblur="if(this.value === '') {this.value='Your login name';}; return true;" 
                           value="Your login name" />{/literal}
                </div>
                <div class="form-line">
                    <label for="password">Password:</label>
                    <input id="password" name="password" type="password" />
                </div>
                <div class="form-line">
                    <input id="login" name="login" type="submit" value="login" />
                </div>
            </form>
        </div>
        <div id="page">
{include file="../logo.tpl" admin="false"}
            <div id="body">
                <div id="designer-categories">
                    <ul class="tabs">
{section name=category loop=$categories}
                       <li><a href="#tab{$smarty.section.category.index + 1}" {include file="./title.tpl" titleHeader=$categories[category].category_name}><img src="{$smarty.const.ROOT}{$categories[category].category_image}" alt="{$categories[category].category_name}" /></a></li> 
{/section}
                    </ul>
                    <div id="designer-categories-devives">
{foreach from=$devices item=devicesInCategory}
                        <div id="tab{$devicesInCategory@iteration}" class="tab">
{if count($devicesInCategory) > 0}
{foreach from=$devicesInCategory item=device}
                            <button {include file="./title.tpl" titleHeader=$device->name} name="{$device->name}" onclick="designer.createDevice(this, {$device->id}, {$device->ports});">
                                <img src="{$smarty.const.ROOT}{$device->image}" alt="{$device->name}" class="device" />
                            </button>
{/foreach}
{/if}
                        </div>
{/foreach}
                        <div id="tab{$cablesTab}" class="tab">
{foreach from=$cables item=cable}
                            <button {include file="./title.tpl" titleHeader=$cable->name} name="{$cable->name}" onclick="designer.setCable(this, Network.Cables.{$cable->constantName});">
                                <img src="{$smarty.const.ROOT}{$cable->image}" alt="{$cable->name}" class="cable" />
                            </button>
{/foreach}
                        </div>
                    </div>
                </div>
                        
                <div id="designer-actions">
                    <button onclick="designer.changeConfiguration();" {include file="./title.tpl" titleHeader="Change configuration"}>
                        <img src="{$smarty.const.ROOT}images/config.png" alt="Change configuration" />
                    </button>
                    <button onclick="designer.clear();" {include file="./title.tpl" titleHeader="Erase designer"}>
                        <img src="{$smarty.const.ROOT}images/erase.png" alt="Erase designer" />
                    </button>
                    <button onclick="designer.showImage();" {include file="./title.tpl" titleHeader="Show network as image"}>
                        <img src="{$smarty.const.ROOT}images/present.png" alt="Show etwork as image" />
                    </button>
                    <button onclick="designer.setDeletionAction(this);" {include file="./title.tpl" titleHeader="Element removal tool"}>
                        <img src="{$smarty.const.ROOT}images/remove.png" alt="Element removal tool" />
                    </button>
                    
                    <span class="separator"></span>
                    
                    <label for="networks">Created networks:</label>
                    <select id="networks" name="networks">
{if isset($networks) && $networks neq false}
{foreach from=$networks item=network}
                        <option value="{$network[0]}">{$network[1]}</option>
{/foreach}
{/if}
                    </select>
                    <button onclick="designer.load(true);" {include file="./title.tpl" titleHeader="Load network"}>
                        <img src="{$smarty.const.ROOT}images/load.png" alt="Load network" />
                    </button>
                        
                    <button onclick="designer.importNetwork();" {include file="./title.tpl" titleHeader="Import network..."}>
                        <img src="{$smarty.const.ROOT}images/import.png" alt="Import network..." />
                    </button> 
                        
                    <button onclick="designer.exportNetwork();" {include file="./title.tpl" titleHeader="Export network..."}>
                        <img src="{$smarty.const.ROOT}images/export.png" alt="Export network..." />
                    </button>                        
                        
                    <button onclick="designer.remove();" {include file="./title.tpl" titleHeader="Remove network"}>
                        <img src="{$smarty.const.ROOT}images/rem.png" alt="Remove network" />
                    </button> 
                    <button onclick="designer.save();" {include file="./title.tpl" titleHeader="Save network"}>
                        <img src="{$smarty.const.ROOT}images/save.png" alt="Save network" />
                    </button>
                    <button onclick="designer.saveAs();" {include file="./title.tpl" titleHeader="Save network as..."}>
                        <img src="{$smarty.const.ROOT}images/save_as.png" alt="Save network as..." />
                    </button>
                    
                    <span class="separator"></span>
                    
{if $smarty.session.admin gt 0}
                    <button onclick="location.href = 'index.php'" {include file="./title.tpl" titleHeader="Administration"}>
                        <img src="{$smarty.const.ROOT}images/admin.png" alt="Administration" />
                    </button>
{/if}
                    <button onclick="location.href = '{basename($smarty.server.PHP_SELF)}?logout'" {include file="./title.tpl" titleHeader="Logout"}>
                        <img src="{$smarty.const.ROOT}images/logout.png" alt="Logout" />
                    </button>
                </div>
                        
                <div id="designer-container">
                    <div id="designer"></div>
                </div>
            </div> 
        </div>
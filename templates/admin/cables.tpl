{if !empty($javascriptData)}
        <script type="text/javascript">
            Network.Cables = {ldelim}
{foreach from=$javascriptData item=cable}
                {$cable->constantName}: {ldelim}structure: new Network.CableStructure("{$cable->id}", "{$cable->name}", "{$cable->color}", "{$cable->line}"){rdelim}{if !$cable@last},{/if}

{/foreach}
            {rdelim};
                
            Network.Cables.cables = {ldelim}
{foreach from=$javascriptData item=cable}
                {$cable->id}: Network.Cables.{$cable->constantName}{if !$cable@last},{/if}

{/foreach}
            {rdelim};
        </script>
{/if}
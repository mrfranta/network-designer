        <div id="admin-page">
{include file="../logo.tpl" admin="true"}

            <div id="admin-body"> 
{include file="./menu.tpl"}
                
                <h1>{$header}</h1>
            
                <div id="admin-content">
{if isset($content)}
{include file="./{$content}.tpl"}  
{/if}
                </div>
            </div>

{include file="../footer.tpl" admin="true"}   
        </div>
                    <table class="table-data">
                        <tr>
{section name=column loop=$tableHeader}
                            <th{if $tableHeader[column]->width gt -1} style="width: {$tableHeader[column]->width}px;"{/if}>{$tableHeader[column]->name}</th>
{/section}
                            <th style="width: 75px">Deletion</th>
                            <th style="width: 75px">Editing</th>                                          
                        </tr>
{section name=row loop=$tableData}
{assign var="showDelete" value="true"}
{assign var="showEdit" value="true"}
    
                        <tr class="{cycle values='odd-row,even-row'}">
{foreach from=array_keys($tableData[row]) item=col}
{if is_numeric($col)}
{if isset($noDelete) && array_key_exists($col, $noDelete)}
{if $noDelete[$col] eq $tableData[row][$col]}
{assign var="showDelete" value="false"}
{else}
{assign var="showDelete" value="true"}
{/if}
{/if}
{if isset($noEdit) && array_key_exists($col, $noEdit)}
{if $noEdit[$col] eq $tableData[row][$col]}
{assign var="showEdit" value="false"}
{else}
{assign var="showEdit" value="true"}
{/if}
{/if}
                            <td>{$tableData[row][$col]}</td>           
{/if}
{/foreach}
                            <td>
{if {$showDelete} == "true"}
                                <a href="{$smarty.server.REQUEST_PAGE}&amp;action=delete&amp;id={$tableData[row][0]}" {include file="../title.tpl" titleHeader="Delete {$elementName}"}><img src="../images/delete.png" alt="Delete" /></a>
{else}
                                <span {include file="../title.tpl" titleHeader="Cannot be deleted"}>-</span>
{/if}
                            </td>
                            <td>
{if {$showEdit} == "true"}
                                <a href="{$smarty.server.REQUEST_PAGE}&amp;action=edit&amp;id={$tableData[row][0]}" {include file="../title.tpl" titleHeader="Edit {$elementName}"}><img src="../images/edit.png" alt="Edit" /></a>
{else}
                                <span {include file="../title.tpl" titleHeader="Cannot be edited"}>-</span>
{/if}
                            </td>
{/section}
                    </table>             
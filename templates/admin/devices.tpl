                    <h2>{$formTitle}</h2>
{include file="./form.tpl"}

                    <h2>{$tableTitle}</h2>
                    <ul class="tabs">
{section name=category loop=$categories}
                            <li><a href="#tab{$smarty.section.category.index + 1}" {include file="./title.tpl" titleHeader=$categories[category].category_name}><img src="{$smarty.const.ROOT}{$categories[category].category_image}" alt="{$categories[category].category_name}" /></a></li> 
{/section}
                    </ul>
                    <div id="categories-devives">
{foreach from=$categories item=category}
                        <div id="tab{$category@iteration}" class="tab">
{if count($category.devices) > 0}
    {include file="./table.tpl" tableData=$category.devices}
{else}
                            <p class="none">(none)</p>
{/if}
                        </div>
{/foreach}
                    </div>
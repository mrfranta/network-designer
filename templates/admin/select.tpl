                            <div class="form-line">
{if !empty({$element->label})}
                                <label for="{$element->id}">{$element->label}</label>
{/if}
                                <div class="form-field">
                                    <select id="{$element->id}"{if !empty($element->class)} class="{$element->class}"{/if} name="{$element->name}"{if !empty({$element->addition})} {$element->addition}{/if}>
{foreach from=$element->options item=elementOption}
                                        <option value="{$elementOption->value}"{if {$element->selected} eq {$elementOption->value}} selected="selected"{/if}>{$elementOption->content}</option>
{/foreach}
                                    </select>
                                </div>
{if !empty({$element->info}) && empty({$element->error})}
                                <img src="../images/info.png" alt="information" {include file="../title.tpl" titleHeader="Information" titleBody={$element->info}} />
{/if}
{if !empty({$element->error})}
                                <img src="../images/error.png" alt="error" {include file="../title.tpl" titleHeader="Error" titleBody={$element->error}} />
{/if}
                            </div>
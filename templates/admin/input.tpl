                            <div class="form-line">
{if !empty({$element->label})}
                                <label for="{$element->id}">{$element->label}</label>
{/if}
{if {$element->type} neq 'submit'}
                                <div class="form-field">
{/if}
                                    <input id="{$element->id}"{if !empty({$element->class})} class="{$element->class}"{/if} name="{$element->name}" type="{$element->type}"{if !empty({$element->value})} value="{$element->value}"{/if}{if !empty({$element->addition})} {$element->addition}{/if} />
{if {$element->type} neq 'submit'}
                                </div>
{else}
                                    <input id="cancel" name="cancel" type="submit" value="Cancel" />  
{/if}
{if !empty({$element->info}) && empty({$element->error})}
                                <img src="../images/info.png" alt="information" {include file="../title.tpl" titleHeader="Information" titleBody="{$element->info}"} />
{/if}
{if !empty({$element->error})}
                                <img src="../images/error.png" alt="error" {include file="../title.tpl" titleHeader="Error" titleBody="{$element->error}"} />
{/if}
                            </div>
                    <div id="form-container">
                        <form method="post" action="{$smarty.server.REQUEST_URI}"{if isset($formEnctype)} enctype="{$formEnctype}"{/if}>
{foreach from=$formElements item=element}
{include file="./{$element->getTemplate()}.tpl" element=$element}
{/foreach}
                        </form>
                    </div>
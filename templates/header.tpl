    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="Network designer" />
        <meta name="keywords" content="network, designer" />

{if isset($robots) && $robots eq 'true'}
        <meta name="robots" content="index, follow, snippet, archive" />
        <meta name="googlebot" content="index, follow, snippet, odp, archive, imageindex" />
{else}
        <meta name="robots" content="noindex, nofollow, nosnippet, noarchive" />
        <meta name="googlebot" content="noindex, nofollow, nosnippet, noodp, noarchive, noimageindex" />
{/if}

        <meta name="author" content="Mr.FrAnTA (Michal Dékány)" />
        <meta name="generator" content="NetBeans IDE 7.3" />

        <title>{$title}</title>
{assign var="path" value="{$smarty.const.ROOT}"}

        <link rel="stylesheet" type="text/css" href="{$path}style/style.css" />
        <link rel="stylesheet" type="text/css" href="{$path}js/message/themes/message_skyblue.css" />
        <!--[if lte IE 8]>
            <link rel="stylesheet" type="text/css" href="{$path}style/iecss.css" />
        <![endif]-->
        <link rel="stylesheet" media="print" type="text/css" href="{$path}style/print.css" />
        
        <script type="text/javascript" src="{$path}js/boxOver/boxOver.js"></script>
        <script type="text/javascript" src="{$path}js/message/message.js"></script>
        <script type="text/javascript" src="{$path}js/jQuery/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="{$path}js/jQuery/jquery-blockUI.js"></script>
{if $designer eq true}
        <script type="text/javascript" src="{$path}js/ajaxfileupload/ajaxfileupload.js"></script>
        <script type="text/javascript" src="{$path}js/kinetic/kinetic-v4.7.4.js"></script>
        <script type="text/javascript" src="{$path}js/network_designer.js"></script>
{/if}
{if ($admin eq true)}
        <script type="text/javascript" src="{$path}js/jscolor/jscolor.js"></script>
{/if}
{if ($admin eq true && $designer eq true)}
        <script type="text/javascript" src="{$path}js/network_creator.js"></script>
{/if}
{if ($admin eq false && $designer eq true)}
        <script type="text/javascript" src="{$path}js/network_viewer.js"></script>
{/if}
        <script type="text/javascript" src="{$path}js/main.js"></script>
{if isset($javascripts)}
{foreach from=$javascripts item=javascript}
{include file="./{$javascript->template}.tpl" javascriptData=$javascript->data}
{/foreach}
{/if}

        <link rel="shortcut icon" href="{$path}images/favicon.png" type="image/png" />
    </head>
<!DOCTYPE html>
<html lang="cs">
{include file="./header.tpl"}
    <body>
        <div id="noscript">
            To use this pages, you must have javascript enabled!
        </div>
        
{include file="{$body}.tpl"}
    </body>
</html>
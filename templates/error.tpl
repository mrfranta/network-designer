        <div id="page">
{include file="./logo.tpl"}
            <div id="body">  
                <div id="error-content">
                    <h1>{$header}</h1>

                    <p>{$message}</p>
                    
                    <button id="go-back" onclick="goBackInHistory()" {include file="./title.tpl" titleHeader="Network designer"}>&lt; Go back</button>
                </div>
            </div>
{include file="./footer.tpl"} 
        </div>
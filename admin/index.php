<?php
session_start();
include_once("../php/config.php");
include_once(PHP_DIR . "libs/Smarty/Smarty.class.php");

$page_param = "";
if(isset($_GET["page"])) {
    $page_param = $_GET["page"];
}

$page = new AdminPage($page_param);
$page->init()->printOut();

if(MySQL::is_opened()) {
    MySQL::get_instance()->close();
}
?>

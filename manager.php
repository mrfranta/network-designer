<?php
session_start();
include_once("./php/config.php");

try {    
    if(isset($_GET["export"])) {
        $manager = new NetworksExporter();
    }
    else if(isset($_GET["import"])) {
        $manager = new NetworksImporter();
    }
    else {
        $manager = new NetworksManager();
    }
    
    $manager->manage();
} catch(Exception $exc) {
    echo "-1;{$exc->getMessage()}";
}

if(MySQL::is_opened()) {
    MySQL::get_instance()->close();
}
?>

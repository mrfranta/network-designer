<?php
/**
 * This class represents menu.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 *
 * @property string $name Name of this menu.
 * @property-read array $items Array of menu items in this menu.
 */
class Menu {
    /**
     * Name of this menu.
     * @var string
     */
    private $name;
    /**
     * Array of menu items in this menu.
     * @var array
     */
    private $items;
    
    /**
     * Constructs menu.
     * 
     * @param string $name name of this menu.
     * @param MenuItem|array $items one or more menu items.
     * @return Menu Instance of this menu.
     */
    public function __construct(/*string*/ $name = "", /*array|MenuItem*/ $items = null) {
        $this->name = $name;
        $this->items = array();
        if($items != null) {
            $this->addMenuItem($items);
        }
        
        return $this;
    }

/**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        if($name === "items") {
            return;
        }
        unset($this->$name);
    }
    
    /**
     * Returns name of this menu.
     * 
     * @return string Name of this menu.
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Sets name of this menu.
     * 
     * @param string $name name of this menu.
     * @return Menu Instance of this menu.
     */
    public function setName(/*string*/ $name = "") {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * Adds one or more menu items into this menu.
     * 
     * @param MenuItem|array $item one or more menu items.
     * @return Menu Instance of this menu.
     */
    public function addMenuItem(/*MenuItem*/ $item) {
        if(is_array($item)) {
            foreach($item as $itm) {
                if($itm instanceof MenuItem) {
                    $this->items[] = $itm;
                }
            }
        }
        else if(is_object($item) && $item instanceof MenuItem) {
            array_push($this->items, $item);
        }
        
        return $this;
    }
    
    /**
     * Removes one or more menu items from this menu.
     * 
     * @param MenuItem|array $item one or more menu items.
     * @return Menu Instance of this menu.
     */
    public function removeMenuItem(/*MenuItem*/ $item) {
        if(is_array($item)) {
            foreach($item as $itm) {
                if(($index = array_search($itm, $this->items)) !== false) {
                    unset($this->items[$index]);
                }
            }
        }
        else if(($index = array_search($item, $this->items)) !== false) {
            unset($this->items[$index]);
        }
        
        return $this;
    }
    
    /**
     * Returns menu items in this menu.
     * 
     * @return array Menu items in this menu.
     */
    public function getItems() {
        return $this->items;
    }
}
?>

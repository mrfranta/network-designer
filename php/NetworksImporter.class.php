<?php
/**
 * Description of NetworkImporter
 *
 * @author Mr.FrAnTA
 */
class NetworksImporter extends NetworksManagement {
    private $parser;
    
    private $network = array();
    private $devices = array();
    private $interfaces = array();
    private $cables = array();
    private $stack = array();
    
    private $inRoot = false;
    private $inDevices = false;
    private $inDevice = false;
    private $inInterfaces = false;
    private $inInterface = false;
    private $inCables = false;
    private $inCable = false;
    
    private $imported = array();
    
    const ROOT = "network";
    const DEVICES = "devices";
    const DEVICE = "device";
    const INTERFACES = "interfaces";
    const _INTERFACE = "interface";
    const CABLES = "cables";
    const CABLE = "cable";
    
    const ELEMENT_NAME = "fileToUpload";
    
    public function __construct() {
        parent::__construct();
    }
    
    private function uploadFile() {
        $error = "";
	$fileElementName = self::ELEMENT_NAME;
        if(!empty($_FILES[$fileElementName]['error'])) {
            switch($_FILES[$fileElementName]['error']) {
                case '1':
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
                    break;
                
                case '2':
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
                    break;
                
                case '3':
                    $error = 'The uploaded file was only partially uploaded.';
                    break;
                
                case '4':
                    $error = 'No file was uploaded.';
                    break;

                case '6':
                    $error = 'Missing a temporary folder.';
                    break;
                
                case '7':
                    $error = 'Failed to write file to disk.';
                    break;
                
                case '8':
                    $error = 'File upload stopped by extension.';
                    break;
                
                case '999':
                default:
                    $error = 'No error code avaiable.';
            }
        }
        elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none') {
            $error = 'No file was uploaded.';
        }
        else {
            return true;
        }        

        $this->error($error);

        return false;
    }
    
    private function error($error) {
        if(count($this->imported) > 0) {
            $this->imported[] = array("-1", $error);
            $this->message();
        }
        else {
            echo "{";
            echo "error: '{$error}',\n";
            echo "msg: ''\n";
            echo "}";
        }
        $this->closeFiles();
        
        die;
    }
    
    private function message() {
        if(count($this->imported) === 0) {
            $this->error("No network imported.");
        }
        
        $msg = "";
        foreach($this->imported as $imported) {
            $id = $imported[0];
            $name = $imported[1];
            if($id == "-1") {
                $msg .= "{$id};{$name};-;";
            }
            else {
                $msg .= "{$id};{$name};Network {$name} was successfully imported.;";
            }
        }
        $msg = preg_replace("/;$/", "", $msg);
        
        echo "{";
        echo "error: '',\n";
        echo "msg: '{$msg}'\n";
        echo "}";
    }
    
    public function manage() {
        if($this->uploadFile()) {
            $this->import();
        }
    }
    
    private function closeFiles() {
        //for security reason, we force to remove all uploaded file
        @unlink($_FILES[self::ELEMENT_NAME]);
    }
    
    protected function startElement($parser, $name, $attrs) {        
        array_push($this->stack, $name);
        if($name === self::ROOT) {
            $this->inRoot = true;
        }
        if($name === self::DEVICES) {
            if(!$this->inRoot) {
                throw new Exception("Devices must be in root element.");
            }
            
            $this->inDevices = true;
        }
        else if ($name === self::DEVICE) {
            if(!$this->inDevices) {
                throw new Exception("Device must be in devices element.");
            }
            
            $this->inDevice = true;
            $this->devices[] = array(); 
        }
        else if ($name === self::CABLES) {
            if(!$this->inRoot) {
                throw new Exception("Cables must be in root element.");
            }
            
            $this->inCables = true;
        }
        else if ($name === self::CABLE) {
            if(!$this->inCables) {
                throw new Exception("Cable must be in cables element.");
            }
            
            $this->inCable = true;
            $this->cables[] = array(); 
        }
        else if ($name === self::INTERFACES) {
            if(!$this->inDevice) {
                throw new Exception("Interfaces must be in device element.");
            }
            
            $this->inInterfaces = true;
        }
        else if ($name === self::_INTERFACE) {
            if(!$this->inInterfaces) {
                throw new Exception("Interface must be in interfaces element.");
            }
            
            $this->inInterface = true;
            $this->interfaces[] = array(); 
        }
        else {           
            if(!$this->inRoot) {
                throw new Exception("There cannot be other root element than " . self::ROOT);
            }
            else if($this->inDevices && !$this->inDevice) {
                throw new Exception("There cannot be other element than " . self::DEVICE . " in " . self::DEVICES);
            }
            else if($this->inInterfaces && !$this->inInterface) {
                throw new Exception("There cannot be other element than " . self::_INTERFACE . " in " . self::INTERFACES); 
            }
            else if($this->inCables && !$this->inCable) {
                throw new Exception("There cannot be other element than " . self::CABLES . " in " . self::CABLES);
            }
        }
    }

    protected function endElement($parser, $name) {
        $opened = array_pop($this->stack);
        if($opened !== $name) {
            throw new Exception("Syntax error: Crossing tags");
        }
        
        if($name === self::ROOT) {
            $this->inRoot = false;            
            $this->insert();
        }
        if($name === self::DEVICES) {            
            $this->inDevices = false;
        }
        else if ($name === self::DEVICE) {            
            $this->inDevice = false;
        }
        else if ($name === self::INTERFACES) {
            $this->inInterfaces = false;
        }
        else if ($name === self::_INTERFACE) {
            $this->inInterface = false;
        }
        else if ($name === self::CABLES) {
            $this->inCables = true;
        }
        else if ($name === self::CABLE) {
            $this->inCable = false; 
        }
    }
    
    protected function insertData(&$array, $key, $data) {
        $str = "" . $key;
        if(empty($str)) {
            return;
        }
        
        $key = "";
        for($i = 0; $i < strlen($str); $i++) {
            $c = $str{$i};
            if(ctype_upper($c)) {
                $key .= "_" . strtolower($c);
            }
            else {
                $key .= $c;
            }
        }

        if(array_key_exists($key, $array)) {
            throw new Exception("Duplicate entry");
        }
        
        $array[$key] = $data;
    }

    protected function cdata($parser, $data) {
        $element = end($this->stack);
        if($this->inDevice && !$this->inInterfaces) {
            if($element === self::DEVICE) {
                throw new Exception("Device cannot contains cdata");
            }
            
            $this->insertData($this->devices[count($this->devices) - 1], $element, $data);
        }
        else if($this->inDevices && $element === self::DEVICES) {
            throw new Exception("Devices cannot contains cdata");
        }
        else if($this->inInterface) {
            if($element === self::_INTERFACE) {
                throw new Exception("Interface cannot contains cdata");
            }
            
            $this->insertData($this->interfaces[count($this->interfaces) - 1], $element, $data);
        }
        else if($this->inInterfaces && $element === self::INTERFACES) {
            throw new Exception("Interfaces cannot contains cdata");
        }
        else if($this->inCable) {
            if($element === self::CABLE) {
                throw new Exception("Cable cannot contains cdata");
            }
            
            $this->insertData($this->cables[count($this->cables) - 1], $element, $data);
        }
        else if($this->inCables && $element === self::CABLES) {
            throw new Exception("Cables cannot contains cdata");
        }
        else if($this->inRoot) {
            if($element === self::ROOT) {
                throw new Exception("Root cannot contains cdata");
            }
            
            $this->insertData($this->network, $element, $data);
        }
    }
    
    private function import() {
        $this->parser = xml_parser_create("utf-8");
        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, false);
        xml_set_object($this->parser, $this);
        xml_set_element_handler($this->parser, "tag_open", "tag_close");
        xml_set_character_data_handler($this->parser, "cdata");
        
        xml_set_element_handler($this->parser, "startElement", "endElement");

        if(is_uploaded_file($_FILES[self::ELEMENT_NAME]['tmp_name'])) {
            $fp = fopen($_FILES[self::ELEMENT_NAME]['tmp_name'], "r");
            if(!$fp) {
                $this->error("Cannot open file: " . $_FILES[self::ELEMENT_NAME]['name']);
            }
            try {
                while(!feof($fp)) {
                    while($x = fread($fp, 4096)) {
                        if(!xml_parse($this->parser, $x, feof($fp))) {
                            $this->error(sprintf("XML error: %s at line %d", 
                                    xml_error_string(xml_get_error_code($this->parser)),
                                    xml_get_current_line_number($this->parser)));
                        }
                    }
                }
            } catch(Exception $exc) {
                @fclose($fp);
                $this->error($exc->getMessage());
            }

            @fclose($fp);
            
            $this->message();
        }
        else {
            $this->error("No file was uploaded.");
        }

        xml_parser_free($this->parser);
        $this->closeFiles();
    }
    
    private function insert() {        
        $id = $this->insertNetwork($this->network);
        $devices = $this->insertDevices($id, $this->devices);
        $this->insertInterfaces($id, $this->interfaces, $devices);
        $this->insertCables($id, $this->cables, $devices);
        
        $this->imported[] = array($id, $this->network[AdminNetworks::NAME]);
                
        $this->network = array();
        $this->devices = array();
        $this->interfaces = array();
        $this->cables = array();
    }
    
    private function insertNetwork($networkData) {
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        if(!$user) {
            throw new Exception("Invalid user");
        }
        
        $networkData[AdminUsers::ID] = $user[0][0];        
        $this->mysql->insert(NetworksManager::NETWORKS_TABLE, "", $networkData);
        
        return $this->mysql->insert_id();      
    }
    
     private function insertDevices($networkId, $devicesData) {
        for($i = 0; $i < count($devicesData); $i++) {
            $devicesData[$i][NetworksManager::NETWORK_ID] = $networkId + "";
            $this->mysql->insert(NetworksManager::DEVICES_TABLE, "", $devicesData[$i]);
            $devicesData[$i][NetworksManager::DEVICE_ID] = $this->mysql->insert_id();
        }
        
        return $devicesData;
    }
    
    private function insertInterfaces($networkId, $interfacesData, $devices) {
        for($i = 0; $i < count($interfacesData); $i++) {
            $interfacesData[$i][NetworksManager::NETWORK_ID] = $networkId + "";
            
            $device = $interfacesData[$i][NetworksManager::DEVICE_ID];
            $interfacesData[$i][NetworksManager::DEVICE_ID] = $devices[$device][NetworksManager::DEVICE_ID];
            
            $this->mysql->insert(NetworksManager::INTERFACES_TABLE, "", $interfacesData[$i]);
            $interfacesData[$i][NetworksManager::INTERFACE_ID] = $this->mysql->insert_id();
        }
        
        return $interfacesData;
    }
    
    private function insertCables($networkId, $cablesData, $devices) {        
        for($i = 0; $i < count($cablesData); $i++) {
            $cablesData[$i][NetworksManager::NETWORK_ID] = $networkId + "";
            
            $device = $cablesData[$i][NetworksManager::SOURCE];
            $cablesData[$i][NetworksManager::SOURCE] = $devices[$device][NetworksManager::DEVICE_ID];
            
            $device = $cablesData[$i][NetworksManager::TARGET];
            $cablesData[$i][NetworksManager::TARGET] = $devices[$device][NetworksManager::DEVICE_ID];
            
            $this->mysql->insert(NetworksManager::CABLES_TABLE, "", $cablesData[$i]);
            $cablesData[$i][NetworksManager::CABLE_ID] = $this->mysql->insert_id();
        }
        
        return $cablesData;
    }
}

?>

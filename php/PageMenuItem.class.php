<?php
/**
 * Represents menu item which contains hyperlink address in format: <tt>?page=$link</tt>.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class PageMenuItem extends MenuItem {
    /**
     * Constructs menu item with page address in link.
     * 
     * @param string $link page address of this menu item (?page=$link).
     * @param string $name name of this item.
     * @return PageMenuItem instance of this menu item.
     */
    public function __construct(/*string*/ $link = "#", /*string*/ $name = "MenuItem") {
        parent::__construct($link, $name);
        
        return $this;
    }
    
    /**
     * Sets hyperlink address for this menu item.
     * 
     * @param string $link page address of this menu item (?page=$link).
     * @return PageMenuItem instance of this menu item.
     */
    public function setLink(/*string*/ $link = "#") {
        if($link === "#") {
            parent::setLink();
        }
        else {
            parent::setLink("?page={$link}");
        }
        
        return $this;
    }
}
?>

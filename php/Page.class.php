<?php
/**
 * Description of Page
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $page Actual page (?page=$page).
 * @property string $title Title for this page (title is text in &lt;title&gt;&lt;/title&gt;).
 * @property string $header Header for this page (header is text in &lt;h1&gt;&lt;/h1&gt;).
 * @property-read string $template Name of Smarty template for this page.
 * @property PageContent $content Content of this page.
 * @property-read array $javascripts Array which contains all javascript which will be written into page.
 */
abstract class Page extends Smarty {
    /**
     * Actual page (?page=$page).
     * @var string
     */
    private $page;
    /**
     * Title for this page (title is text in &lt;title&gt;&lt;/title&gt;).
     * @var string
     */
    private $title;
    /**
     * Header for this page (header is text in &lt;h1&gt;&lt;/h1&gt;).
     * @var string
     */
    private $header;
    /**
     * Content of this page.
     * @var PageContent
     */
    private $content;
    /**
     * Array of page popup messages.
     * @var array
     */
    private $messages = array();
    /**
     * Information whether is this page initialized.
     * @var boolean
     */
    private $init;
    /**
     * Array which contains all javascript which will be written into page.
     * @var array
     */
    private $javascripts = array();
    
    /** Page name - constant for default title and header. */
    const PAGE_NAME = "Network Designer";
    
    /**
     * Constructs page.
     * 
     * @param string $page actual page (?page=$page).
     * @return Page instance of this page.
     */
    public function __construct(/*string*/ $page="") {
        if(isset($_GET["error"]) && !($this instanceof HTTPError)) {
            $error = new HTTPError($_GET["error"]);
            $error->printOut();
        }
        
        if(isset($_GET["logout"])) {
            $this->logout();
        }
        
        if(!$this->isLogged()) {
            if(!($this instanceof LoginPage) && !($this instanceof Error)) {
                $login = new LoginPage($this);
                $login->printOut();
                // next line will process only in case of sucessfull login
                $this->addMessages($login->getMessages());
            }
        }
        
        if(!empty($_SESSION["messages"])) { 
            $messages = $_SESSION["messages"];
            
            for($i = 0; $i < count($messages); $i+=2) {
                $this->messages[] = new PageMessage($messages[$i], $messages[$i+1]);
            }
                    
            $_SESSION["messages"] = null;
            unset($_SESSION["messages"]);
        }
        
        parent::__construct();
        
        $this->init = false;
        $this->assign("robots", false);
        $this->assign("admin", isAdminPage());
        $this->assign("designer", false);
        
        $this->initTemplatesDirectories();
        if(!empty($page)) {
            $this->setPage($page);
        }
        else {
            $this->setTitle();
            $this->setHeader();
        }
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        if($name == "template") {
            return;
        }
        
        unset($this->$name);
    }
    
    /**
     * Initialzes sub-directory for templates.
     * 
     * @param string $subdirectory subdirectory for templates of this page.
     */
    protected function initTemplatesDirectories(/*string*/ $subdirectory = "") {
        $this->setTemplateDir(SMARTY_TEMPLATES . $subdirectory);
        $this->setCompileDir(SMARTY_TEMPLATES_COMPILE . $subdirectory);
        $this->setConfigDir(SMARTY_CONFIGS . $subdirectory);
        $this->setCacheDir(SMARTY_CACHE . $subdirectory);
    }
    
    /**
     * Shows error page and ends execution.
     * 
     * @param string $title title of error page.
     * @param string $header header of error page.
     * @param string $text text of error page.
     * 
     * @see Error
     */
    protected function error(/*string*/ $title = "", /*string*/ $header = "", /*string*/ $text = "") {
        $error = new Error($title, $header, $text);
        $error->printOut();
    }
    
    /**
     * Initializes page content of this page.
     * 
     * @return Page instance of this page.
     */
    protected function init() {
        if(isset($this->content) && !$this->init) {
            $this->content->init();
            
            $this->setTitle($this->content->title);
            $this->setHeader($this->content->header);
            $this->assign("content", $this->content->template);
            
            $this->init = true;
        }
        
        return $this;
    }
    
    /**
     * Returns actual page (?page=$page).
     * 
     * @return string actual page.
     */
    public function getPage() {
        return $this->page;
    }
    
    /**
     * Sets actual page (?page=$page).
     * 
     * @param string $page actual page.
     */
    protected function setPage(/*string*/ $page) {
        if(empty($page)) {
            return $this;
        }
        
        $this->page = $page;

        $class = ucfirst($page);
        if(!file_exists(classpath($class))) {            
            header("HTTP/1.0 404 Not Found");
            $error = new HTTPError(404);
            $error->printOut();
        }
        
        $this->setContent(new $class($this));
                
        return $this;
    }
    
    /**
     * Returns page content of this page.
     * 
     * @return PageContent Page content in this page.
     */
    public function getContent() {
        return $this->content;
    }
    
    /**
     * Sets page content for this page.
     * 
     * @param PageContent $content page content for this page.
     * @return Page instance of this page.
     */
    protected function setContent(/*PageContent*/ $content) {
        if($content instanceof PageContent) {   
            $this->content = $content;
            $this->init = false;
        }
        else {
            error("Invalid content type", "Invalid page content type", 
                    "In function <tt>setContent(PageContent)</tt> was set class of wrong type!");
        }
                
        return $this;
    }
    
    /**
     * Returns title for this page (title is text in &lt;title&gt;&lt;/title&gt;).
     * 
     * @return string Title for this page.
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * Sets title for this page (title is text in &lt;title&gt;&lt;/title&gt;).
     *  
     * @param string $title text of title for this page. If entered title is 
     * empty, then is used PAGE_NAME as text in title.
     * @return Page instance of this page.
     */
    public function setTitle(/*string*/ $title = "") {
        $this->title = $title;
        
        if(empty($this->title)) {
            $this->assign("title", static::PAGE_NAME);
        }
        else {
            $this->assign("title", static::PAGE_NAME . " &gt; {$this->title}");
        }
                
        return $this;
    }
    
    /**
     * Returns header for this page (header is text in &lt;h1&gt;&lt;/h1&gt;).
     * 
     * @return string Header for this page.
     */
    public function getHeader() {
        return $this->header;
    }
    
    /**
     * Sets header for this page (header is text in &lt;h1&gt;&lt;/h1&gt;).
     *  
     * @param string $header text of header for this page. If entered header is 
     * empty, then is used PAGE_NAME as text in header.
     * @return Page instance of this page.
     */
    public function setHeader(/*string*/ $header = "") {
        $this->header = $header;
        
        if(empty($this->header)) {
            $this->assign("header", static::PAGE_NAME);
        }
        else {
            $this->assign("header", $this->header);
        }
        
        return $this;
    }
    
    /**
     * Logouts logged user.
     */
    protected function logout() {
        $_SESSION["user"] = null;
        unset($_SESSION["user"]);
        $_SESSION["logged"] = null;
        unset($_SESSION["logged"]);
        $_SESSION["admin"] = null;
        unset($_SESSION["admin"]);
        $this->addMessage("Your logout was successful.");
        
        $this->redirect("index.php");
    }
    
    /**
     * Returns information whether is someone logged or not.
     * 
     * @return boolean information whether is someone logged or not.
     */
    public function isLogged() {
        if(isset($_SESSION["logged"]) && $_SESSION["logged"] === true) {
            return true;
        }
        
        return false;
    }
    
    /**
     * This abstract function will returns name of Smarty template for this page.
     * 
     * @return string Name of template for this page.
     */
    public abstract function getTemplate();
    
    /**
     * Redirects into specified location. If entered location is empty, it will 
     * redirect into actual page (index.php?page=[ActualPage]). After redirection 
     * is called die function -> it means there is no code executed after redirection.
     * 
     * @param string $location location for redirection.
     */
    public function redirect(/*string*/ $location = "") {
        if(empty($location)) {
            $location = "index.php?page={$this->getPage()}";
        }
        
        if(count($this->messages) > 0) {            
            $messages = array();
            
            for($i = 0, $j = 0; $i < count($this->messages); $i++, $j+=2) {
                $messages[$j] = $this->messages[$i]->text;
                $messages[$j+1] = $this->messages[$i]->type;
            }
            
            $_SESSION["messages"] = $messages;
        }
        
        if(MySQL::is_opened()) {
            MySQL::get_instance()->close();
        }
        
                
        header("location: " . $location);
        die;
    }
    
    /**
     * Adds error message into this page.
     * 
     * @param string $text text of error message.
     * @return Page instance of this page.
     */
    public function addErrorMessage(/*string*/ $text) {
        $this->messages[] = new PageMessage($text, "error");
        
        return $this;
    }
    
    /**
     * Adds info message into this page.
     * 
     * @param string $text text of message.
     * @return Page instance of this page.
     */
    public function addMessage(/*string*/ $text) {
        $this->messages[] = new PageMessage($text);
        
        return $this;
    }
    
    /**
     * Adds array of page messages.
     * 
     * @param array $messages page messages which will be added.
     * @return Page instance of this page.
     */
    public function addMessages(/*array*/ $messages) {
        foreach($messages as $message) {
            if($message instanceof PageMessage) {
                $this->messages[] = $message;
            }
        }
        
        return $this;
    }
    
    /**
     * Returns array which contains messages which was created in this page.
     * 
     * @return array Array of PageMessages which was created in this page.
     */
    public function getMessages() {
        return $this->messages;
    }
    
    /**
     * Adds javasript(s) into this page.
     * 
     * @param PageJavascript|array $javascript one or more javascripts.
     * @return Page Instance of this page.
     */
    public function addJavascript(/*PageJavascript|array*/ $javascript) {
        if(is_array($javascript)) {
            foreach($javascript as $js) {
                if($js instanceof PageJavascript) {
                    $this->javascripts[] = $js;
                }
            }
        }
        else if($javascript instanceof PageJavascript) {
            $this->javascripts[] = $javascript; 
        }
        
        return $this;
    }
    
    /**
     * Removes javasript(s) from this page.
     * 
     * @param PageJavascript|array $javascript one or more javascripts.
     * @return Page Instance of this page.
     */
    public function removeJavascript(/*PageJavascript|array*/ $javascript) {
        if(is_array($javascript)) {
            foreach($javascript as $js) {
                if(($index = array_search($js, $this->javascripts)) !== false) {
                    unset($this->javascripts[$index]);
                }
            }
        }
        else if(($index = array_search($javascript, $this->javascripts)) !== false) {
            unset($this->javascripts[$index]);
        }
        
        return $this;
    }
    
    /**
     * Returns array which contains messages which was created in this page.
     * 
     * @return array Array of PageMessages which was created in this page.
     */
    public function getJavascripts() {
        return $this->javascripts;
    }
   
    /**
     * Prints out this page using smarty templates.
     * 
     * @return Page instance of this page.
     */
    public function printOut() {
        if(count($this->messages) > 0) { 
           $this->addJavascript(new PageJavascript("messages", $this->messages));
        }
        
        if(count($this->javascripts) > 0) {
            $this->assign("javascripts", $this->javascripts); 
        }
        
        $this->assign("body", $this->getTemplate());
        $this->display("index.tpl");
        
        return $this;
    }
}

?>

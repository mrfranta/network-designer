<?php
/**
 * This class represents content of admin page which serves for categories managing.
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class AdminCategories extends AdminPageContent {
    /** Name of column in database for category id. */
    const ID = "category_id";
    /** Name of column in database for category name. */
    const NAME = "category_name";
    /** Name of column in database for category image. */
    const IMAGE = "category_image";
    
    /** Name of table in database for categories. */
    const TABLE = "tbl_categories";
    
    /** Image for (none) category. */
    const NONE_IMAGE = "images/categories/(none).png";
    
    /**
     * Constructs page content for managing categories..
     * 
     * @param Page page which will contains this content.
     * @return AdminCategories Instance of this page content.
     */
    public function __construct($page) {
        parent::__construct($page);
        
        $this->assign("noDelete", array(1 => "(none)"));
        $this->assign("noEdit", array(1 => "(none)"));
        
        return $this;
    }
    
    /**
     * Action for category deletion.
     * 
     * @param string $where (unused).
     * @param boolean $redirect (unused).
     */
    protected function deleteAction(/*string*/ $where = "", /*boolean*/ $redirect = true) {
        $mysql = MySQL::get_instance();
        
        $result = $mysql->select(self::TABLE, "*", self::ID . " = '{$_GET['id']}'");
        if($result) {
            if($result[0][self::NAME] === "(none)") {
                $this->addErrorMessage("This category cannot be removed.");
                $this->page->redirect();
                
                return;
            }
            
            @unlink($this->getImage($result[0][self::IMAGE]));
        }
        
        $result = $mysql->select(self::TABLE, "*", self::NAME . " = '(none)'");
        $mysql->update(AdminDevices::TABLE, array(self::ID => $result[0][self::ID]), self::ID . " = '{$_GET['id']}'");
        
        parent::deleteAction(self::ID . " = " . $_GET['id'], true);
    }
    
    /**
     * Action for editing some category.
     * 
     * @return string name of editing category.
     */
    protected function editAction() {
        $mysql = MySQL::get_instance();
        
        $item = array(self::NAME, self::IMAGE);
        $result = $mysql->select(self::TABLE, $item, self::ID . " = {$_GET['id']}");
        if($result === false) {    
            $this->addErrorMessage("Category with entered id was not found in database.");
            $this->getPage()->redirect();
            
            return;
        }
        else {
            if($result[0][self::NAME] === "(none)") {
                $this->addErrorMessage("This category cannot be edited.");
                $this->page->redirect();
            
                return;
            }
        }
        
        $this->setFormValues(array(
            self::NAME => $result[0][self::NAME],
            self::IMAGE . "_old" => $result[0][self::IMAGE],
            self::SUBMIT => "Edit"
        ));
        
        return $result[0][self::NAME];
    }

    /**
     * Function which represents reaction for posting form.
     */
    protected function formAction() {
        $error = false;
        
        $name = $_POST[self::NAME];
        $element = $this->form->getElementByName(self::NAME);
        if(empty($name)) {
            $error = "Name of category cannot be empty.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        elseif(strlen($name) > 20) {
            $error = "Category name is too long.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        elseif(!$this->edit) {
            $mysql = MySQL::get_instance();
            if($mysql->select(self::TABLE, self::NAME, self::NAME . " = '{$name}'")) {
                $error = "Entered category name is already exist in database.";
                
                $this->addErrorMessage($error);
                $element->error = $error;
            }
        }
        
        $image = $_FILES[self::IMAGE];
        $path = "";
        if(empty($image["name"])) {
            if(!$this->getEdit()) {
                $error = "Image of category wasn't selected.";
                $this->addErrorMessage($error);

                $element = $this->form->getElementByName(self::IMAGE);
                $element->error = $error;
            }
        }
        else {
            if(!$error) {
                $uploader = new ImageUploader();
                $result = $uploader->uploadImage($image, $this->getPage()->page, $name);
                
                if(!$result) {
                    $element = $this->form->getElementByName(self::IMAGE);
                    $element->error = $uploader->getError();
                    $error = true;
                }
                
                $path = $result;
            }
        }
        
        $values = array(
            self::NAME => $name,
        );
        
        if(!$error) {
            if($this->getEdit()) {
                $values[self::IMAGE] = $this->renameImage($path, $_POST[self::IMAGE . "_old"], $name);
                
                $this->editValues($values, self::ID . " = " . $_GET['id']);
            }
            else {
                $values[self::IMAGE] = $path;
                $this->insertValues($values);
            }
        }
        else {
            if(isset($_POST[self::IMAGE . "_old"])) {
                $values[self::IMAGE . "_old"] = $_POST[self::IMAGE . "_old"];
            }
            
            $this->setFormValues($values);
        }
    }

    /**
     * Returns array which contains names of columns from MySQL table for categories.
     * Values from this columns will be shown in table.
     * 
     * @return array Array of table columns.
     */
    protected function getTableColumns() {
        return array(self::ID, self::NAME, self::IMAGE);
    }
    
    /**
     * Initializes and returns table header.
     * 
     * @return TableHeader Table header for table which contains created categories.
     */
    protected function getTableHeader() {
        $header = new TableHeader();
        $columns = array(
            new TableHeaderColumn("Id", 50),
            new TableHeaderColumn("Name", 200),
            new TableHeaderColumn("Image", 50)
        );
        
        $header->addColumns($columns);

        return $header;
    }
    
    /**
     * Initializes and returns form elements for form.
     * 
     * @return array Array which contains form elements for form.
     */
    protected function initForm() {
        $elements = array();
        
        $elements[] = new FormInput(array(
            "label" => "Name:",
            "id" => self::NAME,
            "addition" => "maxlength=\"20\"",
            "info" => "Nonempty and unique name of category for devices (max 20 chars).",
        ));
        
        if($this->getEdit()) {
            $elements[] = new FormInput(array(
                "label" => "Old image:",
                "id" => self::IMAGE . "_old",
                "class" => "readonly",
                "type" => "input", 
                "addition" => "readonly=\"readonly\"", 
                "info" => "Address of old category image."
            ));
        }
        
        $elements[] = new FormInput(array(
            "label" => $this->getEdit() ? "New image:" : "Image:",
            "id" => self::IMAGE,
            "type" => "file", 
            "addition" => "accept='image/*'", 
            "info" => "Allowed image formats: *.jpg, *.gif, *.png<br />" .  
                      "Maximum image size: " . ImageUploader::MAX_IMAGE_SIZE . " MB<br />" .
                      "Maximum image resolution: " . ImageUploader::MAX_SIZE . "x" . ImageUploader::MAX_SIZE . "<br />"
        ));
                
        $this->getForm()->setEnctype("multipart/form-data");

        return $elements;
    }
    
    /**
     * Initializes and returns data for table which contains created categories.
     * 
     * @return string Data for table with created categories.
     */
    protected function initTable() {
        $result = parent::initTable();
        if(!$result) {
            self::recovery();
            $result = MySQL::get_instance()->select(self::TABLE, $this->getTableColumns(), 
                    "", "", $this->getElementIDColumn());    
        }
        
        if(is_array($result)) {
            for($i = 0; $i < count($result); $i++) {
                $result[$i][2] = "<img src=\"{$this->getImage($result[$i][2])}\" style=\"width: 16px; height: auto;\" alt=\"{$result[$i][1]}\" />";
            }
        }
        
        return $result;
    }
    
    /**
     * Returns title for categories page content.
     * 
     * @return string Always "Devices categories" (title for categories).
     */
    public function getTitle() {
        return "Devices categories";
    }
    
    /**
     * Returns name of element which will be edited in this page content.
     *
     * @return string Always "category" (element name).
     */
    public function getElementName() {
        return "category";
    }
    
    /**
     * Recoveries "(none)" category in database.
     * @static
     */
    public static function recovery() {
        $mysql = MySQL::get_instance();
        if(!$mysql->select(self::TABLE, self::NAME, self::NAME . " = '(none)'")) {
            $mysql->insert(self::TABLE, "", array(
                self::NAME => "(none)",
                self::IMAGE => self::NONE_IMAGE
            ));
        }
    }
}

?>

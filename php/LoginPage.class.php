<?php
/**
 * This class represents login page.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class LoginPage extends Page {
    /**
     * Constructs login page.
     * 
     * @return LoginPage instance of this login page.
     */
    public function __construct() {       
        parent::__construct();
        
        $this->assign("error", false);
        
        if(isset($_POST["login"])) {
            $this->login();
        }
        
        return $this;
    }

    /**
     * Login action.
     */
    private function login() {
        $user = $_POST["user"];
        $pass = $_POST["password"];

        $mysql = MySQL::get_instance();
        $where = "user_name='{$mysql->string_escape($user)}' AND user_pass='". md5($pass) . "'";
        $result = $mysql->select("tbl_users", "*", $where);
        if($result !== false && count($result) > 0) {
            $_SESSION["logged"] = true;
            $_SESSION["user"] = $result[0]["user_name"];
            $_SESSION["admin"] = $result[0]["user_level"];
            
            $this->addMessage("Login as {$_SESSION['user']} was successful.");
        }
        else {
            $this->assign("error", true);
            $this->addErrorMessage("Login as {$user} failed.");
        }
    }
   
    /**
     * Returns name of Smarty template for this login page.
     * 
     * @return string Name of template for this login page.
     */
    public function getTemplate() {
        return 'login';
    }
    
   /**
     * Prints out this error page using smarty templates and exits php execution.
     */
    public function printOut() {
        if($this->isLogged()) {
            return;
        }
        
        parent::printOut();
        exit(0);
    }
}

?>

<?php
/**
 * This class represents one item (hyperlink) in menu.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $link Hyperlink address of this item.
 * @property string $name Name of this item.
 */
class MenuItem {
    /**
     * Hyperlink address of this item.
     * @var string
     */
    public $link;
    /**
     * Name of this item.
     * @var string
     */
    public $name;
    
    /**
     * Constructs menu item.
     * 
     * @param string $link hyperlink address of this item.
     * @param string $name name of this item.
     * @return MenuItem instance of this menu item.
     */
    public function __construct(/*string*/ $link = "#", /*string*/ $name = "MenuItem") {
        $this->setLink($link);
        $this->setName($name);
        
        return $this;
    }

        /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        unset($this->$name);
    }
    
    /**
     * Returns hyperlink address of this menu item.
     * 
     * @return string Hyperlink address of this menu item.
     */
    public function getLink() {
        return $this->link;
    }
    
    /**
     * Sets hyperlink address of this menu item.
     * 
     * @param string $link hyperlink address of this menu item.
     * @return MenuItem instance of this menu item.
     */
    public function setLink(/*string*/ $link = "#") {
        $this->link = $link;
        
        return $this;
    }
    
    /**
     * Name of this menu item.
     * 
     * @return string name of this menu item.
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Sets name of this menu item.
     * 
     * @param string $name name of this menu item.
     * @return MenuItem instance of this menu item.
     */
    public function setName(/*string*/ $name = "MenuItem") {
        $this->name = $name;
        
        return $this;
    }
}

?>

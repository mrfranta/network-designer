<?php
/**
 * This class represent HTTP error page.
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property int $error Number of HTTP error.
 */
final class HTTPError extends Error {
    /**
     * Number of HTTP error.
     * @var int
     */
    private $error;
    /**
     * Constructs HTTP error page.
     * 
     * @param int $number number of HTTP error.
     * @return HTTPError instance of this error page.
     */
    public function HTTPError(/*int*/ $number = 0) {
        parent::__construct();
        $this->setError($number);

        return $this;
    }

    /**
     * Sets title and message of this error page.
     * 
     * @param string $title title and header of error page.
     * @param string $message error message (text of error).
     * @return HTTPError instance of this error page.
     */
    public function set(/*string*/ $title, /*string*/ $message) {
        $this->setTitle($title);
        $this->setHeader($title);
        $this->setMessage($message);

        return $this;
    }
    
    /**
     * Returns number of HTTP error.
     * 
     * @return int number of HTTP error.
     */
    public function getError() {
        return $this->error;
    }

    /**
     * Sets number of HTTP error.
     * 
     * @param type $number number of HTTP error.
     * @return HTTPError instance of this error page.
     */
    public function setError(/*int*/ $number) {
        switch($number) {
            case 400: 
                $this->set("400 - Bad Request", 
                        "The request was denied due to a syntax error in the request."); 
                break;

            case 401: 
                $this->set("401 - Unauthorized", 
                        "Your IP address or the username/password you entered were not correct. Your request was denied as you have no permission to access the data."); 
                break;

            case 402: 
                $this->set("402 - Payment Required", 
                        "The data is not accessible at the time. The owner of the space has not yet payed their service provider."); 
                break;

            case 403: 
                $this->set("403 - Forbidden", 
                        "Your IP address or the username/password you entered were not correct. Your request was denied as you have no permission to access the data."); 
                break;

            case 404: 
                $this->set("404 - Not Found", 
                        "The document that has been requested either no longer exists, or has never existed on the server."); 
                break;

            case 405: 
                $this->set("405 - Method Not Allowed", 
                        "The method you are using to access the document is not allowed. "); 
                break;

            case 406: 
                $this->set("406 - Not Acceptable", 
                        "The client (webbrowser) does not accept the document format. The formats that may be specified not to accept are charset, encoding, certain file types, languages, or ranges."); 
                break;

            case 407: 
                $this->set("406 - Proxy Authentication Required", 
                        "The browser has not been authenticated on the required proxy server to access the data. This error is probably most commonly returned by content filters/parental controls."); 
                break;

            case 408: 
                $this->set("407 - Request Timeout", 
                        "The server has closed the socket due to communications between the client and server taking too long. This could be due to server load, bandwidth issues, the client being disconnected from the internet, etc.");
                break;

            case 409: 
                $this->set("409 - Conflict", 
                        "Too many requests for the same file at one time."); 
                break;

            case 410: 
                $this->set("410 - Gone", 
                        "The document that has been requested either no longer exists, or has never existed on the server."); 
                break;

            case 411: 
                $this->set("411 - Length Required", 
                        "When trying to send a document to the server the server did not recieve a Content-Length specification in the header."); 
                break;
            
            case 412: 
                $this->set("412 - Precondition Failed", 
                        "A precondition setting required by the client or server has not been met."); 
                break;
            
            case 413: 
                $this->set("413 - Request Entity Too Large", 
                        "The process is too large to process."); 
                break;
            
            case 414: 
                $this->set("412 - Request-URI Too Large", 
                        "The URL requested is simply too long."); 
                break;
            
            case 415: 
                $this->set("415 - Unsupported Media Type", 
                        "This usually occurs if the server does not support the type of media the client is requesting. "); 
                break;
            
            case 416: 
                $this->set("416 - Requested Range Not Satisfiable", 
                        "The client request included a range for acceptable file size, however the document requested did not fit into that range."); 
                break;
            
            case 417: 
                $this->set("417 - Expectation Failed", 
                        "The client's expect header requested certain server behaviors that the server could not perform."); 
                break;
            
            case 500: 
                $this->set("500 - Internal Server Error", 
                        "The server encountered an error. This is most often caused by a scripting problem, a failed database access attempt, or other similar reasons."); 
                break;
            
            case 501: 
                $this->set("501 - Not Implemented", 
                        "The method you are using to access the document can not be performed by the server."); 
                break;
            
            case 502: 
                $this->set("502 - Bad Gateway", 
                        "The document requested resides on a 3rd party server and the original server received an error from the 3rd party server."); 
                break;
            
            case 503: 
                $this->set("503 - Service Unavailable", 
                        "The server is overloaded or down for maintenance and due to this was unable to process the client request."); 
                break;
            
            case 504: 
                $this->set("504 - Gateway Timeout", 
                        "Most likely the client has lost connectivity (disconnected from the internet) or the cleint's host is having technical difficulties. This could also mean that a server that allows access to the requested server is down, having bandwidth/load issues, or otherwise unavailable."); 
                break;
            
            case 505: 
                $this->set("505 - HTTP Version not supported", 
                        "The server does not support the HTTP version used by the client. (This usually occurs if the server is using an OLDER version of HTTP than the client.)"); 
                break;
            
            default: 
                $this->set("Unknown Error", ""); 
                break;
        }

        return $this;
    }
}

?>

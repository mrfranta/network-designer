<?php
/**
 * Description of NetworksManager
 *
 * @author Mr.FrAnTA
 */
class NetworksManager extends NetworksManagement {   
    const NETWORKS_TABLE = "tbl_networks";
    const DEVICES_TABLE = "tbl_network_devices";
    const INTERFACES_TABLE = "tbl_network_interfaces";
    const CABLES_TABLE = "tbl_network_cables";
    
    const NETWORK_ID = "network_id";
    const DEVICE_ID = "network_device_id";
    const INTERFACE_ID = "network_interface_id";
    const CABLE_ID = "network_cable_id";
    
    const SOURCE = "source_network_device_id";
    const TARGET = "target_network_device_id";
    
    private static $networks_header = ["network_name", "network_title", 
        "network_width", "network_height", "network_background", "user_id"];
    
    private static $networks_header_load = ["network_name", "network_title", 
        "network_width", "network_height", "network_background"];
    
    private static $devices_header = ["device_id", "network_device_label", 
        "network_device_x", "network_device_y", "network_device_ports"];
    
    private static $interfaces_header = ["network_device_id", "network_interface_ip", 
        "network_interface_mask", "network_interface_vlans", "network_interface_info"];
    
    private static $cables_header = ["cable_id", "source_network_device_id", 
        "target_network_device_id", "source_device_port", "target_device_port"];
    
    public function __construct() {
        parent::__construct();
        
        if(!isset($_SERVER["HTTP_REFERER"])) {
            throw new Exception("Access denied");
        }
        if($_SERVER["HTTP_REFERER"] != "http://" . $_SERVER["SERVER_NAME"] . "/admin/designer.php") {
            if(strpos($_SERVER["HTTP_REFERER"], "http://" . $_SERVER["SERVER_NAME"]) !== 0) {
                throw new Exception("Access denied");
            }
        }
    }
    
    private function parse(/*String*/ $data) {
        $atom = "";
        $parsed = array();
        if(strlen($data) === 0) {
            return $parsed;
        }
        
        $esc = false;
        for($i = 0; $i < strlen($data); $i++) {
            $c = $data{$i};
            if($c === "\\") {
                if($esc) {
                    $atom .= "\\";
                    $esc = false;
                }
                else {
                    $esc = true;
                }
                
                continue;
            }
            
            if(!$esc && $c === ";") {
                $parsed[] = $atom;
                $atom = "";
                
                continue;
            }
            
            if($esc && $c !== ";") {
                throw new Exception("Invalid format of data");
            }
            else {
                $esc = false;
            }
            
            $atom .= $c;
        }
        
        $parsed[] = $atom;
        
        return $parsed;
    }
    
    private function toSaveString($params) {
        $ret = "";
        for($i = 0; $i < count($params); $i++) {
            if($i > 0) {
                $ret .= ";";
            }
            
            $ret .= $this->escape($params[$i]);
        }
        
        return $ret;
    }
    
    private function dataToSaveString($data) {
        $ret = "";
        if(empty($data)) {
            return $ret;
        }
        
        foreach($data as $row) {
            $ret .= $this->toSaveString($row) . ";";
        }
        
        return preg_replace("/;$/", "", $ret);
    }
    
    public function manage() {
        if(isset($_GET["save"])) {
            $this->save();
        }
        elseif(isset($_GET["load"])) {
            $this->load();
        }
        elseif(isset($_GET["remove"])) {
            $this->remove();
        }
        else {
            throw new Exception("Invalid operation for manager.");
        }
    }
    
    private function insertNetwork($networkData) {
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        if(!$user) {
            throw new Exception("Invalid user");
        }
        
        $networkData[] = $user[0][0];
        $values = array();
        for($i = 0; $i < count(self::$networks_header); $i++) {
            $values[self::$networks_header[$i]] = $networkData[$i];
        }
        
        $this->mysql->insert(self::NETWORKS_TABLE, "", $values);
        
        return $this->mysql->insert_id();      
    }
    
    private function insertDevices($networkId, $devices) {
        $devicesData = array();
        if(count($devices) > 0) {
            $columns = count(self::$devices_header) + 1;
            $device = array();
            for($i = 0; $i < count($devices); $i++) {
                $mod = $i % $columns;
                if($mod == 0) {
                    if($i > 0) {
                        $devicesData[] = $device;
                        $device = array();
                    }
                    
                    continue;
                }

                $device[self::$devices_header[$mod - 1]] = $devices[$i];
            }
            $devicesData[] = $device;
        }
        
        for($i = 0; $i < count($devicesData); $i++) {
            $devicesData[$i][self::NETWORK_ID] = $networkId + "";
            $this->mysql->insert(self::DEVICES_TABLE, "", $devicesData[$i]);
            $devicesData[$i][self::DEVICE_ID] = $this->mysql->insert_id();
        }
        
        return $devicesData;
    }
    
    private function insertInterfaces($networkId, $interfaces, $devices) {
        $interfacesData = array();
        if(count($interfaces) > 0) {
            $columns = count(self::$interfaces_header);
            $interface = array();
            for($i = 0; $i < count($interfaces); $i++) {
                $mod = $i % $columns;
                if($i > 0 && $mod == 0) {
                    $interfacesData[] = $interface;
                    $interface = array();
                }

                $interface[self::$interfaces_header[$mod]] = $interfaces[$i];
            }
            
            $interfacesData[] = $interface;
        }

        for($i = 0; $i < count($interfacesData); $i++) {
            $interfacesData[$i][self::NETWORK_ID] = $networkId + "";
            
            $device = $interfacesData[$i][self::DEVICE_ID];
            $interfacesData[$i][self::DEVICE_ID] = $devices[$device][self::DEVICE_ID];
            
            $this->mysql->insert(self::INTERFACES_TABLE, "", $interfacesData[$i]);
            $interfacesData[$i][self::INTERFACE_ID] = $this->mysql->insert_id();
        }
        
        return $interfacesData;
    }
    
    private function insertCables($networkId, $cables, $devices) {
        $cablesData = array();
        if(count($cables) > 0) {
            $columns = count(self::$cables_header);
            $cable = array();
            for($i = 0; $i < count($cables); $i++) {
                $mod = $i % $columns;
                if($i > 0 && $mod == 0) {
                    $cablesData[] = $cable;
                    $cable = array();
                }

                $cable[self::$cables_header[$mod]] = $cables[$i];
            }
            
            $cablesData[] = $cable;
        }
        
        for($i = 0; $i < count($cablesData); $i++) {
            $cablesData[$i][self::NETWORK_ID] = $networkId + "";
            
            $device = $cablesData[$i][self::SOURCE];
            $cablesData[$i][self::SOURCE] = $devices[$device][self::DEVICE_ID];
            
            $device = $cablesData[$i][self::TARGET];
            $cablesData[$i][self::TARGET] = $devices[$device][self::DEVICE_ID];
            
            $this->mysql->insert(self::CABLES_TABLE, "", $cablesData[$i]);
            $cablesData[$i][self::CABLE_ID] = $this->mysql->insert_id();
        }
        
        return $cablesData;
    }
    
    private function save() {
        $id = $_GET["save"];
        
        if(!isset($_POST["network"]) || empty($_POST["network"])) {
            throw new Exception("Missing data: network");
        }
        elseif(!isset($_POST["devices"])) {
            throw new Exception("Missing data: devices");    
        }
        elseif(!isset($_POST["cables"])) {
            throw new Exception("Missing data: cables");  
        }
        
        $network = $this->parse($_POST["network"]);
        $devices = $this->parse($_POST["devices"]);
        $interfaces = $this->parse($_POST["interfaces"]);
        $cables = $this->parse($_POST["cables"]);
        
        if($id > 0) {
            $this->update($id, $network, $devices, $interfaces, $cables);
            
            return;
        }
        
        $id = $this->insertNetwork($network);
        $devicesData = $this->insertDevices($id, $devices);
        $this->insertInterfaces($id, $interfaces, $devicesData);
        $this->insertCables($id, $cables, $devicesData);
        
        echo $id . ";" . "Network was successfully inserted into database.";
    }
    
    private function update(/*int*/ $id, /*Array*/ $network, $devices, $interfaces, $cables) {
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        if(!$user) {
            throw new Exception("Invalid user");
        }
        
        $network[] = $user[0][0];
        $values = array();
        for($i = 0; $i < count(self::$networks_header); $i++) {
            $values[self::$networks_header[$i]] = $network[$i];
        }
        
        $where = self::NETWORK_ID . " = {$id} AND " . AdminUsers::ID . " = {$user[0][0]}";
        $this->mysql->update(self::NETWORKS_TABLE, $values, $where);
        
        $this->mysql->delete(self::DEVICES_TABLE, self::NETWORK_ID . " = {$id}");
        $devicesData = $this->insertDevices($id, $devices);
        
        $this->mysql->delete(self::INTERFACES_TABLE, self::NETWORK_ID . " = {$id}");
        $this->insertInterfaces($id, $interfaces, $devicesData);
        
        $this->mysql->delete(self::CABLES_TABLE, self::NETWORK_ID . " = {$id}");
        $this->insertCables($id, $cables, $devicesData);
        
        echo $id . ";" . "Network was successfully updated";
    }
    
    private function escape($variable) {
        $str = "" . $variable;
        $ret = "";
        for($i = 0; $i < strlen($str); $i++) {
            $c = $str{$i};
            
            if($c === "\\" || $c === ";") {
                $ret .= "\\";
            }
            
            $ret .= $c;
        }
        
        return $ret;
    }
    
    private function loadNetwork($id) {
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        if(!$user) {
            throw new Exception("Invalid user");
        }
        
        $where = self::NETWORK_ID . " = {$id} AND " . AdminUsers::ID . " = {$user[0][0]}";
        $network = $this->mysql->select(self::NETWORKS_TABLE, self::$networks_header_load, $where);
        if(!$network) {
            throw new Exception("Cannot load network: no such network.");
        }
        
        $data = array();
        foreach(self::$networks_header_load as $key) {
            $data[] = $network[0][$key];
        }
        
        return $this->toSaveString($data);
    }
    
    private function loadDevices($id, $withId = false) {        
        $devices = $this->mysql->select(AdminDevices::TABLE, [AdminDevices::ID, AdminDevices::IMAGE]);
        if(!$devices) {
            return "";
        }
        
        $devicesImages = array();
        foreach($devices as $device) {
            $devicesImages[$device[AdminDevices::ID]] = ROOT . $device[AdminDevices::IMAGE];
        }
        
        $item = self::$devices_header;
        $item[] = self::DEVICE_ID;
        $devices = $this->mysql->select(self::DEVICES_TABLE, $item, self::NETWORK_ID . " = {$id}");
        if(!$devices) {
            return "";
        }
        
        $data = array();
        foreach($devices as $device) {
            $row = array();
            foreach($item as $column) {
                if($column === self::DEVICE_ID) {
                    if($withId) {
                        $row[] = $device[$column];
                    }
                    continue;
                }
                else if($column === AdminDevices::ID) {
                    $row[] = $device[$column];
                    $row[] = $devicesImages[$device[$column]];
                    continue;
                }
                
                $row[] = $device[$column];
            }
            
            $data[] = $row;
        }
        
        return $data;
    }
    
    private function loadInterfaces($id, $devicesMap) {
        $interfaces = $this->mysql->select(self::INTERFACES_TABLE, self::$interfaces_header, self::NETWORK_ID . " = {$id}");
        if(!$interfaces) {
            return "";
        }
        
        $data = array();
        foreach($interfaces as $interface) {
            $row = array();
            foreach(self::$interfaces_header as $column) {
                if($column == self::DEVICE_ID) {
                    $row[] = array_search($interface[$column], $devicesMap);
                    continue;
                }
                
                $row[] = $interface[$column];
            }
            
            $data[] = $row;
        }
        
        return $data;
    }
    
    private function loadCables($id, $devicesMap) {
        $cables = $this->mysql->select(self::CABLES_TABLE, self::$cables_header, self::NETWORK_ID . " = {$id}");
        if(!$cables) {
            return "";
        }
        
        $data = array();
        foreach($cables as $cable) {
            $row = array();
            foreach(self::$cables_header as $column) {
                if($column == self::SOURCE || $column == self::TARGET) {
                    $row[] = array_search($cable[$column], $devicesMap);
                    continue;
                }
                
                $row[] = $cable[$column];
            }
            
            $data[] = $row;
        }
        
        return $data;
    }
    
    /**
     * Loads some network from database.
     * 
     * @throws Exception if some problem occurs.
     */
    private function load() {
        $id = $_GET["load"];
        $network = $this->loadNetwork($id);
        
        $devicesData = $this->loadDevices($id);
        
        $devicesWithId = $this->loadDevices($id, true);
        $devicesMap = array();
        foreach($devicesWithId as $device) {
            $devicesMap[] = $device[count(self::$devices_header) + 1];
        }
        
        $interfacesData = $this->loadInterfaces($id, $devicesMap);
        $cablesData = $this->loadCables($id, $devicesMap);
        
        $devices = $this->dataToSaveString($devicesData);
        $interfaces = $this->dataToSaveString($interfacesData);
        $cables = $this->dataToSaveString($cablesData);
        
        echo "\$data={$network}\n{$devices}\n{$interfaces}\n{$cables}";
    }
    
    /**
     * Removes some network from database.
     * 
     * @throws Exception if some problem occurs.
     */
    private function remove() {
        $id = $_GET["remove"];
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        if(!$user) {
            throw new Exception("Invalid user");
        }
        
        $where = self::NETWORK_ID . " = {$id} AND " . AdminUsers::ID . " = {$user[0][0]}";
        $network = $this->mysql->select(self::NETWORKS_TABLE, self::$networks_header_load, $where);
        if(!$network) {
            throw new Exception("Cannot remove network: no such network.");
        }
        
        $where = self::NETWORK_ID . " = {$id}";
        $this->mysql->delete(self::NETWORKS_TABLE, $where);
        $this->mysql->delete(self::DEVICES_TABLE, $where);
        $this->mysql->delete(self::INTERFACES_TABLE, $where);
        $this->mysql->delete(self::CABLES_TABLE, $where);
        
        echo $id . ";" . "Network was successfully removed from database.";
    }
}

?>

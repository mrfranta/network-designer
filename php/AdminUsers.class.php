<?php
/**
 * This class represents content of admin page which serves for users managing.
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class AdminUsers extends AdminPageContent {
    /** Name of column in database for user id. */
    const ID = "user_id";
    /** Name of column in database for user name. */
    const NAME = "user_name";
    /** Name of column in database for user password. */
    const PASS = "user_pass";
    /** Name of input in form for user password again. */
    const PASS_AGAIN = "user_pass_again";
    /** Name of column in database for user email. */
    const EMAIL = "user_email"; 
    /** Name of column in database for user level. */
    const LEVEL = "user_level";
    
    /** Name of table in database for users. */
    const TABLE = "tbl_users";
    
    /**
     * Constructs page content for managing users.
     * 
     * @param Page page which will contains this content.
     * @return AdminUsers Instance of this page content.
     */
    public function __construct(/*Page*/ $page) {
        parent::__construct($page);

        $this->assign("noDelete", array(1 => "admin"));
        
        return $this;
    }

    /**
     * Action for user deletion.
     * 
     * @param type $where (unused).
     * @param type $redirect (unused).
     */
    protected function deleteAction(/*String*/ $where = "", /*boolean*/ $redirect = true) {
        $mysql = MySQL::get_instance();
        
        $result = $mysql->select(self::TABLE, self::NAME, self::ID . " = {$_GET['id']}");
        if($result) {
            if($result[0][self::NAME] === "admin") {
                $this->addErrorMessage("This user cannot be removed.");
                $this->page->redirect();
                
                return;
            }
        }
        
        parent::deleteAction(self::ID . " = " . $_GET['id'], true);
    }
    
    /**
     * Action for editing some user.
     * 
     * @return string name of editing user.
     */
    protected function editAction() {
        $mysql = MySQL::get_instance();
        
        $item = array(self::NAME, self::EMAIL, self::LEVEL);
        $result = $mysql->select(self::TABLE, $item, self::ID . " = {$_GET['id']}");
        if($result === false) {
            $this->addErrorMessage("User with entered id was not found in database.");
            $this->getPage()->redirect();
            
            return;
        }
        
        $this->setFormValues(array(
            self::NAME => $result[0][self::NAME],
            self::EMAIL => $result[0][self::EMAIL],
            self::LEVEL => $result[0][self::LEVEL],
            self::SUBMIT => "Edit"
        ));
        
        $this->getForm()->getElementByName(self::PASS)->info .= 
                " If this field be empty, there will be no change of password.";
        
        return $result[0][self::NAME];
    }
    
    /**
     * Function which represents reaction for posting form.
     */
    protected function formAction() {
        $error = false;
        
        $name = $_POST[self::NAME];
        $element = $this->form->getElementByName(self::NAME);
        if(empty($name)) {
            $error = "User name cannot be empty.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        elseif(strlen($name) > 20) {
            $error = "User name is too long.";

            $this->addErrorMessage($error);
            $element->error = $error;
        }
        elseif(!$this->edit) {
            $mysql = MySQL::get_instance();
            if($mysql->select(self::TABLE, self::NAME, self::NAME . " = '{$name}'")) {
                $error = "User name is already exist in database.";
                
                $this->addErrorMessage($error);
                $element->error = $error;
            }
        }
        
        $password = $_POST[self::PASS];
        if(!$this->edit || ($this->edit && isset($password))) {
            $element = $this->form->getElementByName(self::PASS);
            if(strlen($password) < 5 || strlen($password) > 50) {
                $error = "Invalid length of user password.";

                $this->addErrorMessage($error);
                $element = $this->form->getElementByName(self::PASS);
                $element->error = $error;
            }
            else {
                $again = $_POST[self::PASS_AGAIN];
                if(strcmp($password, $again) !== 0) {
                    $error = "User password isn't equal to password again.";

                    $this->addErrorMessage($error);
                    $element = $this->form->getElementByName(self::PASS_AGAIN);
                    $element->error = $error;
                }
            }
        }

        $email = $_POST[self::EMAIL];
        $element = $this->form->getElementByName(self::EMAIL);
        if(empty($email)) {
            $error = "User e-mail cannot be empty.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = "Invalid user e-mail address.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
       elseif(!$this->edit) {
            $mysql = MySQL::get_instance();
            if($mysql->select(self::TABLE, self::NAME, self::EMAIL . " = '{$email}'")) {
                $error = "User e-mail is already exist in database.";
                
                $this->addErrorMessage($error);
                $element->error = $error;
            }
        }
                
        $level = $_POST[self::LEVEL];
        $element = $this->form->getElementByName(self::LEVEL);
        if(!isset($level)) {
            $error = "User level cannot be empty. It must be 'Basic user' or 'Admin'.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        else if(!preg_match("/^[01]$/", $level)) {
            $error = "Invalid type of user level. It must be 'Basic user'(0) or 'Admin'(1).";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        
        $values = array(
            self::NAME => $name,
            self::EMAIL => $email,
            self::LEVEL => $level
        );
        
        if(!$error) {
            $values[self::PASS] = md5($password);
            
            if($this->getEdit()) {
                $this->editValues($values, self::ID . " = " . $_GET['id']);
            }
            else {
                $this->insertValues($values);
            }
        }
        else {
            $this->setFormValues($values);
        }
    }
    
    /**
     * Returns array which contains names of columns from MySQL table for users.
     * Values from this columns will be shown in table.
     * 
     * @return array Array of table columns.
     */
    protected function getTableColumns() {
        return array(self::ID, self::NAME, self::EMAIL, self::LEVEL);
    }

    /**
     * Initializes and returns table header.
     * 
     * @return TableHeader Table header for table which contains created users.
     */
    protected function getTableHeader() {
        $header = new TableHeader();
        $columns = array(
            new TableHeaderColumn("Id", 50),
            new TableHeaderColumn("User name", 200),
            new TableHeaderColumn("E-mail", 250),
            new TableHeaderColumn("User type", 100),
        );
        $header->addColumns($columns);
        
        return $header;
    }
    
    /**
     * Initializes and returns data for table which contains created users.
     * 
     * @return string Data for table with created users.
     */
    protected function initTable() {
        $result = parent::initTable();      
        
        if($result) {
            for($i = 0; $i < count($result); $i++) {
                $result[$i][3] = ($result[$i][3] == 1) ? "admin" : "basic";
            }
        }

        return $result;
    }
    
    /**
     * Initializes and returns form elements for form.
     * 
     * @return array Array which contains form elements for form.
     */
    protected function initForm() {
        $elements = array();
        
        $elements[] = new FormInput(array(
            "label" => "Name:",
            "id" => self::NAME,
            "addition" => "maxlength=\"20\"",
            "info" => "Nonempty and unique user name (max 20 chars).",
        ));
        
        $elements[] = new FormInput(array(
            "label" => "Password:",
            "id" => self::PASS,
            "type" => "password",
            "addition" => "maxlength=\"50\"",
            "info" => "Nonempty user password (from 5 to 50 chars).",
        ));
        
        $elements[] = new FormInput(array(
            "label" => "Password again:",
            "id" => self::PASS_AGAIN,
            "type" => "password",
            "addition" => "maxlength=\"50\"",
            "info" => "User password again for verification.",
        ));
        
        $elements[] = new FormInput(array(
            "label" => "E-mail",
            "id" => self::EMAIL,
            "addition" => "maxlength=\"50\"",
            "info" => "Nonempty and unique user e-mail.",
        ));
        
        $elements[] = new FormSelect(array(
            "label" => "User type:",
            "id" => self::LEVEL,
            "options" => array(
                new FormSelectOption("0", "Basic user"),
                new FormSelectOption("1", "Admin")
            ),
            "selected" => "0",
            "info" => "Type of user. Basic user only can create and view created configuration. Admin can change content for designer."
        ));
        
        return $elements;
    }
}

?>

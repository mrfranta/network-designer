<?php
/**
 * This class represents content of admin page which serves for networks managing.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class AdminNetworks extends PageContent {
    /** Name of column in database for network id. */
    const ID = "network_id";
    /** Name of column in database for network name. */
    const NAME = "network_name";
    /** Name of column in database for network title. */
    const TITLE = "network_title";
    /** Name of column in database for network width. */
    const WIDTH = "network_width";
    /** Name of column in database for network height. */
    const HEIGHT = "network_height";
    /** Name of column in database for network background. */
    const BACKGROUND = "network_background";
    
    /** Name of table in database for networks. */
    const TABLE = "tbl_networks";
    
    /**
     * Serves for initialization of page content.
     */
    public function init() {
        
    }
    
    /**
     * Returns name of Smarty template for this page content.
     * 
     * @return string Name of template for this page content.
     */
    public function getTemplate() {
        
    }
    
    /**
     * Returns header of this page content.
     * 
     * @return string Header of this page content.
     */
    public function getHeader() {
        return "Networks";
    }

    /**
     * Returns title of this page content.
     * 
     * @return string Title of this page content.
     */
    public function getTitle() {
        return "Networks";
    }
}

?>

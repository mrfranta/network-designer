<?php
/**
 * This abstract class which serves for management with network.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
abstract class NetworksManagement {
    /**
     * MySQL connector.
     * @var NetworksManagementMySQL
     */
    protected $mysql;
    
   /**
    * Constructs management for networks.
    * 
    * @return NetworksManagement instance of this networks manager.
    * @throws Exception whether is no user logged.
    */
    public function __construct() {
        if(!$this->isLogged()) {
            throw new Exception("You have to be logged for work with configurations");
        }
        
        $this->mysql = NetworksManagementMySQL::get_instance();
        
        return $this;
    }
    
    /**
     * This abstract function will process network management.
     */
    public abstract function manage();
    
    /**
     * Returns information whether is someone logged or not.
     * 
     * @return boolean information whether is someone logged or not.
     */
    public function isLogged() {
        if(isset($_SESSION["logged"]) && $_SESSION["logged"] === true) {
            return true;
        }
        
        return false;
    }
}

?>

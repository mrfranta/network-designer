<?php
/**
 * Represents content of some page.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property-read string $title Title of this page content.
 * @property-read string $header Header of this page content.
 * @property-read string $template Smarty template for this page content.
 * @property-read Page $page Page which contains this page content.
 */
abstract class PageContent {
    /**
     * Page which contains this page content.
     * @var Page
     */
    private $page;
    
    /**
     * Constructs page content.
     * 
     * @param Page $page page which contains this page content.
     * @return PageContent instance of this page content.
     */
    public function __construct(/*Page*/ $page) {
        if(!is_object($page) || !($page instanceof Page)) {
            $this->error("Invalid page", "Invalid page", 
                    "For page content was entered wrong parameter as <tt>page</tt>");
        }
        
        $this->page = $page;
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }
    
    /**
     * This abstract function serves for initialization of page content.
     */
    public abstract function init();
    
    /**
     * Shows error page and ends execution.
     * 
     * @param string $title title of error page.
     * @param string $header header of error page.
     * @param string $text text of error page.
     * 
     * @see Error
     */
    protected function error(/*string*/ $title = "", /*string*/ $header = "", /*string*/ $text = "") {
        $error = new Error($title, $header, $text);
        $error->printOut();
    }
    
    /**
     * This abstract function will returns title of this page content.
     * 
     * @return string Title of this page content.
     */
    public abstract function getTitle();
    
    /**
     * This abstract function will returns header of this page content.
     * 
     * @return string Header of this page content.
     */
    public abstract function getHeader();
    
    /**
     * This abstract function will returns name of Smarty template for this page content.
     * 
     * @return string Name of template for this page content.
     */
    public abstract function getTemplate();
    
    /**
     * Assigns a Smarty variable
     *
     * @param array|string $tpl_var the template variable name(s)
     * @param mixed $value the value to assign
     * @param boolean $nocache if true any output of this variable will be not cached
     * @return Instance of this page content.
     */
    protected function assign(/*array|string*/ $tpl_var, /*mixed*/ $value = null, /*mixed*/ $nocache = false) {
        $this->page->assign($tpl_var, $value, $nocache);
        
        return $this;
    }
    
    /**
     * Adds error message into owner of this page content.
     * 
     * @param string $text text of error message.
     * @return PageContent instance of this page content.
     */
    public function addErrorMessage(/*string*/ $text) {
        $this->page->addErrorMessage($text);
        
        return $this;
    }
    
    /**
     * Adds info message into owner of this page content.
     * 
     * @param string $text text of message.
     * @return PageContent instance of this page content.
     */
    public function addMessage(/*string*/ $text) {
        $this->page->addMessage($text);
        
        return $this;
    }
    
   /**
     * Adds array of page messages.
     * 
     * @param array $messages page messages which will be added.
     * @return PageContent instance of this page content.
     */
    public function addMessages(/*array*/ $messages) {
        $this->page->addMessages($messages);
        
        return $this;
    }
    
    /**
     * Returns page which contains this content.
     * 
     * @return Page page which contains this content.
     */
    public function getPage() {
        return $this->page;
    }
}

?>

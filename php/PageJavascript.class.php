<?php
/**
 * Represents javascript object for page. This is only data handler for some 
 * smarty template which contains javascript construction.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $template Name of smarty template.
 * @property mixed $data Data for javascript.
 */
class PageJavascript {
    /**
     * Name of smarty template.
     * @var string
     */
    private $template;
    /**
     * Data for javascript.
     * @var mixed
     */
    private $data;
    
    /**
     * Constructs javascript object for page.
     * 
     * @param string $template name of smarty template.
     * @param mixed $data data for javascript.
     * @return PageJavascript instance of this javascript object.
     */
    public function __construct(/*string*/ $template, /*mixed*/ $data = null) {
        $this->template = $template;
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        unset($this->$name);
    }
    
    /**
     * Returns name of smarty template.
     * 
     * @return string name of smarty template.
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * Sets name of smarty template.
     * 
     * @param string $template name of smarty template.
     * @return PageJavascript instance of this javascript object.
     */
    public function setTemplate(/*string*/ $template) {
        $this->template = $template;
        
        return $this;
    }

    /**
     * Returns data for Javascript.
     * 
     * @return mixed Data for Javascript.
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Sets data for Javascript.
     * 
     * @param mixed $data data for Javascript.
     * @return PageJavascript instance of this javascript object.
     */
    public function setData(/*mixed*/ $data = null) {
        $this->data = $data;
        
        return $this;
    }
}
?>

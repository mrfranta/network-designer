<?php
/**
 * This class represents popup message for page.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $text Text of message.
 * @property string $type Type of this message - it can be only: info or error.
 */
class PageMessage {
    /**
     * Text of message.
     * @var string 
     */
    private $text;
    /**
     * Type of this message - it can be only: info or error.
     * @var string
     */
    private $type;
    
    /**
     * Constructs page message.
     * 
     * @param string $text text of message.
     * @param string $type type of this message - it can be only: info or error.
     * @return PageMessage Instance of this page message.
     */
    public function __construct(/*string*/ $text, /*string*/ $type = "") {
        $this->setText($text);
        $this->setType($type);
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get($name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set($name, $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset($name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset($name) {
        unset($this->$name);
    }
    
    /**
     * Returns text of this message.
     * 
     * @return string text of this message.
     */
    public function getText() {
        return stripcslashes($this->text);
    }
    
    /**
     * Sets text of this message.
     * 
     * @param string $text text of this message.
     * @return PageMessage Instance of this page message.
     */
    public function setText(/*string*/ $text) {
        $this->text = addslashes($text);
        
        return $this;
    }
    
    /**
     * Returns type of this message.
     * 
     * @return string type of this mesage.
     */
    public function getType() {
        return stripcslashes($this->type);
    }
    
    /**
     * Sets type of this message.
     * 
     * @param string $type type of this message - it can be only: info or error.
     * @return PageMessage Instance of this page message.
     */
    public function setType(/*string*/ $type = "") {
        $this->type = empty($type) ? "" : addslashes($type);
        
        return $this;
    }
    
    /**
     * Returns string value of this message in format:
     * <pre>
     *     new MessageBox($text, $type)
     * </pre>
     * 
     * This method is used for printing messages into javascript which will show 
     * them.
     *
     * @return string String value of this message. 
     */
    public function __toString() {
        $type = "";
        if(!empty($this->type)) {
            $type = ", '{$this->type}'";
        }
        
        return "new MessageBox('{$this->text}'{$type})";
    }
}

?>

<?php
class MySQL {
    private $link;
    private static $instance;

    protected function __construct() {
        if(!$this->link = @mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS)) {
            $this->error("Connection error", "Cannot connect to MySQL!");
        }

        if(!@mysql_selectdb(MYSQL_DB, $this->link)) {
            $this->error("Connection error", "Unable to connect to database!");
        }

        @mysql_set_charset(MYSQL_CHARSET, $this->link);

        return $this;
    }

    protected function error(/*String*/ $header, /*String*/ $text) {
        $error = new Error("MySQL error", $header, $text . "\n<br />MySQL error: " . mysql_error());            
        $error->printOut();
    }

    public function close() {
        if(!@mysql_close($this->link)) {
            $this->error("Disconnection error", "Unable to close the connection to MySQL!");
        }

        return true;
    }

    public function string_escape(/*String*/ $str) {
        $len = strlen($str);
        $escapeCount = 0;
        $targetString = '';
        for($offset=0; $offset < $len; $offset++) {
            switch($c = $str{$offset}) {
                case "'":
                // Escapes this quote only if its not preceded by an unescaped backslash
                    if($escapeCount % 2 == 0) {
                        $targetString .= "\\";
                    }

                    $escapeCount = 0;
                    $targetString .= $c;
                    break;

                case '"':
                // Escapes this quote only if its not preceded by an unescaped backslash
                    if($escapeCount % 2 == 0) {
                        $targetString .= "\\";
                    }

                    $escapeCount = 0;
                    $targetString .= $c;
                       break;

                case '\\':
                    $escapeCount++;
                    $targetString .= $c;
                    break;

                default:
                    $escapeCount=0;
                    $targetString.=$c;
            }
        }

        return $targetString;
    } 

    public function query(/*String*/ $sql) {
        if(!$result = @mysql_query($sql, $this->link)) {
            $this->error("Query execution error", "Failed to execute MySQL query: <tt>" . $sql . "</tt>!");
        }

        return $result;
    }

    public function insert(/*String*/ $table, /*String*/ $columns, /*Mixed*/ $values) {
        if(is_array($values)) {
            $val = "";
            $keys = array_keys($values);

            foreach($keys as $key) {
                $columns .= "$key, ";

                if(is_int($values[$key])) {
                    $val .= $values[$key] . ", ";
                    continue;
                }
                
                if($values[$key] === "NOW()") {
                    $val .= "NOW(), ";
                    continue;
                }

                $val .= "'" . $this->string_escape($values[$key]) . "', ";
            }

            $columns = preg_replace("/, $/", "", $columns);
            $values = preg_replace("/, $/", "", $val);
        }

        if(!empty($columns)) {
            $columns = "(" . $columns . ")";
        }

        return $this->query("INSERT INTO " . $table . $columns . "  VALUES (" . $values . ")");
    }

    public function insert_id() {
        return mysql_insert_id($this->link);
    }

    public function update(/*String*/ $table, /*Mixed*/ $set, /*String*/ $where = "") {        
        if(is_array($set)) {
            $set_tmp = "";
            $keys = array_keys($set);

            foreach($keys as $key) {
                if(is_int($set[$key])) {
                    $set_tmp .= "$key = {$set[$key]}, ";
                    continue;
                }
                
                if($set[$key] === "NOW()") {
                    $set_tmp .= "$key = NOW(), ";
                    continue;
                }

                $set_tmp .= "$key = '" . $this->string_escape($set[$key]) . "', ";
            }

            $set = preg_replace("/, $/", "", $set_tmp);
        }

        if(!empty($where)) {
            $where = " WHERE " . $where;
        }

        return $this->query("UPDATE " . $table . " SET " . $set . $where);
    }

    public function select(/*String*/ $table, /*String*/ $item = "*", /*String*/ $where = "", /*String*/ $limit = "", /*String*/ $order = "") {
        if(is_array($item)) {
            $item_tmp = "";
            $keys = array_keys($item);
            foreach($keys as $key) {
                $item_tmp .= $this->string_escape($item[$key]) . ", ";
            }

            $item = preg_replace("/, $/", "", $item_tmp);
        }

        if(!empty($where)) {
            $where = " WHERE " . $where;
        }

        if(!empty($limit)) {
            $limit = " LIMIT " . $limit;
        }

        if(!empty($order)) {
            $order = " ORDER BY " . $order;
        }

        $result = $this->query("SELECT " . $item . " FROM `" . $table . "`" . $where . $order . $limit);

        if(!mysql_num_rows($result)) {
            return false;
        }


        return $this->fetch_all($result);
    }

    public function delete(/*String*/ $table, /*Array*/ $where) {
        if(!empty($where)) {
            $where = " WHERE " . $where;
        }

        return $this->query("DELETE FROM `" . $table . "`" . $where);
    }

    public function truncate(/*String*/ $table) {
        return $this->query("TRUNCATE `" . $table . "`");
    }

    public function drop(/*String*/ $item, /*String*/ $name) {
        if(preg_match("/^(TABLE)|(DATABASE)$/", strtoupper($item))) {
            return $this->query("DROP " . $item . " `" . $this->string_escape($name) . "`");
        }
    }

    public function fetch_all(/*Mixed*/ $result) {
        $fetch = array();
        for($i = 0; ($data = mysql_fetch_array($result)); $i++) {
            $keys = array_keys($data);
            for($j = 0; $j < count($keys, 1); $j++) {
                $fetch[$i][$keys[$j]] = $data[$keys[$j]];
            } 
        }

        return $fetch;    
    }

    public static function get_instance() {
        if(!is_object(self::$instance)) {
            return self::$instance = new MySQL();
        }

        return self::$instance;
    }
    
    public static function is_opened() {
        return is_object(self::$instance);
    }
}
?>
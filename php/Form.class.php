<?php
/**
 * This class represents form.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property array $elements Array of form elements.
 * @property string $enctype Enctype of this form.
 */
class Form {
    /**
     * Array of form elements.
     * @var array
     */
    private $elements;
    /**
     * Enctype of this form.
     * @var type 
     */
    private $enctype;
    
    /**
     * Constructs form with entered elements and enctype.
     * 
     * @param array|FormElement $elements one or more form elements.
     * @param string $enctype enctype of this form.
     */
    public function __construct(/*array|FormElement*/ $elements = null, /*string*/ $enctype = "") {
        $this->elements = array();
        $this->addElements($elements);
        
        if(!empty($enctype)) {
            $this->enctype = $enctype;
        }
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        unset($this->$name);
    }
    
    /**
     * Add form element(s) into this form.
     * 
     * @param array|FormElement $elements one or more form elements to add.
     * @return Form instance of this form.
     */
    public function addElements(/*array|FormElement*/ $elements) {
        if(is_array($elements)) {
            $keys = array_keys($elements);
            foreach($keys as $key) {
                $element = $elements[$key];
                if($element instanceof FormElement) {
                    $this->elements[$element->getName()] = $element;
                }
            }
        }
        else if(is_object($elements) && $elements instanceof FormElement) {
            $this->elements[$elements->getName()] = $elements;
        }
        
        return $this;
    }
    
    /**
     * Removes form element(s) into this form.
     * 
     * @param array|FormElement $elements one or more form elements to remove.
     * @return Form instance of this form.
     */
    public function removeElements(/*array|FormElement*/ $elements) {
        if(is_array($elements)) {
            $keys = array_keys($elements);
            foreach($keys as $key) {
                $element = $elements[$key];
                if($element instanceof FormElement) {
                    unset($this->elements[$element->getName()]);
                }
            }
        }
        else if(is_object($elements) && $elements instanceof FormElement) {
            unset($this->elements[$elements->getName()]);
        }
        
        return $this;
    }

    /**
     * Returns FormElement with entered name.
     * 
     * @param string $name name of form element.
     * @return FormElement If this form contains form element with entered name 
     * it returns that element; null otherwise.
     */
    public function getElementByName(/*string*/ $name) {
        if(!empty($name)) {
            return $this->elements[$name];
        }
        
        return null;
    }
    
    /**
     * Returns array of form elements.
     * 
     * @return array Array of form elements.
     */
    public function getElements() {
        return $this->elements;
    }
    
    /**
     * Returns enctype for this form.
     * 
     * @return string enctype for this form.
     */
    public function getEnctype() {
        return $this->enctype;
    }
    
    /**
     * Sets enctype for this form.
     * 
     * @param string $enctype Enctype for this form.
     * @return Form instance of this form.
     */
    public function setEnctype(/*string*/ $enctype) {
        $this->enctype = $enctype;
        
        return $this;
    }
    
    /**
     * Returns Smarty template file for this form.
     * 
     * @return string Smarty template file for this form.
     */
    public function getTemplate() {
        return "form";
    }
}

?>

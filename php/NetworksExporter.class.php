<?php

/**
 * Description of NetworkExporter
 *
 * @author Mr.FrAnTA
 */
class NetworksExporter extends NetworksManagement {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function manage() {
        if(isset($_GET["export"])) {
            $this->export();
        }
        else {
            throw new Exception("Invalid operation for manager.");
        }
    }
    
    private function toXMLTag($key) {
        $str = "" . $key;
        $ret = "";
        $space = false;
        for($i = 0; $i < strlen($str); $i++) {
            $c = $str{$i};
            if($c === "_") {
                $space = true;
                continue;
            }
            
            if($space) {
                $ret .= strtoupper($c);
                $space = false;
                
                continue;
            }
            
            $ret .= $c;
        }
        
        return $ret;
    }
    
    private function export() {
        $id = $_GET["export"];
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        if(!$user) {
            throw new Exception("Invalid user");
        }
        
        $where = AdminNetworks::ID . " = {$id} AND " . AdminUsers::ID . " = {$user[0][0]}";
        $item = array(AdminNetworks::NAME, AdminNetworks::TITLE, AdminNetworks::WIDTH, AdminNetworks::HEIGHT, AdminNetworks::BACKGROUND);
        $network = $this->mysql->select(AdminNetworks::TABLE, $item, $where);
        if(!$network) {
            throw new Exception("Cannot export network: no such network.");
        }
        
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>' . '<network></network>');
        $network = $network[0];
        $keys = array_keys($network);
        foreach($keys as $key) {
            if(!is_numeric($key)) {
                $xml->addChild($this->toXMLTag($key), $network[$key]);
            }
        }
        
        $where = NetworksManager::NETWORK_ID . " = {$id}";
        $devices = $this->mysql->select(NetworksManager::DEVICES_TABLE, "*", $where);
        if($devices) {
            $devicesXML = $xml->addChild("devices");
            $i = 0;
            foreach ($devices as $device) {
                $keys = array_keys($device);
                $deviceXML = $devicesXML->addChild("device");
                foreach($keys as $key) {
                    if(is_numeric($key) || $key == NetworksManager::NETWORK_ID || $key == NetworksManager::DEVICE_ID) {
                        continue;
                    }                    

                    $deviceXML->addChild($this->toXMLTag($key), $device[$key]);
                }
            
                $interfaces = $this->mysql->select(NetworksManager::INTERFACES_TABLE, "*", $where . " AND " . NetworksManager::DEVICE_ID . " = " . $device[NetworksManager::DEVICE_ID]);
                if($interfaces) {
                    $interfacesXML = $deviceXML->addChild("interfaces");
                    foreach($interfaces as $interface) {
                       $keys = array_keys($interface);
                       $interfaceXML = $interfacesXML->addChild("interface");
                       foreach($keys as $key) {
                           if(is_numeric($key) || $key == NetworksManager::NETWORK_ID || $key == NetworksManager::INTERFACE_ID) {
                               continue;
                           }

                           $value = $interface[$key];
                           if($key == NetworksManager::DEVICE_ID) {
                               $value = $i;
                           }

                           $interfaceXML->addChild($this->toXMLTag($key), $value);
                        }
                    }
                }
                
                $i++;
            }
        }
        
        $devicesMap = array();
        foreach($devices as $device) {
            $devicesMap[] = $device[NetworksManager::DEVICE_ID];
        }
        
        $cables = $this->mysql->select(NetworksManager::CABLES_TABLE, "*", $where);
        if($cables) {
            $cablesXML = $xml->addChild("cables");
            foreach($cables as $cable) {
                $keys = array_keys($cable);
                $cableXML = $cablesXML->addChild("cable");
                foreach($keys as $key) {
                    if(is_numeric($key) || $key == NetworksManager::NETWORK_ID || $key == NetworksManager::CABLE_ID) {
                        continue;
                    }
                    
                    $value = $cable[$key];
                    if($key == NetworksManager::SOURCE || $key == NetworksManager::TARGET) {
                        $value = array_search($cable[$key], $devicesMap);
                    }
                    
                    $cableXML->addChild($this->toXMLTag($key), $value);
                }
            }
        }
        
        
        $xmlStr = $xml->asXML();
        header('Content-Description: File Transfer');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . self::editImageName($network[AdminNetworks::NAME]) . '.xml');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . strlen($xmlStr));
        header('Content-type: text/xml');

        echo $xmlStr;
    }
    
    /**
     * Returns name of XML file in right form (no accents, spaces etc) - only characters 
     * with a-z, A-Z, 0-9.
     * 
     * @param string $xmlName old image iname.
     * @return string Name of XML file in right form.
     * @static
     */
    public static function editImageName($xmlName) {
        setlocale(LC_ALL, 'cs_CZ.utf8');
        $xmlName = iconv('utf-8', 'ascii//TRANSLIT', $xmlName);
        $xmlName = preg_replace("/'/", "", $xmlName);
        $xmlName = strtolower($xmlName);
        $xmlName = preg_replace("/[^a-zA-Z0-9\-_]/", "_", $xmlName);

        return $xmlName;
    }
}

?>

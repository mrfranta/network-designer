<?php
/**
 * Represents page for designer.
 *
 * @author Mr.FrAnTA
 */
class DesignerPage extends Page {
    /**
     * MySQL connector.
     * @var MySQL
     */
    private $mysql;
    
    /**
     * Constructs designer page.
     * 
     * @return DesignerPage Instance of this designer page.
     */
    public function __construct() {        
        parent::__construct();
        
        if(isAdminPage()) {
            $this->initTemplatesDirectories("admin/");
        }
        $this->assign("designer", true);

        $this->setTitle("Designer");
        $this->mysql = MySQL::get_instance();
        
        return $this;
    }
    
    /**
     * Initializes designer page.
     * 
     * @return DesignerPage Instance of this designer page.
     */
    public function init() {
        if(isAdminPage()) {
            $this->adminInit();
        }
        else {
            $this->spectratorInit();
        }
        
        return $this;
    }
    
    /**
     * Initializes designer page for networks administration.
     */
    private function adminInit() {
        $item = array(AdminCategories::ID, AdminCategories::NAME, AdminCategories::IMAGE);
        $where = AdminCategories::NAME . " != '(none)'";
        $result = $this->mysql->select(AdminCategories::TABLE, $item, $where);
        
        $categories = $result;
        $categories[] = array(AdminCategories::ID => "0", 
            AdminCategories::NAME => "Cables", 
            AdminCategories::IMAGE => AdminCables::CATEGORY_IMG);
        
        $this->assign("categories", $categories);
        
        $devices = array();
        foreach($result as $row) {
            $category = $row[0];
            $devicesInCategory = array();
            
            $item = array(AdminDevices::ID, AdminDevices::NAME, AdminDevices::IMAGE, AdminDevices::PORTS);
            $where = AdminCategories::ID . " = '{$row[AdminCategories::ID]}'";
            $dbDevices = $this->mysql->select(AdminDevices::TABLE, $item, $where);
            if($dbDevices) {
                foreach($dbDevices as $device) {
                    $deviceObj = new DesignerDevice($device[AdminDevices::NAME], 
                            $device[AdminDevices::IMAGE], $device[AdminDevices::PORTS]);
                    $deviceObj->id = $device[AdminDevices::ID];
                    $devicesInCategory[] = $deviceObj;
                }
            }
            
            $devices[$category] = $devicesInCategory;
        }

        $this->assign("devices", $devices);
        
        $cables = array();
        $dbCables = $this->mysql->select(AdminCables::TABLE);
        if($dbCables) {
            foreach($dbCables as $cable) {
                $cableObj = new DesignerCable($cable[AdminCables::NAME], $cable[AdminCables::COLOR],
                        $cable[AdminCables::IMAGE], $cable[AdminCables::LINE]);
                $cableObj->id = $cable[AdminCables::ID];
            
                $cables[] = $cableObj;
            }
        }
        
        $this->assign("cablesTab", count($categories));
        $this->assign("cables", $cables);
        
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        $dbNetworks = $this->mysql->select(NetworksManager::NETWORKS_TABLE, "*", AdminUsers::ID . " = '{$user[0][0]}'");
        $this->assign("networks", $dbNetworks);
        
        $this->addJavascript(new PageJavascript("admin/cables", $cables));
    }
    
    /**
     * Initializes designer page for network viewing.
     */
    private function spectratorInit() {
        $user = $this->mysql->select(AdminUsers::TABLE, AdminUsers::ID, AdminUsers::NAME . " = '{$_SESSION["user"]}'");
        $dbNetworks = $this->mysql->select(NetworksManager::NETWORKS_TABLE, "*", AdminUsers::ID . " = '{$user[0][0]}'");
        $this->assign("networks", $dbNetworks);
        
        $cables = array();
        $dbCables = $this->mysql->select(AdminCables::TABLE);
        if($dbCables) {
            foreach($dbCables as $cable) {
                $cableObj = new DesignerCable($cable[AdminCables::NAME], $cable[AdminCables::COLOR],
                        $cable[AdminCables::IMAGE], $cable[AdminCables::LINE]);
                $cableObj->id = $cable[AdminCables::ID];
            
                $cables[] = $cableObj;
            }
        }
        $this->addJavascript(new PageJavascript("admin/cables", $cables));
    }
    
    /**
     * Returns name of Smarty template for this designer page.
     * 
     * @return string Name of template for this designer page.
     */
    public function getTemplate() {        
        return "designer";
    }    
}

?>

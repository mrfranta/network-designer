<?php
/**
 * Represents cable structure which will be printed in designer.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property int $id Identifier number of cable.
 * @property string $name Cable name.
 * @property-read string $constantName Name of constant which will be used in designer.
 * @property string $color Color of cable line.
 * @property string $line Type of cable line.
 * @property string $image Address of image for this cable.
 */
class DesignerCable {
    /**
     * Identifier number of cable.
     * @var int
     */
    private $id;
    /**
     * Cable name.
     * @var string
     */
    private $name;
    /**
     * Name of constant which will be used in designer.
     * @var string
     */
    private $constantName;
    /**
     * Color of cable line.
     * @var string
     */
    private $color;
    /**
     * Type of cable line.
     * @var string
     */
    private $line = "solid";
    /**
     * Address of image for this cable.
     * @var string
     */
    private $image;
    
    /**
     * Constructs cable structure for designer.
     * 
     * @param string $name Cable name.
     * @param string $color of cable line.
     * @param string $image address of image for this cable.
     * @param string $line type of cable line.
     * @return DesignerCable instance of this cable structure.
     */
    public function __construct(/*String*/ $name, /*String*/ $color, /*String*/ $image, /*String*/ $line = "solid") {
        $this->setName($name);
        $this->color = $color;
        $this->line = $line;
        $this->image = $image;
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        if($name == "constantName") {
            return;
        }
        
        unset($this->$name);
    }
    
    /**
     * Returns identifier number of cable.
     * 
     * @return int Identifier number of cable.
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Sets identifier number of cable.
     * 
     * @param int $id identifier number of cable.
     * @return DesignerCable instance of this cable structure.
     */
    public function setId(/*int*/ $id) {
        $this->id = $id;
        
        return $this;
    }

    /**
     * Returns name of constant which will be used in designer.
     * 
     * @return string Name of constant which will be used in designer.
     */
    public function getConstantName() {
        return $this->constantName;
    }
    
    /**
     * Sets name of constant which will be used in designer.
     * 
     * @param string $name name of constant which will be used in designer.
     * @return DesignerCable instance of this cable structure.
     */
    private function setConstantName(/*string*/ $name) {
        setlocale(LC_ALL, 'cs_CZ.utf8');
        $this->constantName = iconv('utf-8', 'ascii//TRANSLIT', $name);
        $this->constantName = preg_replace("/'/", "", $this->constantName);
        $this->constantName = strtolower($this->constantName);
        $this->constantName = preg_replace("/[^a-zA-Z0-9\_]/", "_", $this->constantName);
        $this->constantName = strtoupper($this->constantName);
        
        return $this;
    }
    
    /**
     * Returns cable name.
     * 
     * @return string Cable name.
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets cable name.
     * 
     * @param string $name cable name.
     * @return DesignerCable instance of this cable structure.
     */
    public function setName(/*string*/ $name) {
        $this->name = $name;
        $this->setConstantName($name);
        
        return $this;
    }

    /**
     * Returns color of cable line.
     * 
     * @return string Color of cable line.
     */
    public function getColor() {
        return $this->color;
    }

    /**
     * Sets color of cable line.
     * 
     * @param string $color color of cable line.
     * @return DesignerCable instance of this cable structure.
     */
    public function setColor(/*string*/ $color) {
        $this->color = $color;
        
        return $this;
    }

    /**
     * Returns address of image for this cable.
     * 
     * @return string Address of image for this cable.
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Sets address of image for this cable.
     * 
     * @param string $image address of image for this cable.
     * @return DesignerCable instance of this cable structure.
     */
    public function setImage(/*string*/ $image) {
        $this->image = $image;
        
        return $this;
    }
    
    /**
     * Returns type of cable line.
     * 
     * @return string type of cable line.
     */
    public function getLine() {
        return $this->line;
    }

    /**
     * Sets type of cable line.
     * 
     * @param string $line type of cable line.
     * @return DesignerCable instance of this cable structure.
     */
    public function setLine(/*string*/ $line) {
        $this->line = $line;
        
        return $this;
    }
}

?>

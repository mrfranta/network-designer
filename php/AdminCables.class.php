<?php
/**
 * This class represents content of admin page which serves for cables managing.
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class AdminCables extends AdminPageContent {
    /** Name of column in database for cable id. */
    const ID = "cable_id";
    /** Name of column in database for cable name. */
    const NAME = "cable_name";
    /** Name of column in database for cable color. */
    const COLOR = "cable_color";
    /** Name of column in database for cable line type. */
    const LINE = "cable_line";
    /** Name of column in database for cable icon image. */
    const IMAGE = "cable_image";
    
    /** Name of table in database for cables. */
    const TABLE = "tbl_cables";
    
    /** Image for category which contains cables. */
    const CATEGORY_IMG = "images/categories/cables.png";
    
    /**
     * Constructs page content for managing cables.
     * 
     * @param Page page which will contains this content.
     * @return AdminCables Instance of this page content.
     */
    public function __construct($page) {
        parent::__construct($page);
        
        return $this;
    }

    /**
     * Action for editing some cable.
     * 
     * @return string name of editing cable.
     */
    protected function editAction() {
        $mysql = MySQL::get_instance();
        
        $item = array(self::NAME, self::COLOR, self::LINE, self::IMAGE);
        $result = $mysql->select(self::TABLE, $item, self::ID . " = {$_GET['id']}");
        if($result === false) {
            $this->addErrorMessage("Cable with entered id was not found in database.");
            $this->getPage()->redirect();
            
            return;
        }
        
        $this->setFormValues(array(
            self::NAME => $result[0][self::NAME],
            self::COLOR => $result[0][self::COLOR],
            self::LINE => $result[0][self::LINE],
            self::IMAGE . "_old" => $result[0][self::IMAGE],
            self::SUBMIT => "Edit"
        ));
        
        return $result[0][self::NAME];
    }
    
    /**
     * Action for cable deletion.
     * 
     * @param string $where (unused).
     * @param boolean $redirect (unused).
     */
    protected function deleteAction(/*string*/ $where = "", /*boolean*/ $redirect = true) {
        $data = parent::deleteAction(self::ID . " = " . $_GET['id'], false);
        @unlink($this->getImage($data[0][self::IMAGE]));
        
        $this->getPage()->redirect();
    }

    /**
     * Function which represents reaction for posting form.
     */
    protected function formAction() {
        $error = false;
        
        $name = $_POST[self::NAME];
        $element = $this->form->getElementByName(self::NAME);
        if(empty($name)) {
            $error = "Name of cable cannot be empty.";
            
            $this->addErrorMessage($error);  
            $element->error = $error;
        }
        elseif(strlen($name) > 20) {
            $error = "Name of cable is too long.";

            $this->addErrorMessage($error);  
            $element->error = $error;
        }
        elseif(!$this->edit) {
            $mysql = MySQL::get_instance();
            if($mysql->select(self::TABLE, self::NAME, self::NAME . " = '{$name}'")) {
                $error = "Entered name of cable is already exist in database.";
                
                $this->addErrorMessage($error);  
                $element->error = $error;
            }
        }

        $color = $_POST[self::COLOR];
        $element = $this->form->getElementByName(self::COLOR);
        if(empty($color)) {
            $error = "Cable color cannot be empty. It must be hexadecimal value of color.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        else if(!preg_match("/^([0-9a-fA-F]{3}){1,2}$/", $color)) {
            $error = "Cable color is in invalid form. It must be hexadecimal value of color.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        
        $line = $_POST[self::LINE];
        $element = $this->form->getElementByName(self::LINE);
        if(empty($line)) {
            $error = "Cable line type cannot be empty. It must be 'Solid', 'Dashed' or 'Dotted'.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        else if(!preg_match("/^(solid)|(dashed)|(dotted)$/", $line)) {
            $error = "Invalid type of cable line. It must be 'Solid', 'Dashed' or 'Dotted'.";
            
            $this->addErrorMessage($error);
            $element->error = $error;
        }
        
        $image = $_FILES[self::IMAGE];
        $path = "";
        if(empty($image["name"])) {
            if(!$this->getEdit()) {
                $error = "Image of cable wasn't selected.";
                $this->addErrorMessage($error);

                $element = $this->form->getElementByName(self::IMAGE);
                $element->error = $error;
            }
        }
        else {
            if(!$error) {
                $uploader = new ImageUploader();
                $result = $uploader->uploadImage($image, $this->getPage()->page, $name);
                
                if(!$result) {
                    $element = $this->form->getElementByName(self::IMAGE);
                    $element->error = $uploader->getError();
                    $error = true;
                }
                
                $path = $result;
            }
        }
        
        $values = array(
            self::NAME => $name,
            self::COLOR => $color,
            self::LINE => $line
        );
        
        if(!$error) {
            if($this->getEdit()) {
                $values[self::IMAGE] = $this->renameImage($path, $_POST[self::IMAGE . "_old"], $name);
                
                $this->editValues($values, self::ID . " = " . $_GET['id']);
            }
            else {
                $values[self::IMAGE] = $path;
                $this->insertValues($values);
            }
        }
        else {
            if(isset($_POST[self::IMAGE . "_old"])) {
                $values[self::IMAGE . "_old"] = $_POST[self::IMAGE . "_old"];
            }
            
            $this->setFormValues($values);
        }
    }

    /**
     * Returns array which contains names of columns from MySQL table for cables.
     * Values from this columns will be shown in table.
     * 
     * @return array Array of table columns.
     */
    protected function getTableColumns() {
        $columns = array(self::ID, self::NAME, self::COLOR, self::LINE, self::IMAGE);
        
        return $columns;
    }
    
    /**
     * Initializes and returns table header.
     * 
     * @return TableHeader Table header for table which contains created cables.
     */
    protected function getTableHeader() {
        $header = new TableHeader();
        $columns = array(
            new TableHeaderColumn("Id", 50),
            new TableHeaderColumn("Name", 200),
            new TableHeaderColumn("Color", 100),
            new TableHeaderColumn("Line type", 100),
            new TableHeaderColumn("Image", 50)
        );
        $header->addColumns($columns);
        
        return $header;
    }

    /**
     * Initializes and returns form elements for form.
     * 
     * @return array Array which contains form elements for form.
     */
    protected function initForm() {
        $elements = array();
        
        $elements[] = new FormInput(array(
            "label" => "Name:",
            "id" => self::NAME,
            "addition" => "maxlength=\"20\"",
            "info" => "Nonempty and unique name of cable (max 20 chars).",
        ));
        
        $elements[] = new FormInput(array(
            "label" => "Color:",
            "id" => self::COLOR,
            "class" => "color",
            "addition" => "maxlength=\"6\"",
            "info" => "Hexadecimal value of color of line which represents cable."
        ));
        
        $elements[] = new FormSelect(array(
            "label" => "Line type:",
            "id" => self::LINE,
            "options" => array(
                new FormSelectOption("solid", "Solid"),
                new FormSelectOption("dashed", "Dashed"),
                new FormSelectOption("dotted", "Dotted"),
            ),
            "selected" => "solid",
            "info" => "Type of line for cable."
        ));
        
        if($this->getEdit()) {
            $elements[] = new FormInput(array(
                "label" => "Old image:",
                "id" => self::IMAGE . "_old",
                "class" => "readonly",
                "type" => "input", 
                "addition" => "readonly=\"readonly\"", 
                "info" => "Address of old cable image."
            ));
        }
        
        $elements[] = new FormInput(array(
            "label" => $this->getEdit() ? "New image:" : "Image:",
            "id" => self::IMAGE,
            "type" => "file", 
            "addition" => "accept='image/*'", 
            "info" => "Allowed image formats: *.jpg, *.gif, *.png<br />" .  
                      "Maximum image size: " . ImageUploader::MAX_IMAGE_SIZE . " MB<br />" .
                      "Maximum image resolution: " . ImageUploader::MAX_SIZE . "x" . ImageUploader::MAX_SIZE . "<br />"
        ));

        $this->getForm()->setEnctype("multipart/form-data");
        
        return $elements;
    }
    
    /**
     * Initializes and returns data for table which contains created cables.
     * 
     * @return string Data for table with created cables.
     */
    protected function initTable() {
        $result = parent::initTable();
        if(is_array($result)) {
            for($i = 0; $i < count($result); $i++) {
                $result[$i][4] = "<img src=\"{$this->getImage($result[$i][4])}\" style=\"width: 16px; height: 16px;\" alt=\"{$result[$i][1]}\" />";
            }
        }
        
        return $result;
    }
}

?>

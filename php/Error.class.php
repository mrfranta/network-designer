<?php
/**
 * This class represents error page which shows error and exits php execution.
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $message Error message (text of error).
 */
class Error extends Page {
    /**
     * Error message (text of error).
     * @var string
     */
    private $message;

    /**
     * Constructs error page.
     * 
     * @param string $title title of this error page.
     * @param string $header
     * @param string $message error message (text of error).
     * @return Error instance of this error page.
     */
    public function __construct(/*string*/ $title = "", /*string*/ $header = "", /*string*/ $message = "") {
        parent::__construct();
        
        $this->setTitle($title);
        $this->setHeader($header);
        $this->setMessage($message);
        
        $this->assign("admin", isAdminPage());
        
        return $this;
    }
    
    /**
     * This function do nothing because it will be endless recursion.
     * 
     * @param string $title (unused)
     * @param string $header (unused)
     * @param string $text (unused)
     */
    protected function error($title = "", $header = "", $text = "") {
        // it will be endless recursion...
    }

    /**
     * Returns error message (text of error).
     * 
     * @return string Error message (text of error).
     */
    public function getMessage() {
        return $this->message;
    }
    
    /**
     * Sets error message (text of error).
     * 
     * @param string $message error message (text of error).
     * @return Error instance of this error page.
     */
    public function setMessage(/*string*/ $message) {
        $this->message = $message;
        $this->assign("message", $this->message);

        return $this;
    }
    
    /**
     * Returns name of Smarty template for this error page.
     * 
     * @return string Name of template for this error page.
     */
    public function getTemplate() {
        return "error";
    }
    
    /**
     * Prints out this error page using smarty templates and exits php execution.
     */
    public function printOut() {
        parent::printOut();
        exit(1);
    }
}

?>

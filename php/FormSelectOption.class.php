<?php
/**
 * This class represents element in form (for example: input).
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $value Value of option.
 * @property string $content Content of option.
 */
class FormSelectOption {
    /**
     * Value of option.
     * @var string
     */
    protected $value;
    /**
     * Content of option.
     * @var string
     */
    protected $content;
    
    /**
     * Constructs option for form select.
     * 
     * @param string $value value of option.
     * @param string $content content of option. If entered content is empty, it 
     * will be set option value as content.
     * @return FormSelectOption Instance of this form select option.
     */
    public function __construct(/*string*/ $value, /*string*/ $content = "") {
        $this->value = $value;
        $this->content = $content;
        
        if(empty($content)) {
            $this->content = $value;
        }
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        unset($this->$name);
    }
    
    /**
     * Returns value of this select option.
     * 
     * @return string Value of this select option.
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Sets value of this select option.
     * 
     * @param string $value value of this select option.
     * @return FormSelectOption Instance of this form select option.
     */
    public function setValue(/*string*/ $value) {
        $this->value = $value;
        
        return $this;
    }

    /**
     * Returns content of this select option.
     * 
     * @return string Content of this select option.
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Sets content of this select option.
     * 
     * @param string $content content of select option.
     * @return FormSelectOption Instance of this form select option.
     */
    public function setContent(/*string*/ $content) {
        $this->content = $content;
        
        return $this;
    }
}

?>

<?php
/**
 * This class represents content of admin page which serves for devices managing.
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class AdminDevices extends AdminPageContent {
    /** Name of column in database for device id. */
    const ID = "device_id";
    /** Name of column in database for category id. */
    const CATEGORY_ID = AdminCategories::ID;
    /** Name of column in database for category name. */
    const CATEGORY_NAME = AdminCategories::NAME;
    /** Name of column in database for device name. */
    const NAME = "device_name";
    /** Name of column in database for device image. */
    const IMAGE = "device_image";
    /** Name of column in database for device ports. */
    const PORTS = "device_ports";
    
    /** Name of table in database for devices. */
    const TABLE = "tbl_devices";
    /** Name of table in database for categories. */
    const JOIN_TABLE = AdminCategories::TABLE;
    
    /**
     * Constructs page content for managing devices.
     * 
     * @param Page page which will contains this content.
     * @return AdminDevices Instance of this page content.
     */
    public function __construct($page) {
        parent::__construct($page);
        
        return $this;
    }
    
    /**
     * Action for device deletion.
     * 
     * @param string $where (unused).
     * @param boolean $redirect (unused).
     */
    protected function deleteAction(/*string*/ $where = "", /*boolean*/ $redirect = true) {
        $data = parent::deleteAction(self::ID . " = " . $_GET['id'], false);
        @unlink($this->getImage($data[0][self::IMAGE]));
        
        $this->getPage()->redirect();
    }
    
    /**
     * Action for editing some device.
     * 
     * @return string name of editing device.
     */
    protected function editAction() {
        $mysql = MySQL::get_instance();
        
        $item = array(self::NAME, self::CATEGORY_ID, self::IMAGE, self::PORTS);
        $result = $mysql->select(self::TABLE, $item, self::ID . " = {$_GET['id']}");
        if($result === false) {
            $this->addErrorMessage("Device with entered id was not found in database.");
            $this->getPage()->redirect();
            
            return;
        }
        
        $this->setFormValues(array(
            self::NAME => $result[0][self::NAME],
            self::CATEGORY_ID => $result[0][self::CATEGORY_ID],
            self::IMAGE . "_old" => $result[0][self::IMAGE],
            self::PORTS => $result[0][self::PORTS],
            self::SUBMIT => "Edit"
        ));
        
        return $result[0][self::NAME];
    }
    
    /**
     * Function which represents reaction for posting form.
     */
    protected function formAction() {
        $error = false;
        
        $name = $_POST[self::NAME];
        $element = $this->form->getElementByName(self::NAME);
        if(empty($name)) {
            $error = "Name of device cannot be empty.";
            
            $this->addErrorMessage($error);  
            $element->error = $error;
        }
        elseif(strlen($name) > 20) {
            $error = "Name of device is too long.";

            $this->addErrorMessage($error);  
            $element->error = $error;
        }
        
        $category = $_POST[self::CATEGORY_ID];
        $element = $this->form->getElementByName(self::CATEGORY_ID);
        if(empty($category) || !is_numeric($category)) {
            $error = "Choosen invalid device category.";

            $this->addErrorMessage($error);  
            $element->error = $error;
        }
        else {
            $mysql = MySQL::get_instance();
            if(!$mysql->select(self::JOIN_TABLE, self::CATEGORY_NAME, self::CATEGORY_ID . " = {$category}")) {
                $error = "Choosen device category doesn't exist.";

                $this->addErrorMessage($error);  
                $element->error = $error;
            }
        }
        
        $ports = $_POST[self::PORTS];
        if(empty($ports) || !is_numeric($ports)) {
            $ports = 0;
        }
        
        $image = $_FILES[self::IMAGE];
        $path = "";
        if(empty($image["name"])) {
            if(!$this->getEdit()) {
                $error = "Image of device wasn't selected.";
                $this->addErrorMessage($error);

                $element = $this->form->getElementByName(self::IMAGE);
                $element->error = $error;
            }
        }
        else {
            if(!$error) {
                $uploader = new ImageUploader();
                $result = $uploader->uploadImage($image, $this->getPage()->page, $name);
                
                if(!$result) {
                    $element = $this->form->getElementByName(self::IMAGE);
                    $element->error = $uploader->getError();
                    $error = true;
                }
                
                $path = $result;
            }
        }
        
        $values = array(
            self::NAME => $name,
            self::CATEGORY_ID => $category,
            self::PORTS => $ports
        );
        
        if(!$error) {
            if($this->getEdit()) {
                $values[self::IMAGE] = $this->renameImage($path, $_POST[self::IMAGE . "_old"], $name);
                
                $this->editValues($values, self::ID . " = " . $_GET['id']);
            }
            else {
                $values[self::IMAGE] = $path;
                $this->insertValues($values);
            }
        }
        else {
            if(isset($_POST[self::IMAGE . "_old"])) {
                $values[self::IMAGE . "_old"] = $_POST[self::IMAGE . "_old"];
            }
            
            $this->setFormValues($values);
        }
    }
    
    /**
     * Returns array which contains names of columns from MySQL table for devices.
     * Values from this columns will be shown in table.
     * 
     * @return array Array of table columns.
     */
    protected function getTableColumns() {
        return array(self::ID, self::NAME, self::IMAGE, self::PORTS);
    }
    
    /**
     * Initializes and returns table header.
     * 
     * @return TableHeader Table header for table which contains created devices.
     */
    protected function getTableHeader() {
        $header = new TableHeader();
        $columns = array(
            new TableHeaderColumn("Id", 50),
            new TableHeaderColumn("Name", 200),
            new TableHeaderColumn("Image", 50),
            new TableHeaderColumn("Ports", 50),
        );
        $header->addColumns($columns);
        
        return $header;
    }
    
    /**
     * Initializes and returns data for table which contains created devices.
     * 
     * @return string Data for table with created categories.
     */
    protected function initTable() {
        $mysql = MySQL::get_instance();

        $this->assign("elementName", $this->getElementName());
        $this->assign("tableTitle", $this->getTitle() . " in database");
        $this->assign("tableHeader", $this->getTableHeader()->getColumns());
        
        $categories = $mysql->select(self::JOIN_TABLE, "*");
        if(!$categories) {
            AdminCategories::recovery();
            $categories = $mysql->select(self::JOIN_TABLE, "*");
        }
        
        for($i = 0; $i < count($categories); $i++) {
            $where = self::CATEGORY_ID . " = {$categories[$i][self::CATEGORY_ID]}";
            $devices = $mysql->select(self::TABLE, $this->getTableColumns(), $where);
            if($devices) {
                for($j = 0; $j < count($devices); $j++) {
                    $devices[$j][2] = "<img src=\" {$this->getImage($devices[$j][2])}\" style=\"width: 16px; height: 16px;\" alt=\"{$devices[$j][1]}\" />";
                }
                
                $categories[$i]["devices"] = $devices;
            }
            else {
                $categories[$i]["devices"] = array();
            }
        }
        
        $this->assign("categories", $categories);
        
        $tabs = new PageJavascript("admin/tabs");
        $this->page->addJavascript($tabs);
        
        return false;
    }
    
    /**
     * Initializes and returns form elements for form.
     * 
     * @return array Array which contains form elements for form.
     */
    protected function initForm() {
        $elements = array();
        
        $elements[] = new FormInput(array(
            "label" => "Name:",
            "id" => self::NAME,
            "addition" => "maxlength=\"20\"",
            "info" => "Nonempty name of device (max 20 chars).",
        ));
        
        $categories = array();
        
        $mysql = MySQL::get_instance();
        $result = $mysql->select(self::JOIN_TABLE);
        if(!$result) {
            AdminCategories::recovery();
            $result = $mysql->select(self::JOIN_TABLE);
        }
       
        foreach($result as $row) {
            $categories[] = new FormSelectOption($row[0], $row[1]);
        }
        
        $elements[] = new FormSelect(array(
            "label" => "Category:",
            "id" => self::CATEGORY_ID,
            "options" => $categories,
            "selected" => "0",
            "info" => "Category for device. Devices in (none) category will be not displayed"
        ));
        
        if($this->getEdit()) {
            $elements[] = new FormInput(array(
                "label" => "Old image:",
                "id" => self::IMAGE . "_old",
                "class" => "readonly",
                "type" => "input", 
                "addition" => "readonly=\"readonly\"", 
                "info" => "Address of old device image."
            ));
        }
        
        $elements[] = new FormInput(array(
            "label" => $this->getEdit() ? "New image:" : "Image:",
            "id" => self::IMAGE,
            "type" => "file", 
            "addition" => "accept='image/*'", 
            "info" => "Allowed image formats: *.jpg, *.gif, *.png<br />" .  
                      "Maximum image size: " . ImageUploader::MAX_IMAGE_SIZE . " MB<br />" .
                      "Maximum image resolution: " . ImageUploader::MAX_SIZE . "x" . ImageUploader::MAX_SIZE . "<br />"
        ));

        $elements[] = new FormInput(array(
            "label" => "Ports",
            "id" => self::PORTS,
            "addition" => "maxlength=\"3\"",
            "value" => "0",
            "info" => "Number of ports in device. Zero indicates an unspecified number of ports that the user enters himself.",
        ));
        
        $this->getForm()->setEnctype("multipart/form-data");
        
        return $elements;
    }
    
    /**
     * Returns name of Smarty template for this page content.
     * 
     * @return string Name of template for this page content.
     */
    public function getTemplate() {
        return "devices";
    }
}
?>

<?php
/**
 * Special MySQL connector for NetworksManagement which doesn't use Error pages but 
 * throws Exceptions on mysql error.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class NetworksManagementMySQL extends MySQL {
    /**
     * Signleton instance of NetworksManagementMySQL.
     * @var NetworksManagementMySQL
     * @static
     */
    private static $instance;
    
    /**
     * Constructs special MySQL connector for NetworksManagement.
     * 
     * @return NetworksManagementMySQL Instance of NetworksManagementMySQL.
     */
    protected function __construct() {
        parent::__construct();
        
        return $this;
    }
    
    /**
     * Throws exeception which contains text of MySQL error.
     * 
     * @param string $header (unused).
     * @param string $text text of MySQL error.
     * @throws Exception allways throws exception with error.
     */
    protected function error(/*string*/ $header, /*string*/ $text) {
        throw new Exception($text . ": " . mysql_error());
    }
    
    /**
     * Returns instance of NetworksManagementMySQL. This function is only way, how 
     * to constructs this MySQL connector. This pattern has name: "Singleton pattern".
     * 
     * @return NetworksManagementMySQL Signleton instance of NetworksManagementMySQL.
     * @static
     */
    public static function get_instance() {
        if(!is_object(self::$instance)) {
            return self::$instance = new NetworksManagementMySQL();
        }

        return self::$instance;
    }
    
    /**
     * Returns information whether was opened MySQL connection.
     * 
     * @return boolean Information whether is MySQL opened.
     * @static
     */
    public static function is_opened() {
        return is_object(self::$instance);
    }
}

?>

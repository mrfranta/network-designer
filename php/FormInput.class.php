<?php
/**
 * This class represents form element input. For example:
 * <pre>
 *     <input id="foo" name="bar" type="text" value="Input value" />
 * </pre>
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $type Type of form input.
 */
class FormInput extends FormElement {
    /**
     * Type of form input.
     * @var string
     */
    protected $type = "text";
    
    /**
     * Constructs form input with specified parameters.
     * 
     * @param array $params array of parameters of form input.
     * @return FormInput instance of this form input.
     */
    public function __construct(/*array*/ $params = null) {
        parent::__construct($params);
        
        return $this;
    }
    
    /**
     * Returns type of this form input.
     * 
     * @return string Type of this input.
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Sets type of this form input.
     * 
     * @param string $type type of this form input.
     * @return FormInput instance of this form input.
     */
    public function setType(/*string*/ $type = "text") {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * Returns smarty template file for this form input.
     * 
     * @return string Smarty template file for this form input.
     */
    public function getTemplate() {
        return "input";
    }
}
?>

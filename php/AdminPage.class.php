<?php
/**
 * This class represents administration page.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property-read AdminMenu $menu Menu for this administration page.
 */
class AdminPage extends Page {
    /**
     * Menu for this administration page.
     * @var AdminMenu 
     */
    private $menu;
    /**
     * Right actual page (?page=$page).
     * @var String
     */
    private $rightPage;
    
    /** Page name - constant for default title and header. */
    const PAGE_NAME = "Network Designer Administration";
    
    /**
     * Constructs administration page for entered page.
     * 
     * @param String $page actual page.
     * @return AdminPage instance of this admin page.
     */
    public function __construct(/*String*/ $page = "") {
        parent::__construct($page);
        
        if(!$this->isAdmin()) {
            $this->redirect("designer.php");
        }
        
        $this->menu = new AdminMenu();
        $this->assign("items", $this->menu->getItems());
        
        $this->initTemplatesDirectories("admin/");
        
        return $this;
    }
    
    /**
     * Returns information whether logged user is admin.
     * 
     * @return boolean information whether logged user is admin.
     */
    protected function isAdmin() {
        if(isset($_SESSION["admin"])) {
            return ($_SESSION["admin"] == 1);
        }
        
        return false;
    }
    
    /**
     * Sets actual page (?page=$page).
     * 
     * @param String $page actual page.
     * @return AdminPage instance of this admin page.
     */
    protected function setPage(/*string*/ $page) {
        parent::setPage("Admin" . ucfirst($page));
        
        $this->rightPage = $page;
        
        return $this;
    }
    
    /**
     * Returns right actual page (?page=$page).
     * 
     * @return string actual page.
     */
    public function getPage() {
        return $this->rightPage;
    }

    /**
     * Returns menu for this administration page.
     * 
     * @return AdminMenu Menu for this administration page.
     */
    public function getMenu() {
        return $this->menu;
    }
    
    /**
     * Returns name of Smarty template for this administration page.
     * 
     * @return string Name of template for this administration page.
     */
    public function getTemplate() {
        if(!empty($this->content)) {
            $this->assign("content", $this->content->getTemplate());
        }

        return "body";
    }
}

?>

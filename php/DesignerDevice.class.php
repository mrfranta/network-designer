<?php
/**
 * Represents device structure which will be printed in designer.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property int $id Identifier number of device.
 * @property string $name Device name.
 * @property string $category Category of this device.
 * @property string $image Address of image for this device.
 * @property int $ports Number of ports in this device.
 */
class DesignerDevice {
    /**
     * Identifier number of device.
     * @var int
     */
    private $id;
    /**
     * Device name.
     * @var string
     */
    private $name;
    /**
     * Category of this device.
     * @var string
     */
    private $category;
    /**
     * Address of image for this device.
     * @var string
     */
    private $image;
    /**
     * Number of ports in this device.
     * @var int
     */
    private $ports;
    
    /**
     * Constructs device structure for designer.
     * 
     * @param string $name device name.
     * @param string $image address of image for this device.
     * @param string $ports number of ports in this device. This parameter is optional and default value is 0.
     * @return DesignerDevice instance of this device structure.
     */
    public function __construct(/*string*/ $name, /*string*/ $image, /*int*/ $ports = 0) {
        $this->name = $name;
        $this->image = $image;
        $this->ports = $ports;
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {        
        unset($this->$name);
    }
    
    /**
     * Returns identifier number of device.
     * 
     * @return int Identifier number of device.
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Sets identifier number of device.
     * 
     * @param int $id identifier number of device.
     * @return DesignerDevice instance of this device structure.
     */
    public function setId($id) {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * Returns device name.
     * 
     * @return string Device name.
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Sets device name.
     * 
     * @param string $name device name.
     * @return DesignerDevice instance of this device structure.
     */
    public function setName(/*String*/ $name) {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * Returns category of this device.
     * 
     * @return string category of this device.
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Sets category of this device.
     * 
     * @param string $category category of this device.
     * @return DesignerDevice instance of this device structure.
     */
    public function setCategory(/*String*/ $category) {
        $this->category = $category;
        
        return $this;
    }
     
    /**
     * Returns address of image for this device.
     * 
     * @return string Address of image for this device.
     */
    public function getImage() {
        return $this->image;
    }
    
    /**
     * Address of image for this device.
     * 
     * @param string $image address of image for this device.
     * @return DesignerDevice instance of this device structure.
     */
    public function setImage(/*String*/ $image) {
        $this->image = $image;
        
        return $this;
    }
    
    /**
     * Returns number of ports in this device.
     * 
     * @return int Number of ports in this device.
     */
    public function getPorts() {
        return $this->ports;
    }
    
    /**
     * Sets number of ports in this device.
     * 
     * @param int $ports number of ports in this device.
     * @return DesignerDevice instance of this device structure.
     */
    public function setPorts(/*int*/ $ports = 0) {
        $this->ports = $ports;
        
        return $this;
    }
}

?>

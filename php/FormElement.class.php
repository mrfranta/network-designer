<?php
/**
 * This class represents element in form (for example: input).
 * 
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $id Identifier of this element.
 * @property string $class Class(es) of this element.
 * @property string $name Name of this element.
 * @property string $label Label for this element.
 * @property string $value Value of this element.
 * @property string $addition Additional information for this element.
 * 
 * @property string $info Informatins about this element.
 * @property string $error Error informations about this element.
 * 
 * @property-read string $template Smarty template file for this element.
 */
abstract class FormElement {
    /**
     * Identifier of this element.
     * @var string
     */
    protected $id;
    /**
     * Class(es) of this element.
     * @var string 
     */
    protected $class;
    /**
     * Name of this element.
     * @var string
     */
    protected $name;
    /**
     * Label for this element.
     * @var string
     */
    protected $label;
    /**
     * Value of this element.
     * @var string
     */
    protected $value;
    /**
     * Additional information for this element.
     * @var string
     */
    protected $addition;
    
    /**
     * Informatins about this element.
     * @var string
     */
    protected $info;
    /**
     * Error informations about this element.
     * @var string
     */
    protected $error;
    
    /**
     * Constructs form element with specified parameters.
     * 
     * @param array $params array of parameters of form element.
     * @return FormElement instance of this form element.
     */
    public function __construct(/*array*/ $params = null) {
        if(is_array($params)) {
            $keys = array_keys($params);
            foreach($keys as $key) {
                $this->$key = $params[$key];
                
                if($key == "id" && empty($this->name)) {
                    $this->name =  $this->id;
                }
            }
        }
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        unset($this->$name);
    }
    
    /**
     * Returns identifier of this element.
     * 
     * @return string identifier of this element.
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Sets identifier of this element. If name of this element is empty, this 
     * function also sets name.
     * 
     * @param type $id identifier of this element.
     * @return FormElement instance of this form element.
     */
    public function setId(/*string*/ $id) {
        $this->id = $id;
        
        if(empty($this->name)) {
            $this->name = $this->id;
        }
        
        return $this;
    }
    
    /**
     * Returns class(es) of this element.
     * 
     * @return string class(es) of this element.
     */
    public function getClass() {
        return $this->class;
    }
    
    /**
     * Sets class(es) of this element.
     * 
     * @param string $class class(es) of this element.
     * @return FormElement instance of this form element.
     */
    public function setClass(/*string*/ $class) {
        $this->class = $class;
        
        return $this;
    }
    
    /**
     * Returns name of this element.
     * 
     * @return string Name of this element.
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Sets name of this element.
     * 
     * @param string $name name of this element.
     * @return FormElement instance of this form element.
     */
    public function setName(/*string*/ $name) {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * Returns label for this element.
     * 
     * @return string Label for this element.
     */
    public function getLabel() {        
        return $this->label;
    }
    
    /**
     * Sets label for this element.
     * 
     * @param string $label label for this element.
     * @return FormElement instance of this form element.
     */
    public function setLabel(/*string*/ $label) {
        $this->label = $label;
        
        return $this;
    }
    
    /**
     * Returns value of this element.
     * 
     * @return string Value of this element.
     */
    public function getValue() {
        return $this->value;
    }
    
    /**
     * Sets value of this element.
     * 
     * @param string $value value of this element.
     * @return FormElement instance of this form element.
     */
    public function setValue(/*string*/ $value) {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * Returns additional information for this element.
     * 
     * @return string Additional information for this element.
     */
    public function getAddition() {
        return $this->addition;
    }
    
    /**
     * Returns additional information for this element.
     * 
     * @param string $addition additional information for this element.
     * @return FormElement instance of this form element.
     */
    public function setAddition(/*string*/ $addition) {
        $this->addition = $addition;

        return $this;
    }
    
    /**
     * Returns informatins about this element.
     * 
     * @return string informatins about this element.
     */
    public function getInfo() {
        return $this->info;
    }
    
    /**
     * Sets informatins about this element.
     * 
     * @param string $info informatins about this element.
     * @return FormElement instance of this form element.
     */
    public function setInfo(/*string*/ $info) {
        $this->info = $info;
        
        return $this;
    }
    
    /**
     * Returns error informations about this element.
     * 
     * @return string error informations about this element.
     */
    public function getError() {
        return $this->error;
    }
    
    /**
     * Sets error informations about this element.
     * @param string $error error informations about this element.
     * @return FormElement instance of this form element.
     */
    public function setError($error) {
        $this->error = $error;
        
        if(strpos($this->class, "error") === false) {
            $this->class = empty($this->class) ? "error" : $this->class . " error";
        }
        
        return $this;
    }

    /**
     * This abstract function will returns smarty template file for this element.
     * 
     * @returns string Smarty template file for this element.
     */
    public abstract function getTemplate();
}

?>

<?php
/**
 * This class represents content of admin page for creating, editing and removing some 
 * elements.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property-read Form $form Form which is in this content of admin page.
 * @property-read boolean $edit Information whether is some element editing.
 */
abstract class AdminPageContent extends PageContent {
    /**
     * Form which is in this content of admin page.
     * @var type 
     */
    private $form;
    /**
     * Information whether is some element editing.
     * @var boolean
     */
    private $edit;
    /**
     * Information whether this content of admin page is initialized.
     * @var boolean
     */
    private $init;
    
    /** Name and id of submit input. */
    const SUBMIT = "submit";
    /** Name and id of cancel input. */
    const CANCEL = "cancel";
    /** Name of table for content. This constant must be overriden. */
    const TABLE = "tbl_unknown";
    
    /**
     * Constructs content of admin page for creating, editing and removing some 
     * elements.
     * 
     * @param Page $page page which contains this page content.
     * @return AdminPageContent instance of this content of admin page.
     */
    public function __construct($page) {
        parent::__construct($page);

        $this->form = new Form();
        $this->edit = false;
        $this->init = false;
        
        return $this;
    }
    
    /**
     * Initializes this contant of admin page.
     * 
     * @return AdminPageContent instance of this content of admin page.
     */
    public function init() {
        if($this->init === true) {
            return $this;
        }
        
        if(isset($_POST["cancel"])) {
            $this->getPage()->redirect();
        }
        
        if(isset($_GET["action"])) {
            $action = $_GET["action"];
            
            if($action === "edit") {
                if(!isset($_GET["id"])) {
                    $this->addErrorMessage("The <tt>id</tt> of " . $this->getElementName() . " wasn't entered.");
                    $this->getPage()->redirect();
                }
                else {
                    $this->edit = true;
                }
            }
            elseif($action == "delete") {
                if(!isset($_GET["id"])) {
                    $this->addErrorMessage("The <tt>id</tt> of " . $this->getElementName() . " wasn't entered.");
                    $this->getPage()->redirect();
                }
                else {
                    $this->deleteAction();
                }
            }
            else {
                $this->error("Invalid action", "Invalid action", "Invalid action for this page.");
            }
        }
        
        $this->assign("formTitle", "Form for new " . $this->getElementName());
        $this->form->addElements($this->initForm());
        $this->form->addElements(new FormInput(array(
            "id" => self::SUBMIT,
            "type" => "submit",
            "value" => "Create"
        )));        
        
        if(isset($_POST[static::SUBMIT])) {
            $this->formAction();        
        }
        else if($this->edit) {
            $element = $this->editAction();
            $this->assign("formTitle", "Form for " . $this->getElementName() . " \"{$element}\"");
        }
        
        $this->assign("formElements", $this->form->getElements()); 
        $this->assign("formEnctype", $this->form->getEnctype()); 
        
        $table = $this->initTable();
        if($table) {
            $this->assign("tableData", $table);
        }
        
        $this->init = true;
        
        return $this;
    }
    
    /**
     * Returns correct path for entered relative image path.
     * 
     * @param string $path relative path for image.
     * @return string Correct path for image.
     */
    protected function getImage($path) {
        return ROOT . $path;
    }
    
    /**
     * Renames image with entered path into new one.
     * 
     * @param string $path specific path for image.
     * @param string $old old path for image.
     * @param string $name new name of image.
     * @return string New path for renamed image.
     */
    protected function renameImage($path, $old, $name) {
        if(!empty($path)) {
            if($path != $old) {
                @unlink($this->getImage($old));
            }
            
            return $path;
        }
        else {
            $old = $this->getImage($old);
            $path_parts = pathinfo($old);
            $img_name = ImageUploader::editImageName($name);
            $new = "{$path_parts['dirname']}/{$img_name}.{$path_parts['extension']}";
            $i = 2;
            while(file_exists($path)) {
                $new = "{$path_parts['dirname']}/{$img_name}{$i}.{$path_parts['extension']}";
                $i++;
            }

            if(rename($old, $new)) {
                if(strpos($new, "../") == 0) {
                    return substr($new, 3);
                }

                return $new;
            }
        }
        
        return $old;
    }

    /**
     * Initializes and returns form elements for form.
     * 
     * @return array Array which contains form elements for form.
     */
    protected abstract function initForm();
    
    /**
     * Sets values from entered array into form.
     * 
     * @param array $values values which will be entered into form.
     * @return AdminPageContent instance of this content of admin page.
     */
    protected function setFormValues(/*array*/ $values) {
        if(is_array($values)) {
            foreach(array_keys($values) as $key) {
                $this->form->getElementByName($key)->value = $values[$key];
            }
        }
        
        return $this;
    }
    
    /**
     * This function will represents reaction for posting form.
     */
    protected abstract function formAction();
    
    /**
     * This function will represents action for editing some element.
     * 
     * @return string name of editing some element
     */
    protected abstract function editAction();
    
    /**
     * Action for element deletion.
     * 
     * @param string $where where contition for deletion.
     * @param boolean $redirect determines whether will be this page redirected after deletion.
     */
    protected function deleteAction(/*string*/ $where = "", /*boolean*/ $redirect = true) {
        if(empty($where)) {
            $where = "{$this->getElementIDColumn()} = {$_GET['id']}";
        }
        
        $mysql = MySQL::get_instance();
        $result = $mysql->select(static::TABLE, "*", $where);
        if($result === false) {
            $this->addErrorMessage(ucfirst($this->getElementName()) . " with entered id was not removed from database.");
        }
        else {
            $mysql->delete(static::TABLE, $where);
            $this->addMessage(ucfirst($this->getElementName()) . " with entered id was removed from database.");
        }
        
        if($redirect) {
            $this->getPage()->redirect();
        }
        
        return $result;
    }
    
    /**
     * Insets values into database.
     * 
     * @param array $values values which will be inserted into database.
     * @return AdminPageContent instance of this content of admin page.
     */
    protected function insertValues(/*array*/ $values) {
        if(is_array($values)) {
            $mysql = MySQL::get_instance();
            if($mysql->insert(static::TABLE, "", $values)) {
                $this->addMessage(ucfirst($this->getElementName()) . " was created.");
            }
        }
        
        return $this;
    }
    
    /**
     * Edit (resp. updates) data in database.
     * 
     * @param array $values values to update.
     * @param string $where where condition for update query.
     */
    protected function editValues(/*array*/ $values, $where) {
        if(is_array($values)) {
            if(empty($where)) {
                $where = "{$this->getElementIDColumn()} = {$_GET['id']}";
            }
            
            $mysql = MySQL::get_instance();
            if($mysql->update(static::TABLE, $values, $where)) {
                $this->addMessage(ucfirst($this->getElementName()) . " was edited.");
            }
        }
        
        $this->getPage()->redirect();
    }
    
    /**
     * Returns name of id column in MySQL table for this element.
     * 
     * @return string Name of ID column in MySQL table.
     */
    protected function getElementIDColumn() {
        return "{$this->getElementName()}_id";
    }
    
    /**
     * This function will initializes and returns table header.
     * 
     * @return TableHeader Table header for table which contains created elements.
     */
    protected abstract function getTableHeader();
    
    /**
     * This function will returns array which contains names of columns from 
     * MySQL table for cables. Values from this columns will be shown in table.
     * 
     * @return array Array of table columns.
     */
    protected abstract function getTableColumns();
    
    /**
     * This function will initializes and returns data for table which contains 
     * created elements.
     * 
     * @return string Data for table with created elements.
     */
    protected function initTable() {
        $mysql = MySQL::get_instance();

        $this->assign("elementName", $this->getElementName());
        $this->assign("tableTitle", $this->getTitle() . " in database");
        $this->assign("tableHeader", $this->getTableHeader()->getColumns());
        $result = $mysql->select(static::TABLE, $this->getTableColumns(), "", "", $this->getElementIDColumn());       
        
        return $result;
    }
    
    /**
     * Returns form which is in this admin page.
     * 
     * @return Form form which is in this admin page.
     */
    public function getForm() {
        return $this->form;
    }
    
    /**
     * Returns information whether is some element editing
     * 
     * @return boolean information whether is some element editing.
     */
    public function getEdit() {
        return $this->edit;
    }
    
    /**
     * Returns name of element which will be edited in this page content.
     *
     * @return string name of element which will edited in this page content.
     */
    public function getElementName() {
        return substr($this->getPage()->page, 0, -1);
    }
    
    /**
     * Returns title of this page content.
     * 
     * @return string Title of this page content.
     */
    public function getTitle() {        
        return ucfirst($this->getPage()->page);
    }

    /**
     * Returns header of this page content.
     * 
     * @return string Header of this page content.
     */
    public function getHeader() {
        return $this->getTitle();
    }
    
    /**
     * Returns name of Smarty template for this page content.
     * 
     * @return string Name of template for this page content.
     */
    public function getTemplate() {    
        return "page";
    }
}

?>

<?php
/**
 * This class represents menu in administration.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class AdminMenu extends Menu {
    /**
     * Constructs menu for administration.
     * 
     * @return AdminMenu instance of this menu.
     */
    public function __construct() {
        parent::__construct("Admin menu");
        
        $this->addMenuItem(new PageMenuItem("users", "Users"));
        $this->addMenuItem(new PageMenuItem("categories", "Devices categories"));
        $this->addMenuItem(new PageMenuItem("devices", "Devices"));
        $this->addMenuItem(new PageMenuItem("cables", "Cables"));
        $this->addMenuItem(new MenuItem("designer.php", "Designer"));
        $this->addMenuItem(new MenuItem("?logout", "Logout"));
        
        return $this;
    }
}

?>

<?php
/**
 * Returns server address.
 * 
 * @return String server address.
 */
function getServerAddress() {
    if(array_key_exists('SERVER_ADDR', $_SERVER)) {
        return $_SERVER['SERVER_ADDR'];
    }
    elseif(array_key_exists('LOCAL_ADDR', $_SERVER)) {
        return $_SERVER['LOCAL_ADDR'];
    }
    elseif(array_key_exists('SERVER_NAME', $_SERVER)) {
        return gethostbyname($_SERVER['SERVER_NAME']);
    }
    else {
        // Running CLI
        if(stristr(PHP_OS, 'WIN')) {
            return gethostbyname(php_uname("n"));
        } else {
            $ifconfig = shell_exec('/sbin/ifconfig eth0');
            $match = array();
            preg_match('/addr:([\d\.]+)/', $ifconfig, $match);
            
            return $match[1];
        }
    }
}

/* ===== Configuration of connection for MySQL database ===== */
$server = getServerAddress();
if($server == "localhost" || $server == "127.0.0.1" || $server == "::1") {
    define("MYSQL_HOST", "localhost"); // MySQL server
    define("MYSQL_USER", "root"); // user for server
    define("MYSQL_PASS", "kocka"); // user password
    define("MYSQL_DB", "network_designer"); // database
}
else { // host informations
    define("MYSQL_HOST", "mysql.freehostingnoads.net"); // MySQL server
    define("MYSQL_USER", "u661357616_root"); // user for server
    define("MYSQL_PASS", "kocka5"); // user password
    define("MYSQL_DB", "u661357616_nd"); // database
}

define("MYSQL_CHARSET", "utf8");

/* ===== Configuration of root ===== */
if(preg_match("/(admin)/", $_SERVER["PHP_SELF"])) {
    define("ROOT", "../");
}
else {
    define("ROOT", "./");
}

/**
 * Returns information whether actual page is administration.
 * 
 * @return boolean Information whether actual page is administration.
 */
function isAdminPage() {
    return ROOT === '../';
}

define("PHP_DIR", __DIR__ . "/");

/* ===== Configuration of classloader ===== */

/**
 * Returns path for entered class (classpath).
 * 
 * @param String $class name of class for which will be returned classpath.
 * @return String path for entered class.
 */
function classpath(/*String*/ $class) {
    return (PHP_DIR. "{$class}.class.php");
}

/**
 * Autoloader for loading class files.
 * 
 * @param String $class class which will be loaded.
 */
function autoloader(/*String*/ $class) {  
    if(strpos($class, "Smarty") === 0) {
        return;
    }
    
    $classpath = classpath($class);    

    require_once($classpath);
}

spl_autoload_register('autoloader');

/* ===== Configuration of Smarty ===== */
define("SMARTY_TEMPLATES", PHP_DIR . "../templates/");
define("SMARTY_TEMPLATES_COMPILE", PHP_DIR . "../templates_c/");
define("SMARTY_CONFIGS", PHP_DIR . "../configs/");
define("SMARTY_CACHE", PHP_DIR . "../cache/"); 

/* ===== Configuration of Server properties ===== */
$_SERVER["REQUEST_URI"] = str_replace("&", "&amp;", $_SERVER["REQUEST_URI"]);

$_SERVER["REQUEST_PAGE"] = $_SERVER["PHP_SELF"];
if(isset($_GET["page"])) {
    $_SERVER["REQUEST_PAGE"] .= "?page=" . $_GET["page"];
}
?>

<?php
/**
 * Represents table header which contains columns.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property-read array $columns Columns of table header.
 */
class TableHeader {
    /**
     * Columns of table header.
     * @var array
     */
    private $columns;
    
    /**
     * Constructs table header.
     * 
     * @param array|TableHeaderColumn $columns one or more columns of table header.
     * @return TableHeader instance of this table header.
     */
    public function __construct(/*array|TableHeaderColumn*/ $columns = null) {
        $this->columns = array();
        $this->addColumns($columns);
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }
    
    /**
     * Adds one or more table header columns.
     * 
     * @param array|TableHeaderColumn $columns one or more columns of table header.
     * @return TableHeader instance of this table header.
     */
    public function addColumns(/*array|TableHeaderColumn*/ $columns = null) {
        if(is_array($columns)) {
            foreach($columns as $column) {
                if($column instanceof TableHeaderColumn)  {
                    $this->columns[] = $column;
                }
            }
        }
        else if($columns instanceof TableHeaderColumn)  {
            $this->columns[] = $column;
        }
        
        return $this;
    }
    
    /**
     * Removes one or more table header columns.
     * 
     * @param array|TableHeaderColumn $columns one or more columns of table header.
     * @return TableHeader instance of this table header.
     */
    public function removeColumns(/*array|TableHeaderColumn*/ $columns = null) {
        if(is_array($columns)) {
            foreach($columns as $column) {
                if($column instanceof TableHeaderColumn)  {
                    $key = array_search($column, $this->columns);
                    if($key !== false) {
                        unset($this->columns[$key]);
                    }
                }
            }
        }
        else if($columns instanceof TableHeaderColumn)  {
            $key = array_search($columns, $this->columns);
            if($key !== false) {
                unset($this->columns[$key]);
            }
        }
        
        return $this;
    }
    
    /**
     * Returns array of columns of table header.
     * 
     * @return array Columns of table header.
     */
    public function getColumns() {
        return $this->columns;
    }
}

?>

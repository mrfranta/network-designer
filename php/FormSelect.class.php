<?php
/**
 * This class represents form element input. For example:
 * <pre>
 *     <select id="foo" name="bar">
 *         <option value="1">First</option>
 *         <option value="2">Second</option>
 *         <option value="3">Third</option>
 *     </select>
 * </pre>
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property-read array $options Array of select options.
 * @property string $selected
 */
class FormSelect extends FormElement {
    /**
     * Array of select options.
     * @var array
     */
    private $options;
    
    /**
     * Constructs form select.
     * 
     * @param array $params array of parameters of form select.
     * @return FormSelect instance of this form select.
     */
    public function __construct(/*array*/ $params = null) {
        $this->options = array();

        parent::__construct($params);
        
        return $this;
    }
    
    /**
     * Adds entered option into this form select.
     * 
     * @param FormSelectOption $option option to add.
     * @return FormSelect instance of this form select.
     */
    public function addOption(/*FormSelectOption*/ $option) {
        if(is_object($option) && $option instanceof FormSelectOption) {
            array_push($this->options, $option);
        }
        
        return $this;
    }
    
    /**
     * Removes entered option from this form select.
     * 
     * @param FormSelectOption $option option to remove.
     * @return FormSelect instance of this form select.
     */
    public function removeOption(/*FormSelectOption*/ $option) {
        if(($index = array_search($option, $this->items)) !== false) {
            unset($this->items[$index]);
        }
        
        return $this;
    }

    /**
     * Sets options in this form select.
     * 
     * @param array $options array of form select options.
     * @return FormSelect instance of this form select.
     */
    protected function setOptions(/*array*/ $options) {
        if(is_array($options)) {
            $this->options = $options;
        }
        
        return $this;
    }
    
    /**
     * Returns array of select options.
     * 
     * @return array Array of select options.
     */
    public function getOptions() {
        return $this->options;
    }
    
    /**
     * Returns selected value.
     * 
     * @return Selected value.
     */
    public function getSelected() {
        return parent::getValue();
    }
    
    /**
     * Returns selected value.
     * 
     * @param string $selected selected value.
     * @return FormSelect instance of this form select.
     */
    public function setSelected(/*string*/ $selected) {
        parent::setValue($selected);
        
        return $this;
    }
    
    /**
     * Returns Smarty template file for form select.
     * 
     * @return string Smarty template file for form select.
     */
    public function getTemplate() {
        return "select";
    }
}
?>

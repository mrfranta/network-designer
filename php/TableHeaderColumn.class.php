<?php
/**
 * This class represents column of table header.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 * 
 * @property string $name column name.
 * @property int $width column width in pixels.
 */
class TableHeaderColumn {
    /**
     * Column name.
     * @var string
     */
    private $name;
    /**
     * Column width in pixels.
     * @var int
     */
    private $width;
    
    /**
     * Constructs column of table header.
     * 
     * @param string $name column name.
     * @param int $width width of the column. Default is -1 that means automatic width.
     * @return TableHeaderColumn instance of this column.
     */
    public function __construct(/*string*/ $name, /*int*/ $width = -1) {
        $this->name = $name;
        $this->width = $width;
        
        return $this;
    }
    
    /**
     * Automatic getter, which calls getter of entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which value will be returned.
     * @return mixed value of variable.
     */
    public function __get(/*string*/ $name) {
        $method_name = 'get' . ucfirst($name);
        
        return $this->$method_name();
    }

    /**
     * Automatic setter, which calls setter of entered variable to which is 
     * accessed as <tt>$instance->variable = $value</tt>.
     * 
     * @param string $name name of the variable which value will be set.
     * @param mixed $value value of variable to set.
     */
    public function __set(/*string*/ $name, /*mixed*/ $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }
    
    /**
     * Authomatic isset function which returns information whether entered 
     * variable to which is accessed as <tt>$instance->variable</tt> is set or not.
     * 
     * @param string $name name of the variable which will be tested.
     * @return boolean True if entered variable is set; false otherwise.
     */
    public function __isset(/*string*/ $name) {
        return isset($this->$name);
    }

    /**
     * Authomatic unset function which unsets entered variable to which is 
     * accessed as <tt>$instance->variable</tt>.
     * 
     * @param string $name name of the variable which will be unset.
     */
    public function __unset(/*string*/ $name) {
        unset($this->$name);
    }
    
    /**
     * Returns column name.
     * 
     * @return string column name.
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Sets column name.
     * 
     * @param string $name column name.
     * @return TableHeaderColumn instance of this column.
     */
    public function setName(/*string*/ $name) {
        $this->name = $name;
                
        return $this;
    }
    
    /**
     * Returns column width.
     * 
     * @return int Column width in pixels. Value -1 represents automatic width.
     */
    public function getWidth() {
        return $this->width;
    }
    
    /**
     * Sets column width.
     * 
     * @param int $width column width in pixels. Default is -1 that means automatic width.
     * @return TableHeaderColumn instance of this column.
     */
    public function setWidth(/*int*/ $width = -1) {
        $this->width = $width;
                
        return $this;
    }
}

?>

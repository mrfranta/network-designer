<?php
/**
 * This class serves for uploading and resizing images.
 *
 * @author Mr.FrAnTA (Michal Dékány) <mrfranta@students.zcu.cz>
 * @version 1.0
 */
class ImageUploader {
    /**
     * Error message of uploader.
     * @var string
     */
    private $error;
    
    /** Binary power. */
    const BINARY_POWER = 1024; // B => kB => MB
    /** Maximal image size in megabytes. */
    const MAX_IMAGE_SIZE = 1;
    /** Maximal image size in pixels */
    const MAX_SIZE = 1024;
    /** Quality for saving JPEG images. */
    const JPEG_QUALITY = 100; // best quality
    
    /** Target size of uploaded images.*/
    const TARGET_SIZE = 48; // size of image that will be saved
    
    /**
     * Constructs image uploader.
     * 
     * @return ImageUploader instance of this image uploader.
     */
    public function __construct() {
        $this->error = "";
        
        return $this;
    }
    
    /**
     * Returns error message of uploader.
     * 
     * @return string Error message of uploader.
     */
    public function getError() {
        return $this->error;
    }
    
    /**
     * Returns maximal size of image.
     * 
     * @return int Maximal size of image in pixels.
     */
    public static function getMaxSize() {
        return self::MAX_IMAGE_SIZE * self::BINARY_POWER * self::BINARY_POWER;
    }
    
    /**
     * Uploads, resize and saves entered image.
     * 
     * @param array $image image informations.
     * @param string $directory directory into which will be image saved.
     * @param string $name name of image into which will be uploaded image saved.
     * @return boolean information whether was upload successfull.
     */
    public function uploadImage(/*array*/ $image, /*string*/ $directory, /*string*/ $name) {
        $this->error = "";
        
        if($image["size"] > self::getMaxSize()) {
            $this->error = "Chosen image that is too big!";
            
            return false;
        }

        $size = getimagesize($image["tmp_name"]);
        if(stristr($image["type"], "image") || !$size) {
            $image["width"] = $size[0];
            $image["height"] = $size[1];
            $image["format"] = $size[2];

            if($image["width"] > self::MAX_SIZE || $image["height"] > self::MAX_SIZE) {
                $this->error = "Chosen picture is too large";
                return false;
            }
            
            switch($image["format"]) { // JPG, PNG, GIF
                case IMAGETYPE_GIF:
                    $image["object"] = imagecreatefromgif($image["tmp_name"]);
                    $image["extension"] = "gif";
                    break;
                
                case IMAGETYPE_JPEG:
                    $image["object"] = imagecreatefromjpeg($image["tmp_name"]);
                    $image["extension"] = "jpg";
                    break;
                
                case IMAGETYPE_PNG:
                    $image["object"] = imagecreatefrompng($image["tmp_name"]);
                    $image["extension"] = "png";
                    break;
                
                default:
                    $this->error = "Chosen wrong image format (allowed only *.jpg, *.png, *.gif)";
                    return false;
            }

            //$path = "../images/originals/{$directory}/{$name}.{$extension}";
            //if(!move_uploaded_file($image["tmp_name"], $path)) {
            //    $this->error = "Image can not be uploaded to the server";
            //    return false;
            //}
            
            $image["resized"] = $this->smartResizeImage($image, self::TARGET_SIZE, self::TARGET_SIZE, true);

            $dir = "../images/{$directory}/";
            if(!file_exists($dir)) {
                mkdir($dir);
            }
            
            $name = self::editImageName($name);
            
            $path = "{$dir}{$name}.{$image['extension']}";
            $i = 2;
            while(file_exists($path)) {
                $path = "{$dir}{$name}{$i}.{$image['extension']}";
                $i++;
            }
            
            switch($image["format"]) { // JPG, PNG, GIF
                case IMAGETYPE_GIF:
                    imagegif($image["resized"], $path);
                    break;
                
                case IMAGETYPE_JPEG:
                    imagejpeg($image["resized"], $path, self::JPEG_QUALITY);
                    break;
                
                case IMAGETYPE_PNG:
                    imagepng($image["resized"], $path);
                    break;
            }

            @imagedestroy($image["object"]);
            @imagedestroy($image["resized"]);
            
            if(strpos($path, "../") == 0) {
                return substr($path, 3);
            }
            
            return $path;
        }
        else {
            $this->error = "The selected file is not an image";
            return false;
        }
    }
    
    /**
     * Returns image name in right form (no accents, spaces etc) - only characters 
     * with a-z, A-Z, 0-9.
     * 
     * @param string $imageName old image iname.
     * @return string Image name in right form.
     * @static
     */
    public static function editImageName($imageName) {
        setlocale(LC_ALL, 'cs_CZ.utf8');
        $imageName = iconv('utf-8', 'ascii//TRANSLIT', $imageName);
        $imageName = preg_replace("/'/", "", $imageName);
        $imageName = strtolower($imageName);
        $imageName = preg_replace("/[^a-zA-Z0-9\-_]/", "_", $imageName);

        return $imageName;
    }
    
    /**
     * Resizes entered image into specified width and/or height.
     * 
     * @param array $image array which contains informations about image.
     * @param int $width target width of image.
     * @param int $height target height of image.
     * @param boolean $proportional information whether will be image resized proportional.
     * @return Image|boolean Resized image or false on failure.
     * @link https://github.com/maxim/smart_resize_image/blob/master/smart_resize_image.function.php source
     */
    private function smartResizeImage($image, $width = 0, $height = 0, $proportional = false) {
        if($height <= 0 && $width <= 0) {
            return false;
        }

        // Setting defaults and meta
        $final_width = 0;
        $final_height = 0;
        $width_old = $image["width"];
        $height_old = $image["height"];
        
        // Calculating proportionality
        if($proportional) {
            if($width == 0) {
                $factor = $height / $height_old;
            }
            elseif($height == 0) {
                $factor = $width / $width_old;
            }
            else {
                $factor = min($width / $width_old, $height / $height_old);
            }

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        }
        else {
            $final_width = ($width <= 0) ? $width_old : $width;
            $final_height = ($height <= 0) ? $height_old : $height;
        }
    
        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if(($image["format"] == IMAGETYPE_GIF) || ($image["format"] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image["object"]);

            if($transparency >= 0) {
                $trnprt_color = imagecolorsforindex($image["object"], $transparency);
                $transparency = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            }
            elseif ($image["format"] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        
        imagecopyresampled($image_resized, $image["object"], 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
        
        return $image_resized;
    }
}

?>

-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vygenerováno: Sob 04. led 2014, 04:07
-- Verze serveru: 5.6.15-log
-- Verze PHP: 5.4.22

SET NAMES utf8;

--
-- Databáze: `network_designer`
--
CREATE DATABASE IF NOT EXISTS `network_designer` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `network_designer`;

-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_cables`
--

DROP TABLE IF EXISTS `tbl_cables`;
CREATE TABLE IF NOT EXISTS `tbl_cables` (
  `cable_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cable_name` varchar(50) NOT NULL,
  `cable_color` varchar(6) NOT NULL DEFAULT '000000',
  `cable_line` enum('solid','dotted','dashed') NOT NULL DEFAULT 'solid',
  `cable_image` varchar(100) NOT NULL,
  PRIMARY KEY (`cable_id`),
  UNIQUE KEY `cable_name` (`cable_name`),
  UNIQUE KEY `cable_image` (`cable_image`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_cables`
--

TRUNCATE TABLE `tbl_cables`;
--
-- Vypisuji data pro tabulku `tbl_cables`
--

INSERT INTO `tbl_cables` (`cable_id`, `cable_name`, `cable_color`, `cable_line`, `cable_image`) VALUES
(1, 'Cross-Over', '000000', 'dashed', 'images/cables/cross-over.png'),
(2, 'Straight-Through', '000000', 'solid', 'images/cables/straight-through.png');

-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_categories`
--

DROP TABLE IF EXISTS `tbl_categories`;
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_name` (`category_name`),
  UNIQUE KEY `category_image` (`category_image`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_categories`
--

TRUNCATE TABLE `tbl_categories`;
--
-- Vypisuji data pro tabulku `tbl_categories`
--

INSERT INTO `tbl_categories` (`category_id`, `category_name`, `category_image`) VALUES
(1, '(none)', 'images/categories/(none).png'),
(2, 'Routers', 'images/categories/routers.png'),
(3, 'Switches', 'images/categories/switches.png'),
(4, 'Hubs', 'images/categories/hubs.png'),
(5, 'Wireless devices', 'images/categories/wireless_devices.png'),
(6, 'End devices', 'images/categories/end_devices.png');

-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_devices`
--

DROP TABLE IF EXISTS `tbl_devices`;
CREATE TABLE IF NOT EXISTS `tbl_devices` (
  `device_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '1',
  `device_name` varchar(50) NOT NULL,
  `device_image` varchar(100) NOT NULL,
  `device_ports` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_devices`
--

TRUNCATE TABLE `tbl_devices`;
--
-- Vypisuji data pro tabulku `tbl_devices`
--

INSERT INTO `tbl_devices` (`device_id`, `category_id`, `device_name`, `device_image`, `device_ports`) VALUES
(1, 2, '1841', 'images/devices/1841.png', 2),
(2, 2, '1941', 'images/devices/1941.png', 2),
(3, 2, '2620XM', 'images/devices/2620xm.png', 1),
(4, 2, '2621XM', 'images/devices/2621xm.png', 2),
(5, 2, '2811', 'images/devices/2811.png', 2),
(6, 2, '2901', 'images/devices/2901.png', 2),
(7, 2, '2911', 'images/devices/2911.png', 3),
(8, 2, 'Generic', 'images/devices/generic.png', 0),
(9, 3, '2950', 'images/devices/2950.png', 24),
(10, 3, '2950T', 'images/devices/2950t.png', 26),
(11, 3, '2960', 'images/devices/2960.png', 26),
(12, 3, '3560', 'images/devices/3560.png', 26),
(13, 3, 'Generic', 'images/devices/generic2.png', 0),
(14, 3, 'Bridge', 'images/devices/bridge.png', 2),
(15, 4, 'Generic', 'images/devices/generic3.png', 0),
(16, 4, 'Repeater', 'images/devices/repeater.png', 2),
(17, 5, 'AccessPoint-PT', 'images/devices/accesspoint-pt.png', 1),
(18, 5, 'AccessPoint-PT-A', 'images/devices/accesspoint-pt-a.png', 1),
(19, 5, 'AccessPoint-PT-N', 'images/devices/accesspoint-pt-n.png', 1),
(20, 5, 'Linksys-WRT300N', 'images/devices/linksys-wrt300n.png', 5),
(21, 6, 'PC', 'images/devices/pc.png', 0),
(22, 6, 'Laptop', 'images/devices/laptop.png', 1),
(23, 6, 'Server', 'images/devices/server.png', 0),
(24, 6, 'Printer', 'images/devices/printer.png', 2),
(25, 6, 'TV', 'images/devices/tv.png', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_networks`
--

DROP TABLE IF EXISTS `tbl_networks`;
CREATE TABLE IF NOT EXISTS `tbl_networks` (
  `network_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `network_name` varchar(255) NOT NULL,
  `network_title` varchar(255) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `network_width` int(50) unsigned NOT NULL,
  `network_height` int(50) unsigned NOT NULL,
  `network_background` varchar(50) DEFAULT NULL,
  `network_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`network_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_networks`
--

TRUNCATE TABLE `tbl_networks`;
-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_network_cables`
--

DROP TABLE IF EXISTS `tbl_network_cables`;
CREATE TABLE IF NOT EXISTS `tbl_network_cables` (
  `network_cable_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cable_id` int(10) unsigned NOT NULL,
  `network_id` bigint(20) unsigned NOT NULL,
  `source_network_device_id` int(100) unsigned NOT NULL,
  `target_network_device_id` int(100) unsigned NOT NULL,
  `source_device_port` int(100) unsigned NOT NULL,
  `target_device_port` int(100) unsigned NOT NULL,
  PRIMARY KEY (`network_cable_id`),
  KEY `cable_id` (`cable_id`,`network_id`,`source_network_device_id`,`target_network_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_network_cables`
--

TRUNCATE TABLE `tbl_network_cables`;
-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_network_devices`
--

DROP TABLE IF EXISTS `tbl_network_devices`;
CREATE TABLE IF NOT EXISTS `tbl_network_devices` (
  `network_device_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` bigint(20) unsigned NOT NULL,
  `network_id` bigint(20) unsigned NOT NULL,
  `network_device_label` varchar(255) NOT NULL,
  `network_device_ports` int(10) NOT NULL,
  `network_device_x` int(50) unsigned NOT NULL,
  `network_device_y` int(50) unsigned NOT NULL,
  PRIMARY KEY (`network_device_id`),
  KEY `device_id` (`device_id`,`network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_network_devices`
--

TRUNCATE TABLE `tbl_network_devices`;
-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_network_interfaces`
--

DROP TABLE IF EXISTS `tbl_network_interfaces`;
CREATE TABLE IF NOT EXISTS `tbl_network_interfaces` (
  `network_interface_id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
  `network_id` bigint(20) NOT NULL,
  `network_device_id` bigint(20) unsigned NOT NULL,
  `network_interface_ip` varchar(50) DEFAULT '',
  `network_interface_mask` varchar(50) NOT NULL DEFAULT '',
  `network_interface_vlans` varchar(255) NOT NULL DEFAULT '',
  `network_interface_info` text,
  PRIMARY KEY (`network_interface_id`),
  KEY `network_device_id` (`network_device_id`),
  KEY `network_id` (`network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_network_interfaces`
--

TRUNCATE TABLE `tbl_network_interfaces`;
-- --------------------------------------------------------

--
-- Struktura tabulky `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_pass` varchar(100) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_level` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vyprázdnit tabulku před vkládáním `tbl_users`
--

TRUNCATE TABLE `tbl_users`;
--
-- Vypisuji data pro tabulku `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'mrfranta@students.zcu.cz', 1),
(2, 'petrovic', '087a7b80e974b2434837a8300dfa54d1', 'petrovic@civ.zcu.cz', 1);
